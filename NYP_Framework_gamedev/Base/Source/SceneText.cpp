#include "SceneText.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "MeshBuilder.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
#include <sstream>
#include "KeyboardController.h"
#include "MouseController.h"
#include "SceneManager.h"
#include "GraphicsManager.h"
#include "ShaderProgram.h"
#include "EntityManager.h"

#include "GenericEntity.h"
#include "GroundEntity.h"
#include "TextEntity.h"
#include "SpriteEntity.h"
#include "Light.h"
#include "SkyBox/SkyBoxEntity.h"
#include "Minimap\Minimap.h"

#include "WeatherInfo\Rain.h"
#include "WeatherInfo\Smoke.h"
#include "WeatherInfo\Gas.h"

#include "RenderHelper.h"

#include <iostream>
using namespace std;

SceneText* SceneText::sInstance = new SceneText(SceneManager::GetInstance());

int SceneText::Enemycount = 0;

SceneText::SceneText()
	: theMinimap(NULL)
	, theCameraEffects(NULL)
{
}

SceneText::SceneText(SceneManager* _sceneMgr)
	: theMinimap(NULL)
	, theCameraEffects(NULL)
{
	_sceneMgr->AddScene("Start", this);
}

SceneText::~SceneText()
{
	if (theCameraEffects)
	{
		delete theCameraEffects;
		theCameraEffects = NULL;
	}
	if (theMinimap)
	{
		delete theMinimap;
		theMinimap = NULL;
	}
	if (theMouse)
	{
		delete theMouse;
		theMouse = NULL;
	}
	if (theKeyboard)
	{
		delete theKeyboard;
		theKeyboard = NULL;
	}

}

void SceneText::Init()
{
	currProg = GraphicsManager::GetInstance()->LoadShader("default", "Shader//comg.vertexshader", "Shader//comg.fragmentshader");

	// Tell the shader program to store these uniform locations
	currProg->AddUniform("MVP");
	currProg->AddUniform("MV");
	currProg->AddUniform("MV_inverse_transpose");
	currProg->AddUniform("material.kAmbient");
	currProg->AddUniform("material.kDiffuse");
	currProg->AddUniform("material.kSpecular");
	currProg->AddUniform("material.kShininess");
	currProg->AddUniform("lightEnabled");
	currProg->AddUniform("numLights");
	currProg->AddUniform("lights[0].type");
	currProg->AddUniform("lights[0].position_cameraspace");
	currProg->AddUniform("lights[0].color");
	currProg->AddUniform("lights[0].power");
	currProg->AddUniform("lights[0].kC");
	currProg->AddUniform("lights[0].kL");
	currProg->AddUniform("lights[0].kQ");
	currProg->AddUniform("lights[0].spotDirection");
	currProg->AddUniform("lights[0].cosCutoff");
	currProg->AddUniform("lights[0].cosInner");
	currProg->AddUniform("lights[0].exponent");
	currProg->AddUniform("lights[1].type");
	currProg->AddUniform("lights[1].position_cameraspace");
	currProg->AddUniform("lights[1].color");
	currProg->AddUniform("lights[1].power");
	currProg->AddUniform("lights[1].kC");
	currProg->AddUniform("lights[1].kL");
	currProg->AddUniform("lights[1].kQ");
	currProg->AddUniform("lights[1].spotDirection");
	currProg->AddUniform("lights[1].cosCutoff");
	currProg->AddUniform("lights[1].cosInner");
	currProg->AddUniform("lights[1].exponent");
	currProg->AddUniform("colorTextureEnabled");
	currProg->AddUniform("colorTexture");
	currProg->AddUniform("textEnabled");
	currProg->AddUniform("textColor");
	currProg->AddUniform("transparency");


	// Tell the graphics manager to use the shader we just loaded
	GraphicsManager::GetInstance()->SetActiveShader("default");

	/*lights[0] = new Light();
	GraphicsManager::GetInstance()->AddLight("lights[0]", lights[0]);
	lights[0]->type = Light::LIGHT_DIRECTIONAL;
	lights[0]->position.Set(0, 20, 0);
	lights[0]->color.Set(1, 1, 1, 1);
	lights[0]->power = 1;
	lights[0]->kC = 1.f;
	lights[0]->kL = 0.01f;
	lights[0]->kQ = 0.001f;
	lights[0]->cosCutoff = cos(Math::DegreeToRadian(45));
	lights[0]->cosInner = cos(Math::DegreeToRadian(30));
	lights[0]->exponent = 3.f;
	lights[0]->spotDirection.Set(0.f, 1.f, 0.f);
	lights[0]->name = "lights[0]";

	lights[1] = new Light();
	GraphicsManager::GetInstance()->AddLight("lights[1]", lights[1]);
	lights[1]->type = Light::LIGHT_DIRECTIONAL;
	lights[1]->position.Set(1, 1, 0);
	lights[1]->color.Set(1, 1, 0.5f, 1);
	lights[1]->power = 0.4f;
	lights[1]->name = "lights[1]";

	currProg->UpdateInt("numLights", 1);*/
	currProg->UpdateInt("textEnabled", 0);

	//// Create the playerinfo instance, which manages all information about the player
	playerInfo = CPlayerInfo::GetInstance();
	playerInfo->Init();

	// Create and attach the camera to the scene
	//camera.Init(Vector3(0, 0, 10), Vector3(0, 0, 0), Vector3(0, 1, 0));
	camera.Init(playerInfo->GetPos(), playerInfo->GetTarget(), playerInfo->GetUp());
	playerInfo->AttachCamera(&camera);
	GraphicsManager::GetInstance()->AttachCamera(&camera);

	// Load all the meshes
	MeshBuilder::GetInstance()->GenerateAxes("reference");
	MeshBuilder::GetInstance()->GenerateCrossHair("crosshair");
	MeshBuilder::GetInstance()->GenerateQuad("quad", Color(0, 0, 0), 1.f);
	MeshBuilder::GetInstance()->GenerateQuad("StatQuad", Color(0, 1, 0), 1.f);

	MeshBuilder::GetInstance()->GenerateText("text", 16, 16);
	MeshBuilder::GetInstance()->GetMesh("text")->textureID = LoadTGA("Image//calibri.tga");
	MeshBuilder::GetInstance()->GetMesh("text")->material.kAmbient.Set(1, 0, 0);
	MeshBuilder::GetInstance()->GenerateRing("ring", Color(1, 0, 1), 36, 1, 0.5f);
	MeshBuilder::GetInstance()->GenerateSphere("lightball", Color(1, 1, 1), 18, 36, 1.f);
	MeshBuilder::GetInstance()->GenerateSphere("sphere", Color(1, 0, 0), 18, 36, 1.f);

	MeshBuilder::GetInstance()->GenerateCone("cone", Color(0.5f, 1, 0.3f), 36, 10.f, 10.f);
	MeshBuilder::GetInstance()->GenerateCube("cube", Color(1.0f, 1.0f, 0.0f), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("cone")->material.kDiffuse.Set(0.99f, 0.99f, 0.99f);
	MeshBuilder::GetInstance()->GetMesh("cone")->material.kSpecular.Set(0.f, 0.f, 0.f);
	MeshBuilder::GetInstance()->GenerateQuad("GRASS_DARKGREEN", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("GRASS_DARKGREEN")->textureID = LoadTGA("Image//grass_darkgreen.tga");
	MeshBuilder::GetInstance()->GenerateQuad("Ground", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("Ground")->textureID = LoadTGA("Image//ground.tga");

	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_FRONT", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_BACK", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_LEFT", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_RIGHT", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_TOP", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GenerateQuad("SKYBOX_BOTTOM", Color(1, 1, 1), 1.f);
	MeshBuilder::GetInstance()->GetMesh("SKYBOX_FRONT")->textureID = LoadTGA("Image//SkyBox//sky.tga");
	MeshBuilder::GetInstance()->GetMesh("SKYBOX_BACK")->textureID = LoadTGA("Image//SkyBox//sky.tga");
	MeshBuilder::GetInstance()->GetMesh("SKYBOX_LEFT")->textureID = LoadTGA("Image//SkyBox//sky.tga");
	MeshBuilder::GetInstance()->GetMesh("SKYBOX_RIGHT")->textureID = LoadTGA("Image//SkyBox//sky.tga");
	MeshBuilder::GetInstance()->GetMesh("SKYBOX_TOP")->textureID = LoadTGA("Image//SkyBox//sky.tga");
	MeshBuilder::GetInstance()->GetMesh("SKYBOX_BOTTOM")->textureID = LoadTGA("Image//SkyBox//sky.tga");

	//UI

	MeshBuilder::GetInstance()->GenerateQuad("MenuQuad", Color(0.8, 0.8, 0.8), 1.0f);

	MeshBuilder::GetInstance()->GenerateQuad("ReplaceQuad", Color(0, 0.5, 0), 1.f);

	MeshBuilder::GetInstance()->GenerateQuad("Death", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("Death")->textureID = LoadTGA("Image//gameOverScreen.tga");

	MeshBuilder::GetInstance()->GenerateQuad("Win", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("Win")->textureID = LoadTGA("Image//WinScreen.tga");

	//menu

	//class selection hud
	MeshBuilder::GetInstance()->GenerateQuad("ClassScout", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("ClassScout")->textureID = LoadTGA("Image//ClassHudScout.tga");

	MeshBuilder::GetInstance()->GenerateQuad("ClassTank", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("ClassTank")->textureID = LoadTGA("Image//ClassHudTank.tga");

	MeshBuilder::GetInstance()->GenerateQuad("ClassScavenger", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("ClassScavenger")->textureID = LoadTGA("Image//ClassHudScavenger.tga");


	//Controls screen
	MeshBuilder::GetInstance()->GenerateQuad("Controls", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("Controls")->textureID = LoadTGA("Image//Controls.tga");

	//Title
	MeshBuilder::GetInstance()->GenerateQuad("Title", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("Title")->textureID = LoadTGA("Image//zombs.tga");

	//health icon
	MeshBuilder::GetInstance()->GenerateQuad("Health", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("Health")->textureID = LoadTGA("Image//HealthIcon.tga");

	//high health
	MeshBuilder::GetInstance()->GenerateQuad("quadhigh", Color(0, 1, 0), 1.f);

	//medium health
	MeshBuilder::GetInstance()->GenerateQuad("quadmed", Color(1, 1, 0), 1.f);

	//low health
	MeshBuilder::GetInstance()->GenerateQuad("quadlow", Color(1, 0, 0), 1.f);

	//ammo icon
	MeshBuilder::GetInstance()->GenerateQuad("Ammo", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("Ammo")->textureID = LoadTGA("Image//AmmoIcon.tga");

	//stamina icon
	MeshBuilder::GetInstance()->GenerateQuad("StaminaHigh", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("StaminaHigh")->textureID = LoadTGA("Image//StaminaGreen.tga");

	MeshBuilder::GetInstance()->GenerateQuad("StaminaMed", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("StaminaMed")->textureID = LoadTGA("Image//StaminaYellow.tga");

	MeshBuilder::GetInstance()->GenerateQuad("StaminaLow", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("StaminaLow")->textureID = LoadTGA("Image//StaminaRed.tga");

	//GunStats Icon
	MeshBuilder::GetInstance()->GenerateQuad("GunStats", Color(0, 1, 0), 1.f);

	//grenade icon
	MeshBuilder::GetInstance()->GenerateQuad("GrenadeIcon", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("GrenadeIcon")->textureID = LoadTGA("Image//Grenade.tga");

	//Medkit icon
	MeshBuilder::GetInstance()->GenerateQuad("MedKit", Color(1, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("MedKit")->textureID = LoadTGA("Image//MedKit.tga");

	//crosshairs
	MeshBuilder::GetInstance()->GenerateQuad("CrosshairNormal", Color(0, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("CrosshairNormal")->textureID = LoadTGA("Image//CrosshairNormal.tga");

	MeshBuilder::GetInstance()->GenerateQuad("CrosshairShotgun", Color(0, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("CrosshairShotgun")->textureID = LoadTGA("Image//CrosshairShotgun.tga");

	//money icon
	MeshBuilder::GetInstance()->GenerateQuad("Money", Color(0, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("Money")->textureID = LoadTGA("Image//Money.tga");

	//abilities
	MeshBuilder::GetInstance()->GenerateQuad("ScoutAbilityReady", Color(0, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("ScoutAbilityReady")->textureID = LoadTGA("Image//ScoutIconGreen.tga");

	MeshBuilder::GetInstance()->GenerateQuad("ScoutAbilityDown", Color(0, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("ScoutAbilityDown")->textureID = LoadTGA("Image//ScoutIconRed.tga");

	MeshBuilder::GetInstance()->GenerateQuad("TankAbilityReady", Color(0, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("TankAbilityReady")->textureID = LoadTGA("Image//TankIconGreen.tga");

	MeshBuilder::GetInstance()->GenerateQuad("TankAbilityDown", Color(0, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("TankAbilityDown")->textureID = LoadTGA("Image//TankIconRed.tga");

	MeshBuilder::GetInstance()->GenerateQuad("ScavengerAbilityReady", Color(0, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("ScavengerAbilityReady")->textureID = LoadTGA("Image//ScavengerIconGreen.tga");

	MeshBuilder::GetInstance()->GenerateQuad("ScavengerAbilityDown", Color(0, 0, 0), 1.0f);
	MeshBuilder::GetInstance()->GetMesh("ScavengerAbilityDown")->textureID = LoadTGA("Image//ScavengerIconRed.tga");

	//Particles
	MeshBuilder::GetInstance()->GenerateQuad("RainDrop", Color(1, 0, 0), 2.0f);
	MeshBuilder::GetInstance()->GetMesh("RainDrop")->textureID = LoadTGA("Image//Raindrop.tga");


	MeshBuilder::GetInstance()->GenerateQuad("Smoke", Color(1, 0, 0), 2.0f);
	MeshBuilder::GetInstance()->GetMesh("Smoke")->textureID = LoadTGA("Image//Smoke.tga");

	MeshBuilder::GetInstance()->GenerateQuad("Gas", Color(1, 0, 0), 2.f);
	MeshBuilder::GetInstance()->GetMesh("Gas")->textureID = LoadTGA("Image//gasParticle.tga");

	//sprite animation
	MeshBuilder::GetInstance()->GenerateSpriteAnimation("Explosion", 4, 5);
	MeshBuilder::GetInstance()->GetMesh("Explosion")->textureID = LoadTGA("Image//explosionParticle.tga");


	//guns
	MeshBuilder::GetInstance()->GenerateSphere("Bullet", Color(1, 0, 0), 18, 36, 1.f);
	MeshBuilder::GetInstance()->GetMesh("Bullet")->textureID = LoadTGA("Image//bullet texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("M1911", "OBJ//M1911.obj");
	MeshBuilder::GetInstance()->GetMesh("M1911")->textureID = LoadTGA("Image//M1911 Texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("M9", "OBJ//M9.obj");
	MeshBuilder::GetInstance()->GetMesh("M9")->textureID = LoadTGA("Image//M9 Texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("MPRex", "OBJ//MPRex.obj");
	MeshBuilder::GetInstance()->GetMesh("MPRex")->textureID = LoadTGA("Image//REX Texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("M4A1", "OBJ//M4A1.obj");
	MeshBuilder::GetInstance()->GetMesh("M4A1")->textureID = LoadTGA("Image//M4A1 Texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("PumpShotgun", "OBJ//PumpShotgun.obj");
	MeshBuilder::GetInstance()->GetMesh("PumpShotgun")->textureID = LoadTGA("Image//Shotgun Texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("UMP", "OBJ//UMP.obj");
	MeshBuilder::GetInstance()->GetMesh("UMP")->textureID = LoadTGA("Image//UMP Texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Scar-H", "OBJ//Scar-H.obj");
	MeshBuilder::GetInstance()->GetMesh("Scar-H")->textureID = LoadTGA("Image//SCAR-H Texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("AWP", "OBJ//AWP.obj");
	MeshBuilder::GetInstance()->GetMesh("AWP")->textureID = LoadTGA("Image//AWP Texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Railgun", "OBJ//Railgun.obj");
	MeshBuilder::GetInstance()->GetMesh("Railgun")->textureID = LoadTGA("Image//Railgun Texture.tga");

	//buildings
	MeshBuilder::GetInstance()->GenerateOBJ("Blacksmith", "OBJ//blacksmith.obj");
	MeshBuilder::GetInstance()->GetMesh("Blacksmith")->textureID = LoadTGA("Image//building_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Cabin", "OBJ//cabin.obj");
	MeshBuilder::GetInstance()->GetMesh("Cabin")->textureID = LoadTGA("Image//building_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Church", "OBJ//church.obj");
	MeshBuilder::GetInstance()->GetMesh("Church")->textureID = LoadTGA("Image//building_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Stall", "OBJ//stall.obj");
	MeshBuilder::GetInstance()->GetMesh("Stall")->textureID = LoadTGA("Image//stall_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Cart", "OBJ//cart.obj");
	MeshBuilder::GetInstance()->GetMesh("Cart")->textureID = LoadTGA("Image//cart_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Campfire", "OBJ//campfire.obj");
	MeshBuilder::GetInstance()->GetMesh("Campfire")->textureID = LoadTGA("Image//campfire_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Well", "OBJ//well.obj");
	MeshBuilder::GetInstance()->GetMesh("Well")->textureID = LoadTGA("Image//well_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Wall", "OBJ//wall.obj");
	MeshBuilder::GetInstance()->GetMesh("Wall")->textureID = LoadTGA("Image//wall_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Wall2", "OBJ//wall2.obj");
	MeshBuilder::GetInstance()->GetMesh("Wall2")->textureID = LoadTGA("Image//wall2_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Gate", "OBJ//fence.obj");
	MeshBuilder::GetInstance()->GetMesh("Gate")->textureID = LoadTGA("Image//fence_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("VendingMachine", "OBJ//VendingMachine.obj");
	MeshBuilder::GetInstance()->GetMesh("VendingMachine")->textureID = LoadTGA("Image//VendingMachine.tga");

	//enemies
	MeshBuilder::GetInstance()->GenerateOBJ("BasicEnemy", "OBJ//BasicEnemy.obj");
	MeshBuilder::GetInstance()->GetMesh("BasicEnemy")->textureID = LoadTGA("Image//BasicEnemy.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("Target", "OBJ//target.obj");
	MeshBuilder::GetInstance()->GetMesh("Target")->textureID = LoadTGA("Image//target_texture.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("TankEnemy", "OBJ//TankEnemy.obj");
	MeshBuilder::GetInstance()->GetMesh("TankEnemy")->textureID = LoadTGA("Image//TankEnemy.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("SpeedyEnemy", "OBJ//SpeedyEnemy.obj");
	MeshBuilder::GetInstance()->GetMesh("SpeedyEnemy")->textureID = LoadTGA("Image//SpeedyEnemy.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("AmmoSnatcher", "OBJ//AmmoSnatcher.obj");
	MeshBuilder::GetInstance()->GetMesh("AmmoSnatcher")->textureID = LoadTGA("Image//AmmoSnatcher.tga");

	MeshBuilder::GetInstance()->GenerateOBJ("FlyingEnemy", "OBJ//FlyingEnemy.obj");
	MeshBuilder::GetInstance()->GetMesh("FlyingEnemy")->textureID = LoadTGA("Image//FlyingEnemy.tga");

	//Grenade
	MeshBuilder::GetInstance()->GenerateOBJ("Grenade", "OBJ//Grenade.obj");
	MeshBuilder::GetInstance()->GetMesh("Grenade")->textureID = LoadTGA("Image//Grenade Texture.tga");



	// Create entities into the scene
	groundEntity = Create::Ground("Ground", "Ground");
	//	Create::Text3DObject("text", Vector3(0.0f, 0.0f, 0.0f), "DM2210", Vector3(10.0f, 10.0f, 10.0f), Color(0, 1, 1));
	Create::Sprite2DObject("crosshair", Vector3(0.0f, 0.0f, 0.0f), Vector3(10.0f, 10.0f, 10.0f));

	SkyBoxEntity* theSkyBox = Create::SkyBox("SKYBOX_FRONT", "SKYBOX_BACK",
		"SKYBOX_LEFT", "SKYBOX_RIGHT",
		"SKYBOX_TOP", "SKYBOX_BOTTOM");

	//the buildings
	Boundary();
	AreaOne();
	AreaTwo();
	AreaThree();
	AreaFour();

	weatherManager[0] = new CRain;
	weatherManager[0]->Init();

	weatherManager[1] = new CSmoke;
	weatherManager[1]->Init();

	weatherManager[2] = new CGas;
	weatherManager[2]->Init();

	highscore = new Highscore();
	highscore->LoadData();
	//playerInfo->score = highscore->highScore;

	// Customise the ground entity
	groundEntity->SetPosition(Vector3(0, -10, 0));
	groundEntity->SetScale(Vector3(100.0f, 100.0f, 100.0f));
	groundEntity->SetGrids(Vector3(10.0f, 1.0f, 10.0f));
	playerInfo->SetTerrain(groundEntity);

	// Setup the 2D entities
	float halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2.0f;
	float halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2.0f;
	float fontSize = 25.0f;
	float halfFontSize = fontSize / 2.0f;

	//bot left ui
	for (int i = 0; i < 5; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth, -halfWindowHeight + fontSize*i + halfFontSize, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(1.0f, 1.0f, 1.0f));
	}

	//Grenade count
	textObj[5] = Create::Text2DObject("text", Vector3(270, -235, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(1.0f, 1.0f, 1.0f));

	//class name
	textObj[6] = Create::Text2DObject("text", Vector3(-60, 90, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));

	//ask to restart
	textObj[7] = Create::Text2DObject("text", Vector3(-200, -100, 0.0f), "", Vector3(fontSize * 0.8, fontSize * 0.8, fontSize * 0.8), Color(1.0f, 0.0f, 0.0f));

	//shop
	for (int i = 8; i < 16; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth, halfWindowHeight*1.55 - fontSize*i - halfFontSize, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.3f, 0.3f, 0.3f));
	}
	//gun stats
	for (int i = 16; i < 25; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth *0.4, halfWindowHeight * 0.95 - fontSize*i - halfFontSize, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	}

	//score
	textObj[25] = Create::Text2DObject("text", Vector3(halfWindowWidth * 0.14, halfWindowHeight * 0.9, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));

	//menu
	for (int i = 26; i < 30; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-120,200, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	}

	//Replace
	textObj[30] = Create::Text2DObject("text", Vector3(-150, 30, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));

	//bandage count
	textObj[31] = Create::Text2DObject("text", Vector3(270, -200, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(1.0f, 1.0f, 1.0f));

	//Weapon tab
	textObj[32] = Create::Text2DObject("text", Vector3(-halfWindowWidth, halfWindowHeight * 0.95,0), "", Vector3(fontSize, fontSize, fontSize), Color(1.0f, 1.0f, 1.0f));

	//Equipment tab
	textObj[33] = Create::Text2DObject("text", Vector3(-halfWindowWidth * 0.5, halfWindowHeight * 0.95, 0), "", Vector3(fontSize, fontSize, fontSize), Color(1.0f, 1.0f, 1.0f));

	//ammo
	for (int i = 34; i < 44; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth, halfWindowHeight * 3.72 - fontSize*i - halfFontSize, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	}

	//max nades
	textObj[44] = Create::Text2DObject("text", Vector3(-205, 52, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(1.0f, 0.0f, 0.0f));

	//max bandages
	textObj[45] = Create::Text2DObject("text", Vector3(-205, 28, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(1.0f, 0.0f, 0.0f));

	//wave count
	textObj[46] = Create::Text2DObject("text", Vector3(-halfWindowWidth, 280, 0.0f), "", Vector3(fontSize * 0.8, fontSize * 0.8, fontSize * 0.8), Color(1.0f, 1.0f, 0.0f));

	//enemies
	textObj[47] = Create::Text2DObject("text", Vector3(-halfWindowWidth, 260, 0.0f), "", Vector3(fontSize * 0.8, fontSize * 0.8, fontSize * 0.8), Color(1.0f, 1.0f, 0.0f));

	//gates
	for (int i = 48; i < 52; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth * 0.4, -halfWindowHeight * 0.4, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	}

	//money
	textObj[52] = Create::Text2DObject("text", Vector3(halfWindowWidth * 0.45, halfWindowHeight * 0.75, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));

	//gun cost
	textObj[53] = Create::Text2DObject("text", Vector3(halfWindowWidth * 0.25, halfWindowHeight * 0.5, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));

	//shop prompt
	textObj[54] = Create::Text2DObject("text", Vector3(-halfWindowWidth * 0.4, -halfWindowHeight * 0.4, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));

	//Class upgrade tab
	textObj[55] = Create::Text2DObject("text", Vector3(halfWindowWidth * 0.15, halfWindowHeight * 0.95, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));
	
	//Class upgrase text
	textObj[56] = Create::Text2DObject("text", Vector3(-halfWindowWidth, halfWindowHeight * 0.85, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(0.0f, 1.0f, 0.0f));

	//class tiers
	for (int i = 57; i < 60; ++i)
	{
		textObj[i] = Create::Text2DObject("text", Vector3(-halfWindowWidth * 0.8, halfWindowHeight* 5 - fontSize*i - halfFontSize, 0.0f), "", Vector3(fontSize * 0.9, fontSize * 0.9, fontSize * 0.9), Color(0.0f, 1.0f, 0.0f));
	}

	textObj[60] = Create::Text2DObject("text", Vector3(-26, -265, 0.0f), "", Vector3(fontSize * 0.9, fontSize * 0.9, fontSize * 0.9), Color(1.0f, 0.0f, 0.0f));

	textObj[61] = Create::Text2DObject("text", Vector3(-halfWindowWidth * 0.6, 0, 0.0f), "", Vector3(fontSize, fontSize, fontSize), Color(1.0f, 1.0f, 1.0f));

	AmmoCount = Create::Sprite2DObject("Ammo", Vector3(-378, -235, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	AmmoCount->SetActive(false);

	Health = Create::Sprite2DObject("Health", Vector3(-378, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	Health->SetActive(false);

	Grenades = Create::Sprite2DObject("GrenadeIcon", Vector3(300, -230, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	Grenades->SetActive(false);

	MedKits = Create::Sprite2DObject("MedKit", Vector3(300, -190, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	MedKits->SetActive(false);

	CrossHairNormal = Create::Sprite2DObject("CrosshairNormal", Vector3(0, 0, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	CrossHairNormal->SetActive(false);

	CrossHairShotgun = Create::Sprite2DObject("CrosshairShotgun", Vector3(0, 0, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	CrossHairShotgun->SetActive(false);

	Money = Create::Sprite2DObject("Money", Vector3(halfWindowWidth*0.4, halfWindowHeight * 0.75, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	Money->SetActive(false);

	Death = Create::Sprite2DObject("Death", Vector3(0, 0, 0.0f), Vector3(800.0f, 600.0f, 0.0f), true);
	Death->SetActive(false);

	ScoutClass = Create::Sprite2DObject("ClassScout", Vector3(0, 0, 0.0f), Vector3(800.0f, 600.0f, 0.0f), true);
	ScoutClass->SetActive(false);

	TankClass = Create::Sprite2DObject("ClassTank", Vector3(0, 0, 0.0f), Vector3(800.0f, 600.0f, 0.0f), true);
	TankClass->SetActive(false);

	ScavengerClass = Create::Sprite2DObject("ClassScavenger", Vector3(0, 0, 0.0f), Vector3(800.0f, 600.0f, 0.0f), true);
	ScavengerClass->SetActive(false);

	Controls = Create::Sprite2DObject("Controls", Vector3(0, 0, 0.0f), Vector3(800.0f, 600.0f, 0.0f), true);
	Controls->SetActive(false);

	Win = Create::Sprite2DObject("Win", Vector3(0, 0, 0.0f), Vector3(800.0f, 600.0f, 0.0f), true);
	Win->SetActive(false);

	Title = Create::Sprite2DObject("Title", Vector3(0, halfWindowHeight * 0.65, 0.0f), Vector3(450.0f, 450.0f, 0.0f), true);
	Title->SetActive(true);

	Stamina[0] = Create::Sprite2DObject("StaminaHigh", Vector3(370, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	Stamina[0]->SetActive(false);
	Stamina[1] = Create::Sprite2DObject("StaminaMed", Vector3(370, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	Stamina[1]->SetActive(false);
	Stamina[2] = Create::Sprite2DObject("StaminaLow", Vector3(370, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	Stamina[2]->SetActive(false);

	HealthBar[0] = Create::Sprite2DObject("quadhigh", Vector3(-230, -265, 0.0f), Vector3(playerInfo->Health * 2.5, 24.0f, 0.0f), true);
	HealthBar[0]->SetActive(false);
	HealthBar[1] = Create::Sprite2DObject("quadmed", Vector3(-230, -200, 0.0f), Vector3(32.0f, 24.0f, 0.0f), true);
	HealthBar[1]->SetActive(false);
	HealthBar[2] = Create::Sprite2DObject("quadlow", Vector3(-230, -200, 0.0f), Vector3(32.0f, 24.0f, 0.0f), true);
	HealthBar[2]->SetActive(false);

	StaminaBar[0] = Create::Sprite2DObject("quadhigh", Vector3(230, -200, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	StaminaBar[0]->SetActive(false);
	StaminaBar[1] = Create::Sprite2DObject("quadmed", Vector3(230, -200, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	StaminaBar[1]->SetActive(false);
	StaminaBar[2] = Create::Sprite2DObject("quadlow", Vector3(230, -200, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	StaminaBar[2]->SetActive(false);

	for (int i = 0; i < 10; ++i)
	{
		GunDamage[i] = Create::Sprite2DObject("GunStats", Vector3(30, -154, 0.0f), Vector3(15.0f, 18.0f, 0.0f), true);
		GunDamage[i]->SetActive(false);
	}

	for (int i = 0; i < 10; ++i)
	{
		GunFirerate[i] = Create::Sprite2DObject("GunStats", Vector3(105, -179, 0.0f), Vector3(15.0f, 18.0f, 0.0f), true);
		GunFirerate[i]->SetActive(false);
	}

	for (int i = 0; i < 10; ++i)
	{
		GunRecoil[i] = Create::Sprite2DObject("GunStats", Vector3(30, -204, 0.0f), Vector3(15.0f, 18.0f, 0.0f), true);
		GunRecoil[i]->SetActive(false);
	}

	for (int i = 0; i < 10; ++i)
	{
		GunScope[i] = Create::Sprite2DObject("GunStats", Vector3(30, -229, 0.0f), Vector3(15.0f, 18.0f, 0.0f), true);
		GunScope[i]->SetActive(false);
	}

	for (int i = 0; i < 10; ++i)
	{
		GunWeight[i] = Create::Sprite2DObject("GunStats", Vector3(30, -254, 0.0f), Vector3(15.0f, 18.0f, 0.0f), true);
		GunWeight[i]->SetActive(false);
	}

	for (int i = 0; i < 10; ++i)
	{
		GunMag[i] = Create::Sprite2DObject("GunStats", Vector3(130, -279, 0.0f), Vector3(15.0f, 18.0f, 0.0f), true);
		GunMag[i]->SetActive(false);
	}
	
	ScoutAblity[0] = Create::Sprite2DObject("ScoutAbilityReady", Vector3(0, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	ScoutAblity[0]->SetActive(false);

	ScoutAblity[1] = Create::Sprite2DObject("ScoutAbilityDown", Vector3(0, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	ScoutAblity[1]->SetActive(false);

	TankAblity[0] = Create::Sprite2DObject("TankAbilityReady", Vector3(0, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	TankAblity[0]->SetActive(false);

	TankAblity[1] = Create::Sprite2DObject("TankAbilityDown", Vector3(0, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	TankAblity[1]->SetActive(false);

	ScavengerAblity[0] = Create::Sprite2DObject("ScavengerAbilityReady", Vector3(0, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	ScavengerAblity[0]->SetActive(false);

	ScavengerAblity[1] = Create::Sprite2DObject("ScavengerAbilityDown", Vector3(0, -265, 0.0f), Vector3(32.0f, 32.0f, 0.0f), true);
	ScavengerAblity[1]->SetActive(false);


	ShootingRange();

	//sprite animation
	SpriteAnimation* sa = dynamic_cast<SpriteAnimation*>(MeshBuilder::GetInstance()->GetMesh("Explosion"));
	if (sa)
	{
		sa->m_anim = new Animation();
		sa->m_anim->Set(0, 4, 0, 1.f, true);
	}

	// Hardware Abstraction
	theKeyboard = new CKeyboard();
	theKeyboard->Create(playerInfo);

	// Activate the Blend Function
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// Minimap
	theMinimap = Create::Minimap(false);
	theMinimap->SetBackground(MeshBuilder::GetInstance()->GenerateQuad("MINIMAP", Color(1, 1, 1), 1.f));
	theMinimap->GetBackground()->textureID = LoadTGA("Image//ground.tga");
	theMinimap->SetBorder(MeshBuilder::GetInstance()->GenerateCircle("MINIMAPBORDER", Color(1, 1, 1), 1.05f));
	theMinimap->SetAvatar(MeshBuilder::GetInstance()->GenerateQuad("MINIMAPAVATAR", Color(1, 1, 0), 0.25f));
	theMinimap->GetAvatar()->textureID = LoadTGA("Image//Avatar.tga");
	theMinimap->SetStencil(MeshBuilder::GetInstance()->GenerateCircle("MINIMAP_STENCIL", Color(1, 1, 1), 1.0f));

	// CameraEffects
	theCameraEffects = Create::CameraEffects(false);
	theCameraEffects->SetBloodScreen(MeshBuilder::GetInstance()->GenerateQuad("CAMERAEFFECTS_BLOODSCREEN", Color(1, 1, 1), 1.f));
	theCameraEffects->GetBloodScreen()->textureID = LoadTGA("Image//CameraEffects_Blood.tga");
	theCameraEffects->SetScope(MeshBuilder::GetInstance()->GenerateQuad("Scope", Color(1, 1, 1), 1.f));
	theCameraEffects->GetScope()->textureID = LoadTGA("Image//sniperScope.tga");
	theCameraEffects->SetRailgunScope(MeshBuilder::GetInstance()->GenerateQuad("RailScope", Color(1, 1, 1), 1.f));
	theCameraEffects->GetRailScope()->textureID = LoadTGA("Image//RailgunScope.tga");
	theCameraEffects->SetSmokeScreen(MeshBuilder::GetInstance()->GenerateQuad("SmokeScreen", Color(1, 1, 1), 1.f));
	theCameraEffects->GetSmokeScreen()->textureID = LoadTGA("Image//SmokeScreen.tga");
	theCameraEffects->SetStatus_BloodScreen(false);
	theCameraEffects->SetStatus_Scope(false);
	theCameraEffects->SetStatus_RailgunScope(false);
	theCameraEffects->SetStatus_SmokeScreen(false);

	// Hardware Abstraction
	theKeyboard = new CKeyboard();
	theKeyboard->Create(playerInfo);

	theMouse = new CMouse();
	theMouse->Create(playerInfo);

	GunIndex = 1;
	MenuIndex = 1;
	EnterShop = false;
	rotation = 0;
	Enemycount = 0;

	damage = 0;
	fireRate = 0;
	recoil = 0;
	scope = 0;
	weight = 0;
	magazine = 0;

	m_gravity.Set(0, -9.8f, 0);
	m_particleCount = 0;
	MAX_PARTICLE = 100;
	Rain = false;
	menu = true;

	ReplaceNeeded = false;
	ReplacementIndex = 1;

	ShopTab = 1;
	EquipmentIndex = 1;

	EnemySpawntimer = 2;
	EnemySpawntimer2 = 2;
	EnemySpawntimer3 = 2;

	EnemySpawntimer4 = 2;

	waves = 1;

	preparetimer = 20;

	Enemycount = 0;

	Area1temp = 0;
	Area2temp = 0;
	Area3temp = 0;
	Area4temp = 0;


	Area1WaveSpawned = false;
	Area2WaveSpawned = false;
	Area3WaveSpawned = false;
	Area4WaveSpawned = false;

	Area1Unlocked = false;
	Area2Unlocked = false;
	Area3Unlocked = false;
	Area4Unlocked = false;

	Area4Surprise = false;

	countdownTimer = 120;

	ClassSelect = false;

	EnterControl = false;

	EnterHighScore = false;

	ClassIndex = 1;

	WinGame = false;

	Math::InitRNG();
}

void SceneText::SpawnArea1Waves(float dt)
{
	if (Area1temp < waves)
	{
		EnemySpawntimer -= (float)(1 * dt);

		if (EnemySpawntimer <= 0)
		{
			int rng = Math::RandIntMinMax(1, 3);
			Vector3 pos;

			switch (rng)
			{
			case 1:
				pos.Set(300, 0, -400);
				break;
			case 2:
				pos.Set(350, 0, -200);
				break;
			case 3:
				pos.Set(60, 0, -420);
				break;
			default:
				break;
			}

			BasicEnemy = Create::Enemy3D("BasicEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			BasicEnemy->enemyType = BasicEnemy->Basic;
			BasicEnemy->Init();
			BasicEnemy->SetPos(pos);
			BasicEnemy->SetIsEnemy(true);
			BasicEnemy->SetisBullet(false);
			BasicEnemy->SetCollider(true);
			BasicEnemy->SetTerrain(groundEntity);
			BasicEnemy->SetAABB(Vector3(5, 16, 5), Vector3(-5, 0, -5));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(300, 0, -400);
				break;
			case 2:
				pos.Set(350, 0, -200);
				break;
			case 3:
				pos.Set(60, 0, -420);
				break;
			default:
				break;
			}

			TankEnemy = Create::Enemy3D("TankEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			TankEnemy->enemyType = TankEnemy->Tank;
			TankEnemy->Init();
			TankEnemy->SetPos(pos);
			TankEnemy->SetIsEnemy(true);
			TankEnemy->SetisBullet(false);
			TankEnemy->SetCollider(true);
			TankEnemy->SetTerrain(groundEntity);
			TankEnemy->SetAABB(Vector3(8, 20, 8), Vector3(-8, 0, -8));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(300, 0, -400);
				break;
			case 2:
				pos.Set(350, 0, -200);
				break;
			case 3:
				pos.Set(60, 0, -420);
				break;
			default:
				break;
			}

			SpeedyEnemy = Create::Enemy3D("SpeedyEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			SpeedyEnemy->enemyType = SpeedyEnemy->Speed;
			SpeedyEnemy->Init();
			SpeedyEnemy->SetPos(pos);
			SpeedyEnemy->SetIsEnemy(true);
			SpeedyEnemy->SetisBullet(false);
			SpeedyEnemy->SetCollider(true);
			SpeedyEnemy->SetTerrain(groundEntity);
			SpeedyEnemy->SetAABB(Vector3(6, 8, 8), Vector3(-6, 0, -8));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(300, 0, -400);
				break;
			case 2:
				pos.Set(350, 0, -200);
				break;
			case 3:
				pos.Set(60, 0, -420);
				break;
			default:
				break;
			}

			AmmoSnatcher = Create::Enemy3D("AmmoSnatcher", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			AmmoSnatcher->enemyType = AmmoSnatcher->Snatch;
			AmmoSnatcher->Init();
			AmmoSnatcher->SetPos(pos);
			AmmoSnatcher->SetIsEnemy(true);
			AmmoSnatcher->SetisBullet(false);
			AmmoSnatcher->SetCollider(true);
			AmmoSnatcher->SetTerrain(groundEntity);
			AmmoSnatcher->SetAABB(Vector3(9, 12, 8), Vector3(-9, 0, -8));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(300, 0, -400);
				break;
			case 2:
				pos.Set(350, 0, -200);
				break;
			case 3:
				pos.Set(60, 0, -420);
				break;
			default:
				break;
			}

			FlyingEnemy = Create::Enemy3D("FlyingEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			FlyingEnemy->enemyType = FlyingEnemy->FlyBoi;
			FlyingEnemy->Init();
			FlyingEnemy->SetPos(pos);
			FlyingEnemy->SetIsEnemy(true);
			FlyingEnemy->SetisBullet(false);
			FlyingEnemy->SetCollider(true);
			FlyingEnemy->SetTerrain(groundEntity);
			FlyingEnemy->SetAABB(Vector3(8, 15, 8), Vector3(-8, 0, -8));

			Enemycount += 5;
			EnemySpawntimer = 2;
			Area1temp++;
		}
	}
	else
	{
		Area1WaveSpawned = true;
	}
}

void SceneText::SpawnArea2Waves(float dt)
{
	if (Area2temp < waves)
	{
		EnemySpawntimer2 -= (float)(1 * dt);

		if (EnemySpawntimer2 <= 0)
		{
			int rng = Math::RandIntMinMax(1, 3);
			Vector3 pos;

			switch (rng)
			{
			case 1:
				pos.Set(270, 0, 230);
				break;
			case 2:
				pos.Set(430, 0, 200);
				break;
			case 3:
				pos.Set(350, 0, 400);
				break;
			default:
				break;
			}

			BasicEnemy = Create::Enemy3D("BasicEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			BasicEnemy->enemyType = BasicEnemy->Basic;
			BasicEnemy->Init();
			BasicEnemy->SetPos(pos);
			BasicEnemy->SetIsEnemy(true);
			BasicEnemy->SetisBullet(false);
			BasicEnemy->SetCollider(true);
			BasicEnemy->SetTerrain(groundEntity);
			BasicEnemy->SetAABB(Vector3(5, 16, 5), Vector3(-5, 0, -5));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(270, 0, 230);
				break;
			case 2:
				pos.Set(430, 0, 200);
				break;
			case 3:
				pos.Set(350, 0, 400);
				break;
			default:
				break;
			}

			TankEnemy = Create::Enemy3D("TankEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			TankEnemy->enemyType = TankEnemy->Tank;
			TankEnemy->Init();
			TankEnemy->SetPos(pos);
			TankEnemy->SetIsEnemy(true);
			TankEnemy->SetisBullet(false);
			TankEnemy->SetCollider(true);
			TankEnemy->SetTerrain(groundEntity);
			TankEnemy->SetAABB(Vector3(8, 20, 8), Vector3(-8, 0, -8));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(270, 0, 230);
				break;
			case 2:
				pos.Set(430, 0, 200);
				break;
			case 3:
				pos.Set(350, 0, 400);
				break;
			default:
				break;
			}

			SpeedyEnemy = Create::Enemy3D("SpeedyEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			SpeedyEnemy->enemyType = SpeedyEnemy->Speed;
			SpeedyEnemy->Init();
			SpeedyEnemy->SetPos(pos);
			SpeedyEnemy->SetIsEnemy(true);
			SpeedyEnemy->SetisBullet(false);
			SpeedyEnemy->SetCollider(true);
			SpeedyEnemy->SetTerrain(groundEntity);
			SpeedyEnemy->SetAABB(Vector3(6, 8, 8), Vector3(-6, 0, -8));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(270, 0, 230);
				break;
			case 2:
				pos.Set(430, 0, 200);
				break;
			case 3:
				pos.Set(350, 0, 400);
				break;
			default:
				break;
			}

			AmmoSnatcher = Create::Enemy3D("AmmoSnatcher", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			AmmoSnatcher->enemyType = AmmoSnatcher->Snatch;
			AmmoSnatcher->Init();
			AmmoSnatcher->SetPos(pos);
			AmmoSnatcher->SetIsEnemy(true);
			AmmoSnatcher->SetisBullet(false);
			AmmoSnatcher->SetCollider(true);
			AmmoSnatcher->SetTerrain(groundEntity);
			AmmoSnatcher->SetAABB(Vector3(9, 12, 8), Vector3(-9, 0, -8));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(270, 0, 230);
				break;
			case 2:
				pos.Set(430, 0, 200);
				break;
			case 3:
				pos.Set(350, 0, 400);
				break;
			default:
				break;
			}

			FlyingEnemy = Create::Enemy3D("FlyingEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			FlyingEnemy->enemyType = FlyingEnemy->FlyBoi;
			FlyingEnemy->Init();
			FlyingEnemy->SetPos(pos);
			FlyingEnemy->SetIsEnemy(true);
			FlyingEnemy->SetisBullet(false);
			FlyingEnemy->SetCollider(true);
			FlyingEnemy->SetTerrain(groundEntity);
			FlyingEnemy->SetAABB(Vector3(8, 15, 8), Vector3(-8, 0, -8));

			Enemycount += 5;
			EnemySpawntimer2 = 2;
			Area2temp++;
		}
	}
	else
	{
		Area2WaveSpawned = true;
	}
}

void SceneText::SpawnArea3Waves(float dt)
{
	if (Area3temp < waves)
	{
		EnemySpawntimer3 -= (float)(1 * dt);

		if (EnemySpawntimer3 <= 0)
		{
			int rng = Math::RandIntMinMax(1, 3);
			Vector3 pos;

			switch (rng)
			{
			case 1:
				pos.Set(-340, 0, 320);
				break;
			case 2:
				pos.Set(-60, 0, 300);
				break;
			case 3:
				pos.Set(-470, 0, 440);
				break;
			default:
				break;
			}

			BasicEnemy = Create::Enemy3D("BasicEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			BasicEnemy->enemyType = BasicEnemy->Basic;
			BasicEnemy->Init();
			BasicEnemy->SetPos(pos);
			BasicEnemy->SetIsEnemy(true);
			BasicEnemy->SetisBullet(false);
			BasicEnemy->SetCollider(true);
			BasicEnemy->SetTerrain(groundEntity);
			BasicEnemy->SetAABB(Vector3(5, 16, 5), Vector3(-5, 0, -5));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(-340, 0, 320);
				break;
			case 2:
				pos.Set(-60, 0, 300);
				break;
			case 3:
				pos.Set(-470, 0, 440);
				break;
			default:
				break;
			}

			TankEnemy = Create::Enemy3D("TankEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			TankEnemy->enemyType = TankEnemy->Tank;
			TankEnemy->Init();
			TankEnemy->SetPos(pos);
			TankEnemy->SetIsEnemy(true);
			TankEnemy->SetisBullet(false);
			TankEnemy->SetCollider(true);
			TankEnemy->SetTerrain(groundEntity);
			TankEnemy->SetAABB(Vector3(8, 20, 8), Vector3(-8, 0, -8));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(-340, 0, 320);
				break;
			case 2:
				pos.Set(-60, 0, 300);
				break;
			case 3:
				pos.Set(-470, 0, 440);
				break;
			default:
				break;
			}

			SpeedyEnemy = Create::Enemy3D("SpeedyEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			SpeedyEnemy->enemyType = SpeedyEnemy->Speed;
			SpeedyEnemy->Init();
			SpeedyEnemy->SetPos(pos);
			SpeedyEnemy->SetIsEnemy(true);
			SpeedyEnemy->SetisBullet(false);
			SpeedyEnemy->SetCollider(true);
			SpeedyEnemy->SetTerrain(groundEntity);
			SpeedyEnemy->SetAABB(Vector3(6, 8, 8), Vector3(-6, 0, -8));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(-340, 0, 320);
				break;
			case 2:
				pos.Set(-60, 0, 300);
				break;
			case 3:
				pos.Set(-470, 0, 440);
				break;
			default:
				break;
			}

			AmmoSnatcher = Create::Enemy3D("AmmoSnatcher", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			AmmoSnatcher->enemyType = AmmoSnatcher->Snatch;
			AmmoSnatcher->Init();
			AmmoSnatcher->SetPos(pos);
			AmmoSnatcher->SetIsEnemy(true);
			AmmoSnatcher->SetisBullet(false);
			AmmoSnatcher->SetCollider(true);
			AmmoSnatcher->SetTerrain(groundEntity);
			AmmoSnatcher->SetAABB(Vector3(9, 12, 8), Vector3(-9, 0, -8));

			rng = Math::RandIntMinMax(1, 3);

			switch (rng)
			{
			case 1:
				pos.Set(-340, 0, 320);
				break;
			case 2:
				pos.Set(-60, 0, 300);
				break;
			case 3:
				pos.Set(-470, 0, 440);
				break;
			default:
				break;
			}

			FlyingEnemy = Create::Enemy3D("FlyingEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			FlyingEnemy->enemyType = FlyingEnemy->FlyBoi;
			FlyingEnemy->Init();
			FlyingEnemy->SetPos(pos);
			FlyingEnemy->SetIsEnemy(true);
			FlyingEnemy->SetisBullet(false);
			FlyingEnemy->SetCollider(true);
			FlyingEnemy->SetTerrain(groundEntity);
			FlyingEnemy->SetAABB(Vector3(8, 15, 8), Vector3(-8, 0, -8));

			Enemycount += 5;
			EnemySpawntimer3 = 2;
			Area3temp++;
		}
	}
	else
	{
		Area3WaveSpawned = true;
	}
}

void SceneText::SpawnArea4Waves(float dt)
{
		EnemySpawntimer4 -= (float)(1 * dt);

		countdownTimer -= (float)(1 * dt);

		if (EnemySpawntimer4 <= 0)
		{
			int rng = Math::RandIntMinMax(1, 5);
			Vector3 pos;

			switch (rng)
			{
			case 1:
				pos.Set(-400, 0, -160);
				break;
			case 2:
				pos.Set(-405, 0, 90);
				break;
			case 3:
				pos.Set(-170, 0, -143);
				break;
			case 4:
				pos.Set(-236, 0, 136);
				break;
			case 5:
				pos.Set(-460, 0, -13);
				break;
			default:
				break;
			}

			BasicEnemy = Create::Enemy3D("BasicEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			BasicEnemy->enemyType = BasicEnemy->Basic;
			BasicEnemy->Init();
			BasicEnemy->SetPos(pos);
			BasicEnemy->SetIsEnemy(true);
			BasicEnemy->SetisBullet(false);
			BasicEnemy->SetCollider(true);
			BasicEnemy->SetTerrain(groundEntity);
			BasicEnemy->SetAABB(Vector3(5, 16, 5), Vector3(-5, 0, -5));

			rng = Math::RandIntMinMax(1, 5);

			switch (rng)
			{
			case 1:
				pos.Set(-400, 0, -160);
				break;
			case 2:
				pos.Set(-405, 0, 90);
				break;
			case 3:
				pos.Set(-170, 0, -143);
				break;
			case 4:
				pos.Set(-236, 0, 136);
				break;
			case 5:
				pos.Set(-460, 0, -13);
				break;
			default:
				break;
			}

			TankEnemy = Create::Enemy3D("TankEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			TankEnemy->enemyType = TankEnemy->Tank;
			TankEnemy->Init();
			TankEnemy->SetPos(pos);
			TankEnemy->SetIsEnemy(true);
			TankEnemy->SetisBullet(false);
			TankEnemy->SetCollider(true);
			TankEnemy->SetTerrain(groundEntity);
			TankEnemy->SetAABB(Vector3(8, 20, 8), Vector3(-8, 0, -8));

			rng = Math::RandIntMinMax(1, 5);

			switch (rng)
			{
			case 1:
				pos.Set(-400, 0, -160);
				break;
			case 2:
				pos.Set(-405, 0, 90);
				break;
			case 3:
				pos.Set(-170, 0, -143);
				break;
			case 4:
				pos.Set(-236, 0, 136);
				break;
			case 5:
				pos.Set(-460, 0, -13);
				break;
			default:
				break;
			}

			SpeedyEnemy = Create::Enemy3D("SpeedyEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			SpeedyEnemy->enemyType = SpeedyEnemy->Speed;
			SpeedyEnemy->Init();
			SpeedyEnemy->SetPos(pos);
			SpeedyEnemy->SetIsEnemy(true);
			SpeedyEnemy->SetisBullet(false);
			SpeedyEnemy->SetCollider(true);
			SpeedyEnemy->SetTerrain(groundEntity);
			SpeedyEnemy->SetAABB(Vector3(6, 8, 8), Vector3(-6, 0, -8));

			rng = Math::RandIntMinMax(1, 5);

			switch (rng)
			{
			case 1:
				pos.Set(-400, 0, -160);
				break;
			case 2:
				pos.Set(-405, 0, 90);
				break;
			case 3:
				pos.Set(-170, 0, -143);
				break;
			case 4:
				pos.Set(-236, 0, 136);
				break;
			case 5:
				pos.Set(-460, 0, -13);
				break;
			default:
				break;
			}

			AmmoSnatcher = Create::Enemy3D("AmmoSnatcher", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			AmmoSnatcher->enemyType = AmmoSnatcher->Snatch;
			AmmoSnatcher->Init();
			AmmoSnatcher->SetPos(pos);
			AmmoSnatcher->SetIsEnemy(true);
			AmmoSnatcher->SetisBullet(false);
			AmmoSnatcher->SetCollider(true);
			AmmoSnatcher->SetTerrain(groundEntity);
			AmmoSnatcher->SetAABB(Vector3(9, 12, 8), Vector3(-9, 0, -8));

			rng = Math::RandIntMinMax(1, 5);

			switch (rng)
			{
			case 1:
				pos.Set(-400, 0, -160);
				break;
			case 2:
				pos.Set(-405, 0, 90);
				break;
			case 3:
				pos.Set(-170, 0, -143);
				break;
			case 4:
				pos.Set(-236, 0, 136);
				break;
			case 5:
				pos.Set(-460, 0, -13);
				break;
			default:
				break;
			}

			FlyingEnemy = Create::Enemy3D("FlyingEnemy", Vector3(0.0f, 0.0f, 0.0f), Vector3(2, 2, 2));
			FlyingEnemy->enemyType = FlyingEnemy->FlyBoi;
			FlyingEnemy->Init();
			FlyingEnemy->SetPos(pos);
			FlyingEnemy->SetIsEnemy(true);
			FlyingEnemy->SetisBullet(false);
			FlyingEnemy->SetCollider(true);
			FlyingEnemy->SetTerrain(groundEntity);
			FlyingEnemy->SetAABB(Vector3(8, 15, 8), Vector3(-8, 0, -8));

			Enemycount += 5;
			EnemySpawntimer4 = 5;
		}
}

void SceneText::Boundary()
{
	for (float i = 0; i < 1000; i += 37.6)
	{
		wall = Create::Entity("Wall2", Vector3(-490 + i, -10, 500), Vector3(1, 1, 1));
		wall->SetAABB(Vector3(20, 20, 5), Vector3(-20, -20, -5));

		wall = Create::Entity("Wall2", Vector3(500, -10, -485 + i), Vector3(1, 1, 1));
		wall->SetAABB(Vector3(5, 20, 20), Vector3(-5, -20, -20));
		wall->angle = 90;

		wall = Create::Entity("Wall2", Vector3(-490 + i, -10, -500), Vector3(1, 1, 1));
		wall->SetAABB(Vector3(20, 20, 5), Vector3(-20, -20, -5));
		wall->angle = 180;

		wall = Create::Entity("Wall2", Vector3(-500, -10, -485 + i), Vector3(1, 1, 1));
		wall->SetAABB(Vector3(5, 20, 20), Vector3(-5, -20, -20));
		wall->angle = 270;
	}

	//Shooting Range
	for (float i = 0; i < 130; i += 37.6)
	{
		wall = Create::Entity("Wall2", Vector3(-197, -10, -485 + i), Vector3(1, 1, 1));
		wall->SetAABB(Vector3(5, 20, 20), Vector3(-5, -20, -20));
		wall->angle = 90;

		wall = Create::Entity("Wall2", Vector3(-197, -10, -200 - i), Vector3(1, 1, 1));
		wall->SetAABB(Vector3(5, 20, 20), Vector3(-5, -20, -20));
		wall->angle = 90;
	}

	//Dividers
	//Shooting Range, Area 1 | Area 2, 3, 4
	for (float i = 0; i < 960; i += 37.6)
	{
		wall = Create::Entity("Wall2", Vector3(-478 + i, -10, -182), Vector3(1, 1, 1));
		wall->SetAABB(Vector3(20, 20, 5), Vector3(-20, -20, -5));
	}

	//Area 2 | Area 3, 4
	for (float i = 0; i < 630; i += 37.6)
	{
		wall = Create::Entity("Wall2", Vector3(200, -10, -163 + i), Vector3(1, 1, 1));
		wall->SetAABB(Vector3(5, 20, 20), Vector3(-5, -20, -20));
		wall->angle = 90;
	}

	//Area 3 | Area 4
	for (float i = 0; i < 660; i += 37.6)
	{
		wall = Create::Entity("Wall2", Vector3(183 - i, -10, 163), Vector3(1, 1, 1));
		wall->SetAABB(Vector3(20, 20, 5), Vector3(-20, -20, -5));
	}

	//gates
	gate[0] = Create::Entity("Gate", Vector3(-197, -10, -331), Vector3(0.8, 1.5, 0.8));
	gate[0]->CanInteract = true;
	gate[0]->SetInteractionAABB(Vector3(10, 20, 0), Vector3(-10, -20, -20));
	gate[0]->SetAABB(Vector3(5, 20, 0), Vector3(-5, -20, -20));
	gate[0]->angle = 90;

	gate[1] = Create::Entity("Gate", Vector3(480, -10, -182), Vector3(0.7, 1.5, 1.5));
	gate[1]->CanInteract = true;
	gate[1]->SetInteractionAABB(Vector3(20, 20, 10), Vector3(0, -20, -10));
	gate[1]->SetAABB(Vector3(20, 20, 5), Vector3(0, -20, -5));

	gate[2] = Create::Entity("Gate", Vector3(200, -9, 497), Vector3(1.5, 1.5, 1.5));
	gate[2]->CanInteract = true;
	gate[2]->SetInteractionAABB(Vector3(10, 20, 0), Vector3(-10, -20, -40));
	gate[2]->SetAABB(Vector3(5, 20, 0), Vector3(-5, -20, -40));
	gate[2]->angle = 90;

	gate[3] = Create::Entity("Gate", Vector3(-496, -10, 163), Vector3(0.7, 1.5, 1.5));
	gate[3]->CanInteract = true;
	gate[3]->SetInteractionAABB(Vector3(20, 20, 10), Vector3(0, -20, -10));
	gate[3]->SetAABB(Vector3(20, 20, 5), Vector3(0, -20, -5));
}

void SceneText::ShootingRange()
{
	for (int i = 0; i < 180; i += 60)
	{
		Target = Create::Enemy3D("Target", Vector3(0, 0, 0), Vector3(2, 2, 2));
		Target->enemyType = Target->Target;
		Target->Init();
		Target->targetState = Target->targetIdleL;
		Target->SetIsEnemy(true);
		Target->SetisBullet(false);
		Target->SetCollider(true);
		Target->SetTerrain(groundEntity);
		Target->SetPos(Vector3(-420 + i, 0, -420));
		Target->SetAABB(Vector3(5, 20, 10), Vector3(-5, -20, -5));

		Target = Create::Enemy3D("Target", Vector3(0, 0, 0), Vector3(2, 2, 2));
		Target->enemyType = Target->Target;
		Target->Init();
		Target->targetState = Target->targetIdleR;
		Target->SetIsEnemy(true);
		Target->SetisBullet(false);
		Target->SetCollider(true);
		Target->SetTerrain(groundEntity);
		Target->SetPos(Vector3(-390 + i, 0, -280));
		Target->SetAABB(Vector3(5, 20, 10), Vector3(-5, -20, -5));
	}

	for (int i = 0; i < 200; i += 10)
	{
		wall = Create::Entity("Wall", Vector3(-440 + i, -10, -450), Vector3(1, 0.8, 1));
		wall->SetAABB(Vector3(10, 5, 5), Vector3(-10, -10, -5));

		wall = Create::Entity("Wall", Vector3(-440 + i, -10, -250), Vector3(1, 0.8, 1));
		wall->SetAABB(Vector3(10, 5, 5), Vector3(-10, -10, -5));

		wall = Create::Entity("Wall", Vector3(-445, -10, -445 + i), Vector3(1, 0.8, 1));
		wall->SetAABB(Vector3(10, 5, 5), Vector3(-10, -10, -5));
		wall->angle = 90;

		wall = Create::Entity("Wall", Vector3(-245, -10, -445 + i), Vector3(1, 0.8, 1));
		wall->SetAABB(Vector3(10, 5, 5), Vector3(-10, -10, -5));
		wall->angle = 90;
	}
}

void SceneText::AreaOne()
{
	for (int i = 0; i < 160; i += 40)
	{
		cabin = Create::Entity("Cabin", Vector3(-100 + i, -5, -450), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));

		cabin = Create::Entity("Cabin", Vector3(100 + i, -5, -450), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));

		cabin = Create::Entity("Cabin", Vector3(-100 + i, -5, -250), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));
		cabin->angle = 180;

		cabin = Create::Entity("Cabin", Vector3(100 + i, -5, -250), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));
		cabin->angle = 180;
	}

	for (int i = 0; i < 160; i += 40)
	{
		cabin = Create::Entity("Cabin", Vector3(365, -5, -400 + i), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));
		cabin->angle = 270;
	}


	VendingMachine[0] = Create::Entity("VendingMachine", Vector3(-164, -10, -190), Vector3(5, 5, 5));
	VendingMachine[0]->CanInteract = true;
	VendingMachine[0]->SetInteractionAABB(Vector3(10, 20, 10), Vector3(-10, 0, -14));
	VendingMachine[0]->SetAABB(Vector3(10, 20, 14), Vector3(-10, 0, -12));
	VendingMachine[0]->angle = 180;

}

void SceneText::AreaTwo()
{
	for (int i = 0; i < 280; i += 40)
	{
		cabin = Create::Entity("Cabin", Vector3(225, -5, 0 + i), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));
		cabin->angle = 90;

		cabin = Create::Entity("Cabin", Vector3(475, -5, 0 + i), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));
		cabin->angle = 270;
	}

	for (int i = 0; i < 80; i += 40)
	{
		cabin = Create::Entity("Cabin", Vector3(435 - i, -5, -40), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));

		cabin = Create::Entity("Cabin", Vector3(265 + i, -5, -40), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));

		cabin = Create::Entity("Cabin", Vector3(435 - i, -5, 280), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));
		cabin->angle = 180;

		cabin = Create::Entity("Cabin", Vector3(265 + i, -5, 280), Vector3(1, 1, 1));
		cabin->SetAABB(Vector3(25, 50, 25), Vector3(-25, -50, -25));
		cabin->angle = 180;
	}

	well = Create::Entity("Well", Vector3(350, -10, 120), Vector3(0.3, 0.3, 0.3));
	well->SetAABB(Vector3(20, 30, 20), Vector3(-20, -30, -20));

	VendingMachine[1] = Create::Entity("VendingMachine", Vector3(207, -10, -96), Vector3(5, 5, 5));
	VendingMachine[1]->CanInteract = true;
	VendingMachine[1]->SetInteractionAABB(Vector3(14, 20, 10), Vector3(-10, 0, -10));
	VendingMachine[1]->SetAABB(Vector3(12, 20, 10), Vector3(-10, 0, -10));
	VendingMachine[1]->angle = 90;
}

void SceneText::AreaThree()
{
	for (int i = 0; i < 520; i += 40)
	{
		stall = Create::Entity("Stall", Vector3(-400 + i, -10, 180), Vector3(0.3, 0.3, 0.3));
		stall->SetAABB(Vector3(20, 50, 20), Vector3(-20, -50, -20));

		stall = Create::Entity("Stall", Vector3(-400 + i, -10, 480), Vector3(0.3, 0.3, 0.3));
		stall->SetAABB(Vector3(20, 50, 20), Vector3(-20, -50, -20));
		stall->angle = 180;
	}

	for (int i = 0; i < 200; i += 40)
	{
		stall = Create::Entity("Stall", Vector3(-400 + i, -10, 250), Vector3(0.3, 0.3, 0.3));
		stall->SetAABB(Vector3(20, 50, 20), Vector3(-20, -50, -20));
		stall->angle = 180;

		stall = Create::Entity("Stall", Vector3(80 - i, -10, 410), Vector3(0.3, 0.3, 0.3));
		stall->SetAABB(Vector3(20, 50, 20), Vector3(-20, -50, -20));
	}

	for (int i = 0; i < 160; i += 40)
	{
		stall = Create::Entity("Stall", Vector3(-400, -10, 405 - i), Vector3(0.3, 0.3, 0.3));
		stall->SetAABB(Vector3(20, 50, 20), Vector3(-20, -50, -20));
		stall->angle = 270;

		stall = Create::Entity("Stall", Vector3(80, -10, 255 + i), Vector3(0.3, 0.3, 0.3));
		stall->SetAABB(Vector3(20, 50, 20), Vector3(-20, -50, -20));
		stall->angle = 90;
	}

	blacksmith = Create::Entity("Blacksmith", Vector3(161, -10, 210), Vector3(0.8, 0.8, 0.8));
	blacksmith->SetAABB(Vector3(35, 50, 30), Vector3(-30, -50, -30));
	blacksmith->angle = 270;

	VendingMachine[2] = Create::Entity("VendingMachine", Vector3(192, -10, 310), Vector3(5, 5, 5));
	VendingMachine[2]->CanInteract = true;
	VendingMachine[2]->SetInteractionAABB(Vector3(10, 20, 10), Vector3(-14, 0, -10));
	VendingMachine[2]->SetAABB(Vector3(10, 20, 10), Vector3(-12, 0, -10));
	VendingMachine[2]->angle = -90;

}

void SceneText::AreaFour()
{
	church = Create::Entity("Church", Vector3(90, -10, 0), Vector3(1.5, 1.5, 1.5));
	church->SetAABB(Vector3(90, 75, 45), Vector3(-90, -75, -45));
	church->angle = 270;

	VendingMachine[3] = Create::Entity("VendingMachine", Vector3(-45, -10, 156), Vector3(5, 5, 5));
	VendingMachine[3]->CanInteract = true;
	VendingMachine[3]->SetInteractionAABB(Vector3(10, 20, 10), Vector3(-10, 0, -14));
	VendingMachine[3]->SetAABB(Vector3(10, 20, 10), Vector3(-10, 0, -12));
	VendingMachine[3]->angle = 180;
}

void SceneText::RenderParticles(ParticleObject *particle)
{

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	switch (particle->type)
	{
	case ParticleObject_TYPE::P_WATER:
	{
		modelStack.PushMatrix();
		modelStack.Translate(particle->pos.x, particle->pos.y, particle->pos.z);
		modelStack.Rotate(AngleCalc(particle->pos.x, particle->pos.z) + 180, 0, 1, 0);
		modelStack.Rotate(particle->rotation, 0, 0, 1);
		modelStack.Scale(particle->scale.x, particle->scale.y, particle->scale.z);
		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("RainDrop"),0.5);
		modelStack.PopMatrix();
		break;
	}

	case ParticleObject_TYPE::P_SMOKE:
	{
		modelStack.PushMatrix();
		modelStack.Translate(particle->pos.x, particle->pos.y, particle->pos.z);
		modelStack.Rotate(AngleCalc(particle->pos.x, particle->pos.z) + 180, 0, 1, 0);
		modelStack.Rotate(particle->rotation, 0, 0, 1);
		modelStack.Scale(particle->scale.x, particle->scale.y, particle->scale.z);
		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("Smoke"), particle->disappeartimer);
		modelStack.PopMatrix();
		break;
	}
	case ParticleObject_TYPE::P_GAS:
	{
		modelStack.PushMatrix();
		modelStack.Translate(particle->pos.x, particle->pos.y, particle->pos.z);
		modelStack.Rotate(AngleCalc(particle->pos.x, particle->pos.z) + 180, 0, 1, 0);
		modelStack.Rotate(particle->rotation, 0, 0, 1);
		modelStack.Scale(particle->scale.x, particle->scale.y, particle->scale.z);
		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("Gas"), particle->disappeartimer);
		modelStack.PopMatrix();
		break;
	}
	}
}

//week 12 particles
ParticleObject* SceneText::GetParticle(void)
{
	for (std::vector<ParticleObject *>::iterator it = particleList.begin(); it != particleList.end(); ++it)
	{
		ParticleObject *particle = (ParticleObject *)*it;
		if (!particle->active)
		{
			particle->active = true;
			m_particleCount++;
			return particle;
		}
	}
	for (unsigned i = 0; i < 10; ++i)
	{
		ParticleObject *particle = new ParticleObject(ParticleObject_TYPE::P_WATER);
		particleList.push_back(particle);
	}
	ParticleObject *particle = particleList.back();
	particle->active = true;
	m_particleCount++;
	return particle;
}

void SceneText::UpdateParticles(double dt)
{
	if (m_particleCount < MAX_PARTICLE && weatherManager[0]->GetEffectActive())
	{
		weatherManager[0]->AddParticle(ParticleObject_TYPE::P_WATER);
	}

	if (m_particleCount < MAX_PARTICLE * 0.5 && weatherManager[1]->GetEffectActive())
	{
		weatherManager[1]->AddParticle(ParticleObject_TYPE::P_SMOKE);
	}

	if (m_particleCount < MAX_PARTICLE && weatherManager[2]->GetEffectActive())
	{
		weatherManager[2]->AddParticle(ParticleObject_TYPE::P_GAS);
	}

	weatherManager[0]->Update(dt);
	weatherManager[1]->Update(dt);
	weatherManager[2]->Update(dt);
}

float SceneText::AngleCalc(float x, float z)
{
	float xdistance = x - playerInfo->GetPos().x;
	float zdistance = z - playerInfo->GetPos().z;

	return Math::RadianToDegree(atan2(xdistance, zdistance));
}

void SceneText::Update(double dt)
{
	//temp test
	static bool isKeypressed = false;

	if (KeyboardController::GetInstance()->IsKeyPressed('Y'))
	{
		playerInfo->AddMoney(100);
	}

	//scavenger second perk
	if (playerInfo->characterClass == 2 && playerInfo->characterSkillLevel >= 2)
	{
		playerInfo->ScavengerDiscount = 0.5;
	}

	if (highscore->highScore <= playerInfo->score)
	{
		highscore->highScore = playerInfo->score;
	}

	//reset
	if (playerInfo->Health <= 0)
	{
		highscore->SetData(highscore->highScore);
		if (KeyboardController::GetInstance()->IsKeyPressed('R') && !isKeypressed)
		{
			Exit();
			Init();
			isKeypressed = true;
		}
	}

	else if (WinGame)
	{
		highscore->SetData(highscore->highScore);
		if (KeyboardController::GetInstance()->IsKeyPressed('R') && !isKeypressed)
		{
			Exit();
			Init();
			isKeypressed = true;
			WinGame = false;
		}
	}


	if (EnterShop)
	{
		if (KeyboardController::GetInstance()->IsKeyPressed('E') && !isKeypressed && !ReplaceNeeded)
		{
			EnterShop = false;
			isKeypressed = true;
		}
	}

	else
	{
		isKeypressed = false;
	}

	

	if (!(menu || EnterShop || WinGame) && playerInfo->Health > 0)
	{
		// Update our entities
		EntityManager::GetInstance()->Update(dt);

		weatherManager[0]->SetEffectTimer(weatherManager[0]->GetEffectTimer() + (float)(1 * dt));

		for (int i = 0; i < 3; ++i)
		{
			EntityManager::GetInstance()->Update(weatherManager[i], dt);
		}
		// Hardware Abstraction
		theKeyboard->Read(dt);
		theMouse->Read(dt);
		// Update the player position and other details based on keyboard and mouse inputs
		playerInfo->Update(dt);

		for (int i = 25; i <= 29; ++i)
		{
			textObj[i]->SetText("");
		}

		if (VendingMachine[0]->InInteractionRadius)
		{
			textObj[54]->SetText("Enter Shop?");
			if (KeyboardController::GetInstance()->IsKeyPressed('E') && !EnterShop && !isKeypressed)
			{
				playerInfo->scopedin = false;
				playerInfo->StopSway(dt);
				EnterShop = true;
				isKeypressed = true;
			}
		}

		else if (VendingMachine[1]->InInteractionRadius)
		{
			textObj[54]->SetText("Enter Shop?");
			if (KeyboardController::GetInstance()->IsKeyPressed('E') && !EnterShop && !isKeypressed)
			{
				playerInfo->scopedin = false;
				playerInfo->StopSway(dt);
				EnterShop = true;
				isKeypressed = true;
			}
		}

		else if (VendingMachine[2]->InInteractionRadius)
		{
			textObj[54]->SetText("Enter Shop?");
			if (KeyboardController::GetInstance()->IsKeyPressed('E') && !EnterShop && !isKeypressed)
			{
				playerInfo->scopedin = false;
				playerInfo->StopSway(dt);
				EnterShop = true;
				isKeypressed = true;
			}
		}

		else if (VendingMachine[3]->InInteractionRadius)
		{
			textObj[54]->SetText("Enter Shop?");
			if (KeyboardController::GetInstance()->IsKeyPressed('E') && !EnterShop && !isKeypressed)
			{
				playerInfo->scopedin = false;
				playerInfo->StopSway(dt);
				EnterShop = true;
				isKeypressed = true;
			}
		}

		else
		{
			textObj[54]->SetText("");
			isKeypressed = false;
		}



		if (gate[0]->InInteractionRadius)
		{
			textObj[48]->SetText("Exit tutorial?");
			if (KeyboardController::GetInstance()->IsKeyPressed('E') && preparetimer > 0 && !playerInfo->reload)
			{
				playerInfo->Intututorial = false;
				gate[0]->MoveGateZ = true;
				Area1Unlocked = true;
			}
		}

		else
		{
			textObj[48]->SetText("");
		}

		if (gate[1]->InInteractionRadius)
		{
			if (preparetimer > 0)
			{
				textObj[49]->SetText("Unlock for $100?");
			}
			else
			{
				textObj[49]->SetText("Complete the wave");
			}
			if (KeyboardController::GetInstance()->IsKeyPressed('E') && playerInfo->GetMoney() >= 100 && preparetimer > 0)
			{
				playerInfo->SubtractMoney(100);
				gate[1]->MoveGateX = true;
				Area2Unlocked = true;
			}
		}

		else
		{
			textObj[49]->SetText("");
		}

		if (gate[2]->InInteractionRadius)
		{
			if (preparetimer > 0)
			{
				textObj[50]->SetText("Unlock for $250");
			}
			else
			{
				textObj[50]->SetText("Complete the wave");
			}
			if (KeyboardController::GetInstance()->IsKeyPressed('E') && playerInfo->GetMoney() >= 250 && preparetimer > 0)
			{
				playerInfo->SubtractMoney(250);
				gate[2]->MoveGateZ = true;
				Area3Unlocked = true;
			}
		}

		else
		{
			textObj[50]->SetText("");
		}

		if (gate[3]->InInteractionRadius)
		{
			if (preparetimer > 0)
			{
				textObj[51]->SetText("Unlock for $400");
			}
			else
			{
				textObj[51]->SetText("Complete the wave");
			}
			if (KeyboardController::GetInstance()->IsKeyPressed('E') && playerInfo->GetMoney() >= 400 && preparetimer > 0)
			{
				playerInfo->SubtractMoney(400);
				gate[3]->MoveGateX = true;
				Area4Unlocked = true;
			}
		}



		else
		{
			textObj[51]->SetText("");
		}

		if(Area4Unlocked && playerInfo->GetPos().z < 140 && playerInfo->GetPos().z > 0 && playerInfo->GetPos().x < -400 && !Area4Surprise)
		{
			gate[3]->SurpriseMove = true;
			Area4Surprise = true;
		}

		if (Area1Unlocked)
		{
			preparetimer -= (float)(1 * dt);
		}

		////////////Wave spawning///////////////
		if (preparetimer <= 0)
		{
			if (Area1Unlocked && !Area4Unlocked)
			{
				SpawnArea1Waves(dt);
			}

			if (Area2Unlocked && !Area4Unlocked)
			{
				SpawnArea2Waves(dt);
			}

			if (Area3Unlocked && !Area4Unlocked)
			{
				SpawnArea3Waves(dt);
			}

			if (Area4Unlocked && Area4Surprise)
			{
				SpawnArea4Waves(dt);
			}

			
			if (Area1Unlocked && !Area2Unlocked && !Area3Unlocked && !Area4Unlocked)
			{
				if (Enemycount == 0 && Area1WaveSpawned)
				{
					waves++;
					preparetimer = 20;
					Area1temp = 0;
					Area1WaveSpawned = false;
				}
			}

			else if (Area2Unlocked && !Area3Unlocked && !Area4Unlocked)
			{
				if (Enemycount == 0 && Area1WaveSpawned && Area2WaveSpawned)
				{
					waves++;
					preparetimer = 20;
					Area1temp = 0;
					Area1WaveSpawned = false;
					Area2temp = 0;
					Area2WaveSpawned = false;
				}
			}

			else if (Area3Unlocked && !Area4Unlocked)
			{
				if (Enemycount == 0 && Area1WaveSpawned  && Area2WaveSpawned && Area3WaveSpawned)
				{
					waves++;
					preparetimer = 20;
					Area1temp = 0;
					Area1WaveSpawned = false;
					Area2temp = 0;
					Area2WaveSpawned = false;
					Area3temp = 0;
					Area3WaveSpawned = false;
				}
			}
		}
	}

	//victory condition
	if (countdownTimer <= 0)
	{
		WinGame = true;
	}
	//////////////////////////

	///////////Menu/////////////
	if (menu && !ClassSelect && !EnterControl && !EnterHighScore)
	{
		if (KeyboardController::GetInstance()->IsKeyPressed(VK_DOWN) && !isKeypressed)
		{
			MenuIndex = Math::Wrap(MenuIndex+1,1,4);
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_UP) && !isKeypressed)
		{
			MenuIndex = Math::Wrap(MenuIndex - 1, 1, 4);
			isKeypressed = true;
		}

		else if (MenuIndex == 1 && KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN))
		{
			ClassSelect = true;
			isKeypressed = true;
		}

		else if (MenuIndex == 2 && KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN))
		{
			EnterControl = true;
			isKeypressed = true;
		}

		else if (MenuIndex == 3 && KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN))
		{
			EnterHighScore = true;
			isKeypressed = true;
		}

		else if (MenuIndex == 4 && KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN))
		{
			menu = false;
			isKeypressed = true;
			Exit();
			exit(0);
		}

		else
		{
			isKeypressed = false;
		}

	}

	else if (ClassSelect)
	{
		if (KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN) && !isKeypressed)
		{
			switch (ClassIndex)
			{
			case 1:
				playerInfo->characterClass = playerInfo->SCOUT;
				break;
			case 2:
				playerInfo->characterClass = playerInfo->TANK;
				break;
			case 3:
				playerInfo->characterClass = playerInfo->SCAVENGER;
				break;
			default:
				break;
			}
			ClassSelect = false;
			menu = false;
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_DOWN) && !isKeypressed && ClassIndex < 3)
		{
			ClassIndex++;
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_UP) && !isKeypressed && ClassIndex > 1)
		{
			ClassIndex--;
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed('B') && !isKeypressed)
		{
			ClassSelect = false;
			isKeypressed = true;
		}

		else
		{
			isKeypressed = false;
		}
	}

	else if (EnterControl)
	{
		if (KeyboardController::GetInstance()->IsKeyPressed('B') && !isKeypressed)
		{
			EnterControl = false;
			isKeypressed = true;
		}

		else
		{
			isKeypressed = false;
		}
	}

	else if (EnterHighScore)
	{
		if (KeyboardController::GetInstance()->IsKeyPressed('B') && !isKeypressed)
		{
			EnterHighScore = false;
			isKeypressed = true;
		}
		else
		{
			isKeypressed = false;
		}
	}

	////////////////////////
	if (EnterShop && !ReplaceNeeded && ShopTab == 1)
	{
		rotation += (float)(10 * dt);

		if (KeyboardController::GetInstance()->IsKeyPressed(VK_DOWN) && !isKeypressed && GunIndex < 8)
		{
			GunIndex++;
			isKeypressed = true;
			rotation = 0;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_UP) && !isKeypressed && GunIndex > 1)
		{
			GunIndex--;
			isKeypressed = true;
			rotation = 0;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN) && !isKeypressed && !playerInfo->CheckFullInventory() && playerInfo->GetMoney() >= playerInfo->GetWeaponCost(GetGunindexName()))
		{
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				playerInfo->AddWeapon(GetGunindexName());
				playerInfo->SubtractMoney(playerInfo->GetWeaponCost(GetGunindexName()));
			}
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN) && playerInfo->CheckFullInventory() && !isKeypressed && playerInfo->GetMoney() >= playerInfo->GetWeaponCost(GetGunindexName()))
		{
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				ReplaceNeeded = true;
			}
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_RIGHT) && !isKeypressed)
		{
			rotation = 0;
			isKeypressed = true;
			ShopTab = 2;
		}


		else
		{
			isKeypressed = false;
		}

		textObj[30]->SetText("");
	}

	else if (ShopTab == 2 && EnterShop)
	{
		if (KeyboardController::GetInstance()->IsKeyPressed(VK_UP) && !isKeypressed && EquipmentIndex > 1)
		{
		EquipmentIndex--;
		isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_DOWN) && !isKeypressed && EquipmentIndex < 10)
		{
		EquipmentIndex++;
		isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN) && !isKeypressed && playerInfo->GetMoney() >= playerInfo->GetEquipmentCost(GetEquipmentName()))
		{
			playerInfo->UpdateAmmo(GetEquipmentName(), 1);
			playerInfo->AddItem(GetEquipmentName(), 1);
			if (EquipmentIndex < 9)
			{
				playerInfo->SubtractMoney(playerInfo->GetEquipmentCost(GetEquipmentName()));
			}
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_LEFT) && !isKeypressed)
		{
		rotation = 0;
		isKeypressed = true;
		ShopTab = 1;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_RIGHT) && !isKeypressed)
		{
			isKeypressed = true;
			ShopTab = 3;
		}

		else
		{
			isKeypressed = false;
		}

	}

	else if (ShopTab == 3 && EnterShop && !ReplaceNeeded)
	{

		if (KeyboardController::GetInstance()->IsKeyPressed(VK_LEFT) && !isKeypressed)
		{
			ShopTab = 2;
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN) && !isKeypressed && playerInfo->GetMoney() >= playerInfo->GetClassUpgradeCost() && playerInfo->characterSkillLevel < 3)
		{
			playerInfo->characterSkillLevel++;
			playerInfo->SubtractMoney(playerInfo->GetClassUpgradeCost());
			isKeypressed = true;
		}

		else
		{
			isKeypressed = false;
		}
	}

	else if (ReplaceNeeded)
	{
		if (KeyboardController::GetInstance()->IsKeyPressed(VK_RIGHT) && !isKeypressed && ReplacementIndex < 2)
		{
			ReplacementIndex++;
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_LEFT) && !isKeypressed && ReplacementIndex > 1)
		{
			ReplacementIndex--;
			isKeypressed = true;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN) && !isKeypressed && ReplacementIndex == 1)
		{
			playerInfo->ReplaceWeapon(GetGunindexName(), 1);
			playerInfo->SubtractMoney(playerInfo->GetWeaponCost(GetGunindexName()));
			isKeypressed = true;
			ReplaceNeeded = false;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed(VK_RETURN) && !isKeypressed && ReplacementIndex == 2)
		{
			playerInfo->ReplaceWeapon(GetGunindexName(), 2);
			playerInfo->SubtractMoney(playerInfo->GetWeaponCost(GetGunindexName()));
			isKeypressed = true;
			ReplaceNeeded = false;
		}

		else if (KeyboardController::GetInstance()->IsKeyPressed('B') && !isKeypressed)
		{
			ReplaceNeeded = false;
		}

		else
		{
			isKeypressed = false;
		}

		std::ostringstream ss13;
		ss13 << "Replace with?";
		textObj[30]->SetText(ss13.str());
	}


	// THIS WHOLE CHUNK TILL <THERE> CAN REMOVE INTO ENTITIES LOGIC! Or maybe into a scene function to keep the update clean
	if(KeyboardController::GetInstance()->IsKeyDown('1'))
		glEnable(GL_CULL_FACE);
	if(KeyboardController::GetInstance()->IsKeyDown('2'))
		glDisable(GL_CULL_FACE);
	if(KeyboardController::GetInstance()->IsKeyDown('3'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	if(KeyboardController::GetInstance()->IsKeyDown('4'))
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	
	/*if(KeyboardController::GetInstance()->IsKeyDown('5'))
	{
		lights[0]->type = Light::LIGHT_POINT;
	}
	else if(KeyboardController::GetInstance()->IsKeyDown('6'))
	{
		lights[0]->type = Light::LIGHT_DIRECTIONAL;
	}
	else if(KeyboardController::GetInstance()->IsKeyDown('7'))
	{
		lights[0]->type = Light::LIGHT_SPOT;
	}

	if(KeyboardController::GetInstance()->IsKeyDown('I'))
		lights[0]->position.z -= (float)(10.f * dt);
	if(KeyboardController::GetInstance()->IsKeyDown('K'))
		lights[0]->position.z += (float)(10.f * dt);
	if(KeyboardController::GetInstance()->IsKeyDown('J'))
		lights[0]->position.x -= (float)(10.f * dt);
	if(KeyboardController::GetInstance()->IsKeyDown('L'))
		lights[0]->position.x += (float)(10.f * dt);
	if(KeyboardController::GetInstance()->IsKeyDown('O'))
		lights[0]->position.y -= (float)(10.f * dt);
	if(KeyboardController::GetInstance()->IsKeyDown('P'))
		lights[0]->position.y += (float)(10.f * dt);*/

	// if the left mouse button was released
	if (MouseController::GetInstance()->IsButtonReleased(MouseController::LMB))
	{
	//	cout << "Left Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->IsButtonReleased(MouseController::RMB))
	{
	//	cout << "Right Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->IsButtonReleased(MouseController::MMB))
	{
	//	cout << "Middle Mouse Button was released!" << endl;
	}
	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_XOFFSET) != 0.0)
	{
	//	cout << "Mouse Wheel has offset in X-axis of " << MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_XOFFSET) << endl;
	}
	if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) != 0.0)
	{
	//	cout << "Mouse Wheel has offset in Y-axis of " << MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) << endl;
	}
	// <THERE>
	//camera.Update(dt); // Can put the camera into an entity rather than here (Then we don't have to write this)


	// Update NPC
	//enemyInfo->Update(dt);

	GraphicsManager::GetInstance()->UpdateLights(dt);

	if (weatherManager[0]->GetEffectTimer() >= 50 && weatherManager[0]->GetEffectTimer() <= 65)
	{
		Rain = true;
	}

	else if(weatherManager[0]->GetEffectTimer() > 65)
	{
		Rain = false;
		weatherManager[0]->SetEffectTimer(0);
	}


	if (Rain)
	{
		weatherManager[0]->SetEffectActive(true);
		weatherManager[0]->SetPos(Vector3(Math::RandFloatMinMax(playerInfo->GetPos().x - 200, playerInfo->GetPos().x + 200), 400.f, Math::RandFloatMinMax(playerInfo->GetPos().z - 200, playerInfo->GetPos().z + 200)));
		playerInfo->SetWeatherSpeedModifier(0.8);
	}

	else
	{
		weatherManager[1]->SetEffectActive(true);
		weatherManager[0]->SetEffectActive(false);
		playerInfo->SetWeatherSpeedModifier(1);
	}

	weatherManager[1]->SetEffectTimer(weatherManager[1]->GetEffectTimer() + (float)(1 * dt));

	if (weatherManager[1]->GetEffectTimer() <= 30)
	{
		weatherManager[1]->SetPos(Vector3(280, 0, -150));
		weatherManager[1]->SetEffectActive(true);
	}

	else if (weatherManager[1]->GetEffectTimer() > 30 && weatherManager[1]->GetEffectTimer() <= 60)
	{
		weatherManager[1]->SetPos(Vector3(-152, 0, 330));
		weatherManager[1]->SetEffectActive(true);
	}

	else if (weatherManager[1]->GetEffectTimer() > 60 && weatherManager[1]->GetEffectTimer() <= 90)
	{
		weatherManager[1]->SetPos(Vector3(-64, 0, -88));
		weatherManager[1]->SetEffectActive(true);
	}

	else if (weatherManager[1]->GetEffectTimer() > 90 && weatherManager[1]->GetEffectTimer() <= 110)
	{
		weatherManager[1]->SetEffectActive(false);
	}

	else if (weatherManager[1]->GetEffectTimer() > 110)
	{
		weatherManager[1]->SetEffectTimer(0);
	}

	weatherManager[2]->SetEffectTimer(weatherManager[2]->GetEffectTimer() + (float)(1 * dt));

	if (weatherManager[2]->GetEffectTimer() > 30 && weatherManager[2]->GetEffectTimer() < 60)
	{
		weatherManager[2]->SetEffectActive(true);
		weatherManager[2]->SetPos(weatherManager[2]->GetPos() + Vector3(15, 0, 0) * dt);

	}

	if (weatherManager[2]->GetEffectTimer() > 60)
	{
		weatherManager[2]->SetEffectActive(false);
		weatherManager[2]->SetEffectTimer(0);
		weatherManager[2]->SetPos(Vector3(0, 5, Math::RandFloatMinMax(-300, 300)));
	}

	UpdateParticles(dt);
	SpriteAnimation *sa = dynamic_cast<SpriteAnimation*>(MeshBuilder::GetInstance()->GetMesh("Explosion"));
	if (sa)
	{
		sa->Update(dt);
		sa->m_anim->animActive = true;
	}

	// Update the 2 text object values. NOTE: Can do this in their own class but i'm lazy to do it now :P
	// Eg. FPSRenderEntity or inside RenderUI for LightEntity


	if (menu)
	{
		HealthBar[0]->SetActive(false);
		HealthBar[1]->SetActive(false);
		HealthBar[2]->SetActive(false);

		StaminaBar[0]->SetActive(false);
		StaminaBar[1]->SetActive(false);
		StaminaBar[2]->SetActive(false);

		Stamina[0]->SetActive(false);
		Stamina[1]->SetActive(false);
		Stamina[2]->SetActive(false);


		Health->SetActive(false);
		AmmoCount->SetActive(false);
		Grenades->SetActive(false);
		MedKits->SetActive(false);

		if (!ClassSelect)
		{
			std::ostringstream ss9;
			ss9 << "Start";
			textObj[26]->SetPosition(Vector3(-67, 100, 0.0f));
			textObj[26]->SetText(ss9.str());

			std::ostringstream ss10;
			ss10 << "Instructions";
			textObj[27]->SetPosition(Vector3(-152, 75, 0.0f));
			textObj[27]->SetText(ss10.str());

			std::ostringstream ss11;
			ss11 << "High Score";
			textObj[28]->SetPosition(Vector3(-130, 50, 0.0f));
			textObj[28]->SetText(ss11.str());

			std::ostringstream ss12;
			ss12 << "Exit";
			textObj[29]->SetPosition(Vector3(-55, 25, 0.0f));
			textObj[29]->SetText(ss12.str());
		}

	}

	if (!(EnterShop || menu))
	{

		std::ostringstream ss;

		//ui
		std::ostringstream ss1;
		ss1 << "  x:" << playerInfo->GetCurrentWeaponAmmo() << "|" << playerInfo->GetCurrentWeaponMaxAmmo();
		textObj[2]->SetText(ss1.str());

		std::ostringstream ss2;
		ss2 << "Clips:" << playerInfo->GetCurrentWeaponClips();
		textObj[3]->SetText(ss2.str());

		if (playerInfo->reload)
		{
			std::ostringstream ss3;
			ss3 << "Reloading";
			textObj[4]->SetColor(Color(1,0,0));
			textObj[4]->SetText(ss3.str());
		}

		else if (playerInfo->GetUsingBandages())
		{
			std::ostringstream ss3;
			ss3 << "Patching Up";
			textObj[4]->SetColor(Color(1, 0, 0));
			textObj[4]->SetText(ss3.str());
		}

		else if (playerInfo->GetUsingGrenades())
		{
			std::ostringstream ss3;
			ss3 << "Using Grenade";
			textObj[4]->SetColor(Color(1, 0, 0));
			textObj[4]->SetText(ss3.str());
		}

		else
		{
			std::ostringstream ss3;
			ss3 << "";
			textObj[4]->SetText(ss3.str());
		}

		//Grenade count
		std::ostringstream ss4;
		ss4 << "  x" << playerInfo->GetNumOfGrenades();
		textObj[5]->SetText(ss4.str());

		//bandage count
		std::ostringstream ss5;
		ss5 << "  x" << playerInfo->GetNumOfBandages();
		textObj[31]->SetText(ss5.str());

		std::ostringstream ss8;
		ss8 << "Score:" << playerInfo->score;
		textObj[25]->SetText(ss8.str());

		if (preparetimer > 0)
		{
			std::ostringstream ss6;
			ss6.precision(2);
			ss6 << "Next wave in:" << preparetimer;
			textObj[46]->SetText(ss6.str());

			textObj[47]->SetText("");
		}
		else if(preparetimer <=0 && !Area4Unlocked)
		{
			std::ostringstream ss6;
			ss.precision(2);
			ss6 << "Wave:" << waves;
			textObj[46]->SetText(ss6.str());


			std::ostringstream ss8;
			ss8.precision(1);
			ss8 << "Enemies:" << Enemycount;
			textObj[47]->SetText(ss8.str());
		}

		else if (Area4Unlocked && Area4Surprise && preparetimer <= 0)
		{
			std::ostringstream ss6;
			ss6.precision(3);
			ss6 << "Survive for:" << countdownTimer << "s";
			textObj[46]->SetText(ss6.str());

			textObj[47]->SetText("");
		}


		if (playerInfo->Health <= 0 || WinGame)
		{
			std::ostringstream ss7;
			ss7 << "Press R to play again";
			textObj[7]->SetText(ss7.str());
		}
		else
		{
			std::ostringstream ss7;
			ss7 << "";
			textObj[7]->SetText(ss7.str());
		}

	}

	if (playerInfo->Health <= 0)
	{
		playerInfo->Health = 0;
	}

	// Update camera effects
	theCameraEffects->Update(dt);
}

void SceneText::RenderCurrentWeapon(void)
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	if (playerInfo->scopedin == false)
	{
		if (playerInfo->GetWeaponName() == "M9")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.2, -0.6, -1.5);
			modelStack.Scale(0.02, 0.02, 0.02);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M9"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "M1911")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.2, -0.7, -1.5);
			modelStack.Scale(0.14, 0.14, 0.14);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M1911"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "MPRex")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.2, -1, -1.5);
			modelStack.Scale(0.2, 0.2, 0.2);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("MPRex"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "M4A1")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.2, -0.5, -0.4);
			modelStack.Scale(0.15, 0.15, 0.15);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M4A1"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "Scar-H")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.2, -0.4, -1);
			modelStack.Scale(0.1, 0.1, 0.1);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Scar-H"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "UMP")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.4, -0.7, -1);
			modelStack.Scale(0.12, 0.12, 0.12);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("UMP"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "Pump Shotgun")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.4, -0.9, -2);
			modelStack.Scale(0.3, 0.3, 0.3);
			modelStack.Rotate(-90, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("PumpShotgun"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "AWP")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.16, -0.28, -0.4);
			modelStack.Scale(0.1, 0.1, 0.1);
			modelStack.Rotate(-180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("AWP"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "Railgun")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.1, -0.17, -0.5);
			modelStack.Scale(0.1, 0.1, 0.1);
			modelStack.Rotate(90, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Railgun"));
			modelStack.PopMatrix();
		}

	}
	else
	{
		if (playerInfo->GetWeaponName() == "M9")
		{
			modelStack.PushMatrix();
			modelStack.Translate(-0.026, -0.35, -2);
			modelStack.Scale(0.02, 0.02, 0.02);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M9"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "M1911")
		{
			modelStack.PushMatrix();
			modelStack.Translate(-0.145, -0.5, -2);
			modelStack.Scale(0.14, 0.14, 0.14);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M1911"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "MPRex")
		{
			modelStack.PushMatrix();
			modelStack.Translate(-0.09, -0.75, -1);
			modelStack.Scale(0.2, 0.2, 0.2);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("MPRex"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "M4A1")
		{
			modelStack.PushMatrix();
			modelStack.Translate(-0.5, -0.27, -1);
			modelStack.Scale(0.15, 0.15, 0.15);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M4A1"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "Scar-H")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, -0.33, -1);
			modelStack.Scale(0.1, 0.1, 0.1);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Scar-H"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "UMP")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, -0.55, -0.7);
			modelStack.Scale(0.12, 0.12, 0.12);
			modelStack.Rotate(180, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("UMP"));
			modelStack.PopMatrix();
		}
		else if (playerInfo->GetWeaponName() == "Pump Shotgun")
		{
			modelStack.PushMatrix();
			modelStack.Translate(0, -0.6, -1.5);
			modelStack.Scale(0.3, 0.3, 0.3);
			modelStack.Rotate(-90, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("PumpShotgun"));
			modelStack.PopMatrix();
		}
		else
		{

		}
	}
}

void SceneText::Render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);



	GraphicsManager::GetInstance()->UpdateLightUniforms();

	// Setup 3D pipeline then render 3D
	if (playerInfo->scopedin)
	{
		GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f - playerInfo->getWeaponScope(), 4.0f / 3.0f, 0.1f, 10000.0f);
	}

	else
	{
		GraphicsManager::GetInstance()->SetPerspectiveProjection(45.0f, 4.0f / 3.0f, 0.1f, 10000.0f);
	}

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();


	GraphicsManager::GetInstance()->AttachCamera(&camera);
	EntityManager::GetInstance()->Render();



	// Enable blend mode
	glEnable(GL_BLEND);

	if (menu)
	{
		Death->SetActive(false);
		Win->SetActive(false);

		textObj[61]->SetText("");

		if (!(ClassSelect || EnterControl || EnterHighScore))
		{
			ScoutClass->SetActive(false);
			TankClass->SetActive(false);
			ScavengerClass->SetActive(false);
			Controls->SetActive(false);
			Title->SetActive(true);

			modelStack.PushMatrix();
			modelStack.Translate(0, 0, -9);
			modelStack.Scale(40, 40, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"));
			modelStack.PopMatrix();

			modelStack.PushMatrix();
			switch (MenuIndex)
			{
			case 1:
				modelStack.Translate(0, 0.81, -6);
				modelStack.Scale(1.0, 0.2, 0);
				break;
			case 2:
				modelStack.Translate(0, 0.61, -6);
				modelStack.Scale(2.5, 0.2, 0);
				break;
			case 3:
				modelStack.Translate(0, 0.41, -6);
				modelStack.Scale(2.1, 0.2, 0);
				break;
			case 4:
				modelStack.Translate(0, 0.21, -6);
				modelStack.Scale(0.8, 0.2, 0);
				break;
			default:
				break;
			}
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("MenuQuad"), 0.5);
			modelStack.PopMatrix();
		}

		else if (ClassSelect)
		{
			Title->SetActive(false);
			RenderClassSelection();
		}

		else if (EnterControl)
		{
			for (int i = 25; i < 30; ++i)
			{
				textObj[i]->SetText("");
			}
			Title->SetActive(false);
			Controls->SetActive(true);
		}

		else if (EnterHighScore)
		{
			for (int i = 25; i < 30; ++i)
			{
				textObj[i]->SetText("");
			}

			std::ostringstream ss;
			ss << "Your High Score:" << highscore->highScore;
			textObj[61]->SetText(ss.str());

			modelStack.PushMatrix();
			modelStack.Translate(0, 0, -9);
			modelStack.Scale(40, 40, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"));
			modelStack.PopMatrix();

			Title->SetActive(false);
			
		}

	}

	else if (!menu)
	{
		Money->SetActive(true);
		std::ostringstream ss;
		ss << ":" << playerInfo->GetMoney();
		textObj[52]->SetText(ss.str());

		ScoutClass->SetActive(false);
		TankClass->SetActive(false);

		ScavengerClass->SetActive(false);
		Controls->SetActive(false);


		if (playerInfo->Health <= 0 && !WinGame)
		{
			textObj[2]->SetText("");
			textObj[4]->SetText("");
			textObj[6]->SetText("");
			textObj[3]->SetText("");
			textObj[5]->SetText("");
			textObj[25]->SetText("");
			textObj[31]->SetText("");
			textObj[46]->SetText("");
			textObj[47]->SetText("");
			textObj[52]->SetText("");
			textObj[60]->SetText("");

			Death->SetActive(true);
			Health->SetActive(false);
			AmmoCount->SetActive(false);
			Grenades->SetActive(false);
			MedKits->SetActive(false);
			Money->SetActive(false);
			CrossHairNormal->SetActive(false);
			CrossHairShotgun->SetActive(false);

			HealthBar[0]->SetActive(false);
			HealthBar[1]->SetActive(false);
			HealthBar[2]->SetActive(false);

			StaminaBar[0]->SetActive(false);
			StaminaBar[1]->SetActive(false);
			StaminaBar[2]->SetActive(false);

			Stamina[0]->SetActive(false);
			Stamina[1]->SetActive(false);
			Stamina[2]->SetActive(false);

			for (int i = 0; i < 2; ++i)
			{
				ScoutAblity[i]->SetActive(false);
				TankAblity[i]->SetActive(false);
				ScavengerAblity[i]->SetActive(false);
			}
		}

		else if(playerInfo->Health > 0)
		{
			Death->SetActive(false);
		}

		if (WinGame)
		{
			textObj[2]->SetText("");
			textObj[4]->SetText("");
			textObj[6]->SetText("");
			textObj[3]->SetText("");
			textObj[5]->SetText("");
			textObj[25]->SetText("");
			textObj[31]->SetText("");
			textObj[46]->SetText("");
			textObj[47]->SetText("");
			textObj[52]->SetText("");
			textObj[60]->SetText("");

			Win->SetActive(true);

			Health->SetActive(false);
			AmmoCount->SetActive(false);
			Grenades->SetActive(false);
			MedKits->SetActive(false);
			Money->SetActive(false);
			CrossHairNormal->SetActive(false);
			CrossHairShotgun->SetActive(false);

			HealthBar[0]->SetActive(false);
			HealthBar[1]->SetActive(false);
			HealthBar[2]->SetActive(false);

			StaminaBar[0]->SetActive(false);
			StaminaBar[1]->SetActive(false);
			StaminaBar[2]->SetActive(false);

			Stamina[0]->SetActive(false);
			Stamina[1]->SetActive(false);
			Stamina[2]->SetActive(false);

			for (int i = 0; i < 2; ++i)
			{
				ScoutAblity[i]->SetActive(false);
				TankAblity[i]->SetActive(false);
				ScavengerAblity[i]->SetActive(false);
			}
		}

	}

	if (EnterShop)
	{
		Shop();
	}

	else
	{
		for (int i = 8; i < 25; ++i)
		{
			textObj[i]->SetText("");
		}

		for (int i = 34; i < 44; ++i)
		{
			textObj[i]->SetText("");
		}

		textObj[6]->SetText("");
		textObj[32]->SetText("");
		textObj[33]->SetText("");

		textObj[44]->SetText("");
		textObj[45]->SetText("");
		textObj[53]->SetText("");
		textObj[55]->SetText("");
		textObj[56]->SetText("");
		textObj[57]->SetText("");
		textObj[58]->SetText("");
		textObj[59]->SetText("");


		if (!menu && playerInfo->Health > 0 && !WinGame)
		{
			Health->SetActive(true);
			AmmoCount->SetActive(true);
			Grenades->SetActive(true);
			MedKits->SetActive(true);
			Money->SetActive(true);

			if (playerInfo->scopedin == true)
			{
				CrossHairNormal->SetActive(false);
				CrossHairShotgun->SetActive(false);
			}
			else if (playerInfo->GetWeaponName() == "Pump Shotgun")
			{
				CrossHairNormal->SetActive(false);
				CrossHairShotgun->SetActive(true);
			}

			else if (playerInfo->GetWeaponName() == "AWP" || playerInfo->GetWeaponName() == "Railgun")
			{
				CrossHairNormal->SetActive(false);
				CrossHairShotgun->SetActive(false);
			}
			else
			{
				CrossHairNormal->SetActive(true);
				CrossHairShotgun->SetActive(false);
			}
			

			if (playerInfo->Health > (playerInfo->maxHealth * 0.75))
			{
				HealthBar[0]->SetActive(true);
				HealthBar[1]->SetActive(false);
				HealthBar[2]->SetActive(false);
			}

			else if (playerInfo->Health <= (playerInfo->maxHealth * 0.75) && playerInfo->Health > (playerInfo->maxHealth * 0.35))
			{
				HealthBar[0]->SetActive(false);
				HealthBar[1]->SetActive(true);
				HealthBar[2]->SetActive(false);
			}

			else if (playerInfo->Health <= (playerInfo->maxHealth * 0.35))
			{
				HealthBar[0]->SetActive(false);
				HealthBar[1]->SetActive(false);
				HealthBar[2]->SetActive(true);
			}

			if (playerInfo->Stamina > 75)
			{
				Stamina[0]->SetActive(true);
				Stamina[1]->SetActive(false);
				Stamina[2]->SetActive(false);
				StaminaBar[0]->SetActive(true);
				StaminaBar[1]->SetActive(false);
				StaminaBar[2]->SetActive(false);
			}

			else if (playerInfo->Stamina <= 75 && playerInfo->Stamina > 35)
			{
				Stamina[0]->SetActive(false);
				Stamina[1]->SetActive(true);
				Stamina[2]->SetActive(false);
				StaminaBar[0]->SetActive(false);
				StaminaBar[1]->SetActive(true);
				StaminaBar[2]->SetActive(false);
			}

			else if (playerInfo->Stamina <= 35)
			{
				Stamina[0]->SetActive(false);
				Stamina[1]->SetActive(false);
				Stamina[2]->SetActive(true);
				StaminaBar[0]->SetActive(false);
				StaminaBar[1]->SetActive(false);
				StaminaBar[2]->SetActive(true);
			}


			float difference = 0;
			float initial = 100 * 2.5;
			float difference2 = 0;
			float initial2 = 100 * 2.5;

			if (playerInfo->Health > 0)
			{
				difference = initial - playerInfo->Health * 2.5;
				difference = difference * 0.5;
			}

			if (playerInfo->Stamina > 0)
			{
				difference2 = initial2 - playerInfo->Stamina * 2.5;
				difference2 = difference2 * 0.5;
			}

			for (int i = 0; i < 3; ++i)
			{
				HealthBar[i]->SetPosition(Vector3(-230 - difference, -265, 0));
				HealthBar[i]->SetScale(Vector3(playerInfo->Health * 2.5, 24.0f, 0));
			}

			for (int i = 0; i < 3; ++i)
			{
				StaminaBar[i]->SetPosition(Vector3(230 + difference2, -265, 0));
				StaminaBar[i]->SetScale(Vector3(playerInfo->Stamina * 2.5, 24.0f, 0));
			}

			for (int i = 0; i < 10; ++i)
			{
				GunDamage[i]->SetActive(false);
				GunFirerate[i]->SetActive(false);
				GunRecoil[i]->SetActive(false);
				GunScope[i]->SetActive(false);
				GunWeight[i]->SetActive(false);
				GunMag[i]->SetActive(false);
			}

			if (!playerInfo->scopedin)
			{
				RenderInventory();
			}

			if (playerInfo->characterSkillLevel == 3)
			{
				RenderClassAbility();
			}
		}
	}

	if (ReplaceNeeded)
	{
		ReplaceGun();
	}

	if (!(menu || EnterShop))
	{
		RenderCurrentWeapon();
	}

	for (int i = 0; i < 3; i++)
	{
		for (std::vector<ParticleObject*>::iterator it = weatherManager[i]->m_poList.begin(); it != weatherManager[i]->m_poList.end(); ++it)
		{
			ParticleObject *particle = (ParticleObject*)*it;
			if (particle->active)
			{
				RenderParticles(particle);
			}
		}
	}

	// Setup 2D pipeline then render 2D
	int halfWindowWidth = Application::GetInstance().GetWindowWidth() / 2;
	int halfWindowHeight = Application::GetInstance().GetWindowHeight() / 2;
	GraphicsManager::GetInstance()->SetOrthographicProjection(-halfWindowWidth, halfWindowWidth, -halfWindowHeight, halfWindowHeight, -10, 10);
	GraphicsManager::GetInstance()->DetachCamera();
	EntityManager::GetInstance()->RenderUI();


		if (playerInfo->scopedin && playerInfo->GetWeaponName() == "Railgun")
		{
			theCameraEffects->SetStatus_RailgunScope(true);
			theCameraEffects->SetScale(Vector3(Application::GetInstance().GetWindowWidth(), Application::GetInstance().GetWindowHeight(), 100.0f));
		}

		else if (!playerInfo->scopedin || playerInfo->GetWeaponName() != "Railgun")
		{
			theCameraEffects->SetStatus_RailgunScope(false);
		}

		if (playerInfo->scopedin && playerInfo->GetWeaponName() == "AWP")
		{
			theCameraEffects->SetStatus_Scope(true);
		}

		else if (!playerInfo->scopedin || playerInfo->GetWeaponName() != "AWP")
		{
			theCameraEffects->SetStatus_Scope(false);
		}

		// Render Camera Effects
		theCameraEffects->RenderUI();

	// Disable blend mode
	glDisable(GL_BLEND);
	
}

void SceneText::Shop(void)
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	modelStack.PushMatrix();
	modelStack.Translate(0, 0, -6.8);
	modelStack.Scale(40, 40, 0);
	RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"), 0.6);
	modelStack.PopMatrix();

	for (int i = 0; i < 7; ++i)
	{
		if (i != 5)
		{
			textObj[i]->SetText("");
		}
	}

	textObj[46]->SetText("");

	if (ShopTab != 2)
	{
		textObj[5]->SetText("");
		textObj[31]->SetText("");
	}

	textObj[47]->SetText("");

	textObj[54]->SetText("");

	textObj[60]->SetText("");

	std::ostringstream ss;

	Health->SetActive(false);
	AmmoCount->SetActive(false);
	CrossHairNormal->SetActive(false);
	CrossHairShotgun->SetActive(false);
	
	for (int i = 0; i < 2; ++i)
	{
		ScoutAblity[i]->SetActive(false);
		TankAblity[i]->SetActive(false);
		ScavengerAblity[i]->SetActive(false);
	}



	if (ShopTab != 2)
	{
		Grenades->SetActive(false);
		MedKits->SetActive(false);
	}

	for (int i = 0; i < 3; ++i)
	{
		HealthBar[i]->SetActive(false);
		Stamina[i]->SetActive(false);
		StaminaBar[i]->SetActive(false);
	}


	//gun ui stats
	switch (GunIndex)
	{
	case 1: //Handcannon
	{
		if (damage > 2)
			damage--;
		else if (damage < 2)
			damage++;
		if (fireRate > 1)
			fireRate--;
		else if (fireRate < 1)
			fireRate++;
		if (recoil > 3)
			recoil--;
		else if (recoil < 3)
			recoil++;
		if (scope > 1)
			scope--;
		else if (scope < 1)
			scope++;
		if (weight > 1)
			weight--;
		else if (weight < 1)
			weight++;
		if (magazine > 3)
			magazine--;
		else if (magazine < 3)
			magazine++;
		break;
	}
	case 2: //Revolver
	{
		if (damage > 3)
			damage--;
		else if (damage < 3)
			damage++;
		if (fireRate > 1)
			fireRate--;
		else if (fireRate < 1)
			fireRate++;
		if (recoil > 1)
			recoil--;
		else if (recoil < 1)
			recoil++;
		if (scope > 1)
			scope--;
		else if (scope < 1)
			scope++;
		if (weight > 1)
			weight--;
		else if (weight < 1)
			weight++;
		if (magazine > 3)
			magazine--;
		else if (magazine < 3)
			magazine++;
		break;
	}
	case 3: //Rifle
	{
		if (damage > 4)
			damage--;
		else if (damage < 4)
			damage++;
		if (fireRate > 5)
			fireRate--;
		else if (fireRate < 5)
			fireRate++;
		if (recoil > 2)
			recoil--;
		else if (recoil < 2)
			recoil++;
		if (scope > 4)
			scope--;
		else if (scope < 4)
			scope++;
		if (weight > 5)
			weight--;
		else if (weight < 5)
			weight++;
		if (magazine > 9)
			magazine--;
		else if (magazine < 9)
			magazine++;
		break;
	}
	case 4: //Shotgun
	{
		if (damage > 3)
			damage--;
		else if (damage < 3)
			damage++;
		if (fireRate > 1)
			fireRate--;
		else if (fireRate < 1)
			fireRate++;
		if (recoil > 8)
			recoil--;
		else if (recoil < 8)
			recoil++;
		if (scope > 2)
			scope--;
		else if (scope < 2)
			scope++;
		if (weight > 3)
			weight--;
		else if (weight < 3)
			weight++;
		if (magazine > 2)
			magazine--;
		else if (magazine < 2)
			magazine++;
		break;
	}

	case 5: //SMG
	{
		if (damage > 2)
			damage--;
		else if (damage < 2)
			damage++;
		if (fireRate > 10)
			fireRate--;
		else if (fireRate < 10)
			fireRate++;
		if (recoil > 2)
			recoil--;
		else if (recoil < 2)
			recoil++;
		if (scope > 3)
			scope--;
		else if (scope < 3)
			scope++;
		if (weight > 2)
			weight--;
		else if (weight < 2)
			weight++;
		if (magazine > 10)
			magazine--;
		else if (magazine < 10)
			magazine++;
		break;
	}

	case 6: //Scar
	{
		if (damage > 6)
			damage--;
		else if (damage < 6)
			damage++;
		if (fireRate > 4)
			fireRate--;
		else if (fireRate < 4)
			fireRate++;
		if (recoil > 5)
			recoil--;
		else if (recoil < 5)
			recoil++;
		if (scope > 4)
			scope--;
		else if (scope < 4)
			scope++;
		if (weight > 5)
			weight--;
		else if (weight < 5)
			weight++;
		if (magazine > 9)
			magazine--;
		else if (magazine < 9)
			magazine++;
		break;
	}

	case 7: //Sniper
	{
		if (damage > 8)
			damage--;
		else if (damage < 8)
			damage++;
		if (fireRate > 1)
			fireRate--;
		else if (fireRate < 1)
			fireRate++;
		if (recoil > 9)
			recoil--;
		else if (recoil < 9)
			recoil++;
		if (scope > 10)
			scope--;
		else if (scope < 10)
			scope++;
		if (weight > 8)
			weight--;
		else if (weight < 8)
			weight++;
		if (magazine > 1)
			magazine--;
		else if (magazine < 1)
			magazine++;
		break;
	}

	case 8: //Railgun
	{
		if (damage > 10)
			damage--;
		else if (damage < 10)
			damage++;
		if (fireRate > 1)
			fireRate--;
		else if (fireRate < 1)
			fireRate++;
		if (recoil > 10)
			recoil--;
		else if (recoil < 10)
			recoil++;
		if (scope > 6)
			scope--;
		else if (scope < 6)
			scope++;
		if (weight > 10)
			weight--;
		else if (weight < 10)
			weight++;
		if (magazine > 1)
			magazine--;
		else if (magazine < 1)
			magazine++;
		break;
	default:
		break;
	}

	}

	//shop UI

	//weapons tab ui
	switch (ShopTab)
	{
	case 1:
	{
		//Tabs
		ss.str("");
		ss << "Weapons";
		textObj[32]->SetColor(Color(0, 1, 1));
		textObj[32]->SetText(ss.str());

		ss.str("");
		ss << "Equipment";
		textObj[33]->SetColor(Color(0.6, 0.6, 0.6));
		textObj[33]->SetText(ss.str());

		ss.str("");
		ss << "Class";
		textObj[55]->SetColor(Color(0.6, 0.6, 0.6));
		textObj[55]->SetText(ss.str());

		textObj[56]->SetText("");
		textObj[57]->SetText("");
		textObj[58]->SetText("");
		textObj[59]->SetText("");

		//costs
		ss.str("");
		ss << "Cost:" << playerInfo->GetWeaponCost(GetGunindexName());
		if (playerInfo->GetMoney() < playerInfo->GetWeaponCost(GetGunindexName()))
		{
			textObj[53]->SetColor(Color(1, 0, 0));
		}
		else
		{
			textObj[53]->SetColor(Color(0, 1, 0));
		}
		textObj[53]->SetText(ss.str());

		for (int i = 34; i < 44; ++i)
		{
			textObj[i]->SetText("");
		}

		textObj[44]->SetText("");
		textObj[45]->SetText("");

		ss.str("");
		ss << "Damage: ";
		for (int i = 0; i < 10; ++i)
		{
			if (i < damage)
			{
				GunDamage[i]->SetPosition(Vector3(30 + i * 25, -154, 0.0f));
				GunDamage[i]->SetActive(true);
			}
			else
			{
				GunDamage[i]->SetActive(false);
			}
		}
		textObj[17]->SetText(ss.str());

		ss.str("");
		ss << "Fire Rate:";
		for (int i = 0; i < 10; ++i)
		{
			if (i < fireRate)
			{
				GunFirerate[i]->SetPosition(Vector3(105 + i * 25, -179, 0.0f));
				GunFirerate[i]->SetActive(true);
			}
			else
			{
				GunFirerate[i]->SetActive(false);
			}

		}

		textObj[18]->SetText(ss.str());

		ss.str("");
		ss << "Recoil:";
		for (int i = 0; i < 10; ++i)
		{
			if (i < recoil)
			{
				GunRecoil[i]->SetPosition(Vector3(30 + i * 25, -204, 0.0f));
				GunRecoil[i]->SetActive(true);
			}
			else
			{
				GunRecoil[i]->SetActive(false);
			}
		}

		textObj[19]->SetText(ss.str());

		ss.str("");
		ss << "Scope:";
		for (int i = 0; i < 10; ++i)
		{
			if (i < scope)
			{
				GunScope[i]->SetPosition(Vector3(5 + i * 25, -229, 0.0f));
				GunScope[i]->SetActive(true);
			}
			else
			{
				GunScope[i]->SetActive(false);
			}
		}
		textObj[20]->SetText(ss.str());

		ss.str("");
		ss << "Weight:";
		for (int i = 0; i < 10; ++i)
		{
			if (i < weight)
			{
				GunWeight[i]->SetPosition(Vector3(30 + i * 25, -254, 0.0f));
				GunWeight[i]->SetActive(true);
			}
			else
			{
				GunWeight[i]->SetActive(false);
			}
		}
		textObj[21]->SetText(ss.str());

		ss.str("");
		ss << "Mag Rounds:";
		for (int i = 0; i < 10; ++i)
		{
			if (i < magazine)
			{
				GunMag[i]->SetPosition(Vector3(130 + i * 25, -279, 0.0f));
				GunMag[i]->SetActive(true);
			}
			else
			{
				GunMag[i]->SetActive(false);
			}
		}

		textObj[22]->SetText(ss.str());


		switch (GunIndex)
		{
		case 1:
		{

			std::ostringstream ss8;
			ss8 << "M1911";
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				textObj[8]->SetColor(Color(0, 1, 0));
			}
			else
			{
				textObj[8]->SetColor(Color(1, 0, 0));
			}
			textObj[8]->SetText(ss8.str());

			std::ostringstream ss10;
			ss10 << "MPRex";
			textObj[9]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[9]->SetText(ss10.str());

			std::ostringstream ss11;
			ss11 << "M4A1";
			textObj[10]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[10]->SetText(ss11.str());

			std::ostringstream ss12;
			ss12 << "Pump Shotgun";
			textObj[11]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[11]->SetText(ss12.str());

			std::ostringstream ss13;
			ss13 << "UMP";
			textObj[12]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[12]->SetText(ss13.str());

			std::ostringstream ss14;
			ss14 << "Scar-H";
			textObj[13]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[13]->SetText(ss14.str());

			std::ostringstream ss15;
			ss15 << "AWP";
			textObj[14]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[14]->SetText(ss15.str());

			std::ostringstream ss16;
			ss16 << "Railgun";
			textObj[15]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[15]->SetText(ss16.str());

			break;
		}

		case 2:
		{
			std::ostringstream ss8;
			ss8 << "M1911";
			textObj[8]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[8]->SetText(ss8.str());

			std::ostringstream ss10;
			ss10 << "MPRex";
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				textObj[9]->SetColor(Color(0, 1, 0));
			}
			else
			{
				textObj[9]->SetColor(Color(1, 0, 0));
			}
			textObj[9]->SetText(ss10.str());

			std::ostringstream ss11;
			ss11 << "M4A1";
			textObj[10]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[10]->SetText(ss11.str());


			std::ostringstream ss12;
			ss12 << "Pump Shotgun";
			textObj[11]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[11]->SetText(ss12.str());

			std::ostringstream ss13;
			ss13 << "UMP";
			textObj[12]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[12]->SetText(ss13.str());

			std::ostringstream ss14;
			ss14 << "Scar-H";
			textObj[13]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[13]->SetText(ss14.str());

			std::ostringstream ss15;
			ss15 << "AWP";
			textObj[14]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[14]->SetText(ss15.str());

			std::ostringstream ss16;
			ss16 << "Railgun";
			textObj[15]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[15]->SetText(ss16.str());
			break;

		}

		case 3:
		{
			std::ostringstream ss8;
			ss8 << "M1911";
			textObj[8]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[8]->SetText(ss8.str());

			std::ostringstream ss10;
			ss10 << "MPRex";
			textObj[9]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[9]->SetText(ss10.str());

			std::ostringstream ss11;
			ss11 << "M4A1";
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				textObj[10]->SetColor(Color(0, 1, 0));
			}
			else
			{
				textObj[10]->SetColor(Color(1, 0, 0));
			}
			textObj[10]->SetText(ss11.str());


			std::ostringstream ss12;
			ss12 << "Pump Shotgun";
			textObj[11]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[11]->SetText(ss12.str());

			std::ostringstream ss13;
			ss13 << "UMP";
			textObj[12]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[12]->SetText(ss13.str());

			std::ostringstream ss14;
			ss14 << "Scar-H";
			textObj[13]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[13]->SetText(ss14.str());

			std::ostringstream ss15;
			ss15 << "AWP";
			textObj[14]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[14]->SetText(ss15.str());

			std::ostringstream ss16;
			ss16 << "Railgun";
			textObj[15]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[15]->SetText(ss16.str());
			break;
		}

		case 4:
		{
			std::ostringstream ss8;
			ss8 << "M1911";
			textObj[8]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[8]->SetText(ss8.str());

			std::ostringstream ss10;
			ss10 << "MPRex";
			textObj[9]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[9]->SetText(ss10.str());

			std::ostringstream ss11;
			ss11 << "M4A1";
			textObj[10]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[10]->SetText(ss11.str());

			std::ostringstream ss12;
			ss12 << "Pump Shotgun";
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				textObj[11]->SetColor(Color(0, 1, 0));
			}
			else
			{
				textObj[11]->SetColor(Color(1, 0, 0));
			}
			textObj[11]->SetText(ss12.str());

			std::ostringstream ss13;
			ss13 << "UMP";
			textObj[12]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[12]->SetText(ss13.str());

			std::ostringstream ss14;
			ss14 << "Scar-H";
			textObj[13]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[13]->SetText(ss14.str());

			std::ostringstream ss15;
			ss15 << "AWP";
			textObj[14]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[14]->SetText(ss15.str());

			std::ostringstream ss16;
			ss16 << "Railgun";
			textObj[15]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[15]->SetText(ss16.str());
			break;
		}

		case 5:
		{
			std::ostringstream ss8;
			ss8 << "M1911";
			textObj[8]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[8]->SetText(ss8.str());

			std::ostringstream ss10;
			ss10 << "MPRex";
			textObj[9]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[9]->SetText(ss10.str());

			std::ostringstream ss11;
			ss11 << "M4A1";
			textObj[10]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[10]->SetText(ss11.str());

			std::ostringstream ss12;
			ss12 << "Pump Shotgun";
			textObj[11]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[11]->SetText(ss12.str());

			std::ostringstream ss13;
			ss13 << "UMP";
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				textObj[12]->SetColor(Color(0, 1, 0));
			}
			else
			{
				textObj[12]->SetColor(Color(1, 0, 0));
			}
			textObj[12]->SetText(ss13.str());

			std::ostringstream ss14;
			ss14 << "Scar-H";
			textObj[13]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[13]->SetText(ss14.str());

			std::ostringstream ss15;
			ss15 << "AWP";
			textObj[14]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[14]->SetText(ss15.str());

			std::ostringstream ss16;
			ss16 << "Railgun";
			textObj[15]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[15]->SetText(ss16.str());
			break;
		}

		case 6:
		{
			std::ostringstream ss8;
			ss8 << "M1911";
			textObj[8]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[8]->SetText(ss8.str());

			std::ostringstream ss10;
			ss10 << "MPRex";
			textObj[9]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[9]->SetText(ss10.str());

			std::ostringstream ss11;
			ss11 << "M4A1";
			textObj[10]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[10]->SetText(ss11.str());

			std::ostringstream ss12;
			ss12 << "Pump Shotgun";
			textObj[11]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[11]->SetText(ss12.str());

			std::ostringstream ss13;
			ss13 << "UMP";
			textObj[12]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[12]->SetText(ss13.str());

			std::ostringstream ss14;
			ss14 << "Scar-H";
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				textObj[13]->SetColor(Color(0, 1, 0));
			}
			else
			{
				textObj[13]->SetColor(Color(1, 0, 0));
			}
			textObj[13]->SetText(ss14.str());

			std::ostringstream ss15;
			ss15 << "AWP";
			textObj[14]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[14]->SetText(ss15.str());

			std::ostringstream ss16;
			ss16 << "Railgun";
			textObj[15]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[15]->SetText(ss16.str());
			break;
		}

		case 7:
		{
			std::ostringstream ss8;
			ss8 << "M1911";
			textObj[8]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[8]->SetText(ss8.str());

			std::ostringstream ss10;
			ss10 << "MPRex";
			textObj[9]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[9]->SetText(ss10.str());

			std::ostringstream ss11;
			ss11 << "M4A1";
			textObj[10]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[10]->SetText(ss11.str());

			std::ostringstream ss12;
			ss12 << "Pump Shotgun";
			textObj[11]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[11]->SetText(ss12.str());

			std::ostringstream ss13;
			ss13 << "UMP";
			textObj[12]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[12]->SetText(ss13.str());

			std::ostringstream ss14;
			ss14 << "Scar-H";
			textObj[13]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[13]->SetText(ss14.str());

			std::ostringstream ss15;
			ss15 << "AWP";
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				textObj[14]->SetColor(Color(0, 1, 0));
			}
			else
			{
				textObj[14]->SetColor(Color(1, 0, 0));
			}
			textObj[14]->SetText(ss15.str());

			std::ostringstream ss16;
			ss16 << "Railgun";
			textObj[15]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[15]->SetText(ss16.str());
			break;
		}

		case 8:
		{
			std::ostringstream ss8;
			ss8 << "M1911";
			textObj[8]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[8]->SetText(ss8.str());

			std::ostringstream ss10;
			ss10 << "MPRex";
			textObj[9]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[9]->SetText(ss10.str());

			std::ostringstream ss11;
			ss11 << "M4A1";
			textObj[10]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[10]->SetText(ss11.str());


			std::ostringstream ss12;
			ss12 << "Pump Shotgun";
			textObj[11]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[11]->SetText(ss12.str());

			std::ostringstream ss13;
			ss13 << "UMP";
			textObj[12]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[12]->SetText(ss13.str());

			std::ostringstream ss14;
			ss14 << "Scar-H";
			textObj[13]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[13]->SetText(ss14.str());

			std::ostringstream ss15;
			ss15 << "AWP";
			textObj[14]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[14]->SetText(ss15.str());

			std::ostringstream ss16;
			ss16 << "Railgun";
			if (playerInfo->Weapon2Name() != GetGunindexName() && playerInfo->Weapon3Name() != GetGunindexName())
			{
				textObj[15]->SetColor(Color(0, 1, 0));
			}
			else
			{
				textObj[15]->SetColor(Color(1, 0, 0));
			}
			textObj[15]->SetText(ss16.str());
			break;
		}

		default:
			break;
		}

		switch (GunIndex)
		{
		case 1:
		{
			modelStack.PushMatrix();
			modelStack.Translate(1, -0.1, -5);
			modelStack.Scale(0.22, 0.22, 0.22);
			modelStack.Rotate(90 + rotation, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M1911"));
			modelStack.PopMatrix();

			break;
		}

		case 2:
		{
			modelStack.PushMatrix();
			modelStack.Translate(1, -0.7, -5);
			modelStack.Scale(0.35, 0.35, 0.35);
			modelStack.Rotate(90 + rotation, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("MPRex"));
			modelStack.PopMatrix();
			break;
		}

		case 3:
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.3, 0.1, -5);
			modelStack.Scale(0.07, 0.07, 0.07);
			modelStack.Rotate(90 + rotation, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M4A1"));
			modelStack.PopMatrix();
			break;
		}

		case 4:
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.5, -0.2, -5);
			modelStack.Scale(0.2, 0.2, 0.2);
			modelStack.Rotate(180 + rotation, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("PumpShotgun"));
			modelStack.PopMatrix();
			break;
		}

		case 5:
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.5, -0.1, -5);
			modelStack.Scale(0.16, 0.16, 0.16);
			modelStack.Rotate(90 + rotation, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("UMP"));
			modelStack.PopMatrix();
			break;
		}

		case 6:
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.7, -0.2, -5);
			modelStack.Scale(0.15, 0.15, 0.15);
			modelStack.Rotate(90 + rotation, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Scar-H"));
			modelStack.PopMatrix();
			break;
		}

		case 7:
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.5, -0.2, -3);
			modelStack.Scale(0.1, 0.1, 0.1);
			modelStack.Rotate(90 + rotation, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("AWP"));
			modelStack.PopMatrix();
			break;
		}

		case 8:
		{
			modelStack.PushMatrix();
			modelStack.Translate(0.7, -0.2, -5);
			modelStack.Scale(0.35, 0.35, 0.35);
			modelStack.Rotate(rotation, 0, 1, 0);
			RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Railgun"));
			modelStack.PopMatrix();
			break;
		}

		default:
			break;
		}
		break;
	}

	//equipment tabUI
	case 2:
	{
		
		//Tabs
		ss.str("");
		ss << "Weapons";
		textObj[32]->SetColor(Color(0.6, 0.6, 0.6));
		textObj[32]->SetText(ss.str());

		ss.str("");
		ss << "Equipment";
		textObj[33]->SetColor(Color(0, 1, 1));
		textObj[33]->SetText(ss.str());

		ss.str("");
		ss << "Class";
		textObj[55]->SetColor(Color(0.6, 0.6, 0.6));
		textObj[55]->SetText(ss.str());

		//costs
		ss.str("");
		ss << "Cost:" << playerInfo->GetEquipmentCost(GetEquipmentName());
		textObj[53]->SetText(ss.str());

		if (playerInfo->GetMoney() < playerInfo->GetEquipmentCost(GetEquipmentName()))
		{
			textObj[53]->SetColor(Color(1, 0, 0));
		}
		else
		{
			textObj[53]->SetColor(Color(0, 1, 0));
		}

		textObj[56]->SetText("");

		textObj[57]->SetText("");
		textObj[58]->SetText("");
		textObj[59]->SetText("");

		Grenades->SetActive(true);
		MedKits->SetActive(true);


		for (int i = 8; i < 25; ++i)
		{
			textObj[i]->SetText("");
		}

		if (playerInfo->GetNumOfBandages() == 5)
		{
			textObj[44]->SetText("Max");
		}

		if (playerInfo->GetNumOfGrenades() == 5)
		{
			textObj[45]->SetText("Max");
		}



		//Grenade count
		ss.str("");
		ss << "  x" << playerInfo->GetNumOfGrenades();
		textObj[5]->SetText(ss.str());

		//bandage count
		ss.str("");
		ss << "  x" << playerInfo->GetNumOfBandages();
		textObj[31]->SetText(ss.str());

		for (int i = 0; i < 10; ++i)
		{
			GunDamage[i]->SetActive(false);
			GunFirerate[i]->SetActive(false);
			GunRecoil[i]->SetActive(false);
			GunScope[i]->SetActive(false);
			GunWeight[i]->SetActive(false);
			GunMag[i]->SetActive(false);
		}

		switch (EquipmentIndex)
		{
				case 1:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0, 1, 0));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[43]->SetText(ss18.str());


					break;

				}

				case 2:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0, 1, 0));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[43]->SetText(ss18.str());


					break;
				}

				case 3:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0, 1, 0));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[43]->SetText(ss18.str());


					break;
				}

				case 4:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0, 1, 0));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[43]->SetText(ss18.str());

					break;
				}

				case 5:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0, 1, 0));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[43]->SetText(ss18.str());


					break;
				}

				case 6:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0, 1, 0));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[43]->SetText(ss18.str());


					break;
				}

				case 7:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0, 1, 0));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[43]->SetText(ss18.str());


					break;
				}

				case 8:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0, 1, 0));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[43]->SetText(ss18.str());

					break;
				}

				case 9:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0, 1, 0));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[43]->SetText(ss18.str());

					break;
				}

				case 10:
				{
					std::ostringstream ss8;
					ss8 << "9mm" << " x" << playerInfo->GetAmmoAmt("9mm");
					textObj[34]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[34]->SetText(ss8.str());

					std::ostringstream ss10;
					ss10 << ".357 Magnum" << " x" << playerInfo->GetAmmoAmt(".357 Magnum");
					textObj[35]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[35]->SetText(ss10.str());

					std::ostringstream ss11;
					ss11 << ".45 ACP" << " x" << playerInfo->GetAmmoAmt(".45 ACP");
					textObj[36]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[36]->SetText(ss11.str());


					std::ostringstream ss12;
					ss12 << "5.56mm" << " x" << playerInfo->GetAmmoAmt("5.56mm");
					textObj[37]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[37]->SetText(ss12.str());

					std::ostringstream ss13;
					ss13 << "7.62x51mm" << " x" << playerInfo->GetAmmoAmt("7.62x51mm");
					textObj[38]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[38]->SetText(ss13.str());

					std::ostringstream ss14;
					ss14 << "12 Gauge" << " x" << playerInfo->GetAmmoAmt("12 Gauge");
					textObj[39]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[39]->SetText(ss14.str());

					std::ostringstream ss15;
					ss15 << ".308 Lapua Magnum" << " x" << playerInfo->GetAmmoAmt(".308 Lapua Magnum");
					textObj[40]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[40]->SetText(ss15.str());

					std::ostringstream ss16;
					ss16 << "2mm EC" << " x" << playerInfo->GetAmmoAmt("2mm EC");
					textObj[41]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[41]->SetText(ss16.str());

					std::ostringstream ss17;
					ss17 << "Bandages";
					textObj[42]->SetColor(Color(0.6, 0.6, 0.6));
					textObj[42]->SetText(ss17.str());

					std::ostringstream ss18;
					ss18 << "Grenades";
					textObj[43]->SetColor(Color(0, 1, 0));
					textObj[43]->SetText(ss18.str());

					break;
				}

			break;
			}

		break;
		}

		case 3:
		{
			ss.str("");
			ss << "Weapons";
			textObj[32]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[32]->SetText(ss.str());

			ss.str("");
			ss << "Equipment";
			textObj[33]->SetColor(Color(0.6, 0.6, 0.6));
			textObj[33]->SetText(ss.str());

			ss.str("");
			ss << "Class";
			textObj[55]->SetColor(Color(0, 1, 1));
			textObj[55]->SetText(ss.str());

			ss.str("");
			ss << "Cost:" << playerInfo->GetClassUpgradeCost();
			textObj[53]->SetText(ss.str());

			textObj[44]->SetText("");
			textObj[45]->SetText("");

			if (playerInfo->GetMoney() <  playerInfo->GetClassUpgradeCost())
			{
				textObj[53]->SetColor(Color(1, 0, 0));
			}
			else
			{
				textObj[53]->SetColor(Color(0, 1, 0));
			}

			textObj[5]->SetText("");
			textObj[31]->SetText("");

			for (int i = 34; i < 44; ++i)
			{
				textObj[i]->SetText("");
			}

			textObj[56]->SetText("Upgrade");

			switch (playerInfo->characterClass)
			{
			case 0:
				textObj[6]->SetPosition(Vector3(-60, 90, 0.0f));
				textObj[6]->SetText("Scout");
				textObj[57]->SetText("Tier1:+5% Movement Speed");
				textObj[58]->SetText("Tier2:-70% Stamina cost");
				textObj[59]->SetText("Tier3:25%HP Regeneration");
				break;
			case 1:
				textObj[6]->SetPosition(Vector3(-40, 90, 0.0f));
				textObj[6]->SetText("Tank");
				textObj[57]->SetText("Tier1:+20% Damage Resistance");
				textObj[58]->SetText("Tier2:+10% Damage");
				textObj[59]->SetText("Tier3:5 Seconds Invincibility");
				break;
			case 2:
				textObj[6]->SetPosition(Vector3(-90, 90, 0.0f));
				textObj[6]->SetText("Scavenger");
				textObj[57]->SetText("Tier1:Get Ammo Upon Kills");
				textObj[58]->SetText("Tier2:Lower Shop Prices");
				textObj[59]->SetText("Tier3:10 Seconds Infinite Ammo");
				break;
			default:
				break;
			}

			break;

		default:
			break;
		}
	}

	//text colors
	if (playerInfo->characterSkillLevel < 1)
	{
		textObj[57]->SetColor(Color(1, 0, 0));
	}
	else
	{
		textObj[57]->SetColor(Color(0, 1, 0));
	}

	if (playerInfo->characterSkillLevel < 2)
	{
		textObj[58]->SetColor(Color(1, 0, 0));
	}
	else
	{
		textObj[58]->SetColor(Color(0, 1, 0));
	}

	if (playerInfo->characterSkillLevel < 3)
	{
		textObj[59]->SetColor(Color(1, 0, 0));
	}
	else
	{
		textObj[59]->SetColor(Color(0, 1, 0));
	}
}

void SceneText::ReplaceGun(void)
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	modelStack.PushMatrix();
	modelStack.Translate(-0.35, -0.15, -2);
	modelStack.Scale(0.5, 0.4, 0);
	if (ReplacementIndex == 1)
	{
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("ReplaceQuad"));
	}
	else if (ReplacementIndex == 2)
	{
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"));
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0.35, -0.15, -2);
	modelStack.Scale(0.5, 0.4, 0);
	if (ReplacementIndex == 1)
	{
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"));
	}
	else if (ReplacementIndex == 2)
	{
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("ReplaceQuad"));
	}
	modelStack.PopMatrix();

	//weapon 1
	if (playerInfo->Weapon2Name() == "M9")
	{
		modelStack.PushMatrix();
		modelStack.Translate(-0.18, -0.08, -1);
		modelStack.Scale(0.003, 0.003, 0.003);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M9"));
		modelStack.PopMatrix();
	}

	if (playerInfo->Weapon2Name() == "M1911")
	{
		modelStack.PushMatrix();
		modelStack.Translate(-0.15, -0.08, -1);
		modelStack.Scale(0.02, 0.02, 0.02);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M1911"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "MPRex")
	{
		modelStack.PushMatrix();
		modelStack.Translate(-0.15, -0.15, -1);
		modelStack.Scale(0.03, 0.03, 0.03);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("MPRex"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "M4A1")
	{
		modelStack.PushMatrix();
		modelStack.Translate(-0.1, -0.05, -1);
		modelStack.Scale(0.006, 0.006, 0.006);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M4A1"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "Pump Shotgun")
	{
		modelStack.PushMatrix();
		modelStack.Translate(-0.18, -0.08, -1);
		modelStack.Scale(0.012, 0.012, 0.012);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("PumpShotgun"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "UMP")
	{
		modelStack.PushMatrix();
		modelStack.Translate(-0.15, -0.08, -1);
		modelStack.Scale(0.01, 0.01, 0.01);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("UMP"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "Scar-H")
	{
		modelStack.PushMatrix();
		modelStack.Translate(-0.17, -0.08, -1);
		modelStack.Scale(0.009, 0.009, 0.009);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Scar-H"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "AWP")
	{
		modelStack.PushMatrix();
		modelStack.Translate(-0.17, -0.08, -1);
		modelStack.Scale(0.009, 0.009, 0.009);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("AWP"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "Railgun")
	{
		modelStack.PushMatrix();
		modelStack.Translate(-0.17, -0.08, -1);
		modelStack.Scale(0.022, 0.022, 0.022);
		modelStack.Rotate(180, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Railgun"));
		modelStack.PopMatrix();
	}

	//weapon 2
	if (playerInfo->Weapon3Name() == "M1911")
	{
		modelStack.PushMatrix();
		modelStack.Translate(0.2, -0.08, -1);
		modelStack.Scale(0.02, 0.02, 0.02);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M1911"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "MPRex")
	{
		modelStack.PushMatrix();
		modelStack.Translate(0.2, -0.15, -1);
		modelStack.Scale(0.03, 0.03, 0.03);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("MPRex"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "M4A1")
	{
		modelStack.PushMatrix();
		modelStack.Translate(0.25, -0.05, -1);
		modelStack.Scale(0.006, 0.006, 0.006);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M4A1"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "Pump Shotgun")
	{
		modelStack.PushMatrix();
		modelStack.Translate(0.17, -0.08, -1);
		modelStack.Scale(0.012, 0.012, 0.012);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("PumpShotgun"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "UMP")
	{
		modelStack.PushMatrix();
		modelStack.Translate(0.2, -0.08, -1);
		modelStack.Scale(0.01, 0.01, 0.01);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("UMP"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "Scar-H")
	{
		modelStack.PushMatrix();
		modelStack.Translate(0.18, -0.08, -1);
		modelStack.Scale(0.009, 0.009, 0.009);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Scar-H"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "AWP")
	{
		modelStack.PushMatrix();
		modelStack.Translate(0.18, -0.08, -1);
		modelStack.Scale(0.009, 0.009, 0.009);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("AWP"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "Railgun")
	{
		modelStack.PushMatrix();
		modelStack.Translate(0.18, -0.08, -1);
		modelStack.Scale(0.022, 0.022, 0.022);
		modelStack.Rotate(180, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Railgun"));
		modelStack.PopMatrix();
	}
}

void SceneText::RenderInventory(void)
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	//Inventory UI
	modelStack.PushMatrix();
	modelStack.Translate(0.9, -0.2, -2);
	if (playerInfo->GetCurrentWeapon() == 0)
	{
		modelStack.Scale(0.8, 0.2, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"), 0.6);
	}
	else
	{
		modelStack.Scale(0.5, 0.2, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"), 0.3);
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0.9, 0, -2);
	if (playerInfo->GetCurrentWeapon() == 1)
	{
		modelStack.Scale(0.8, 0.2, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"), 0.6);
	}
	else
	{
		modelStack.Scale(0.5, 0.2, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"), 0.3);
	}
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0.9, 0.2, -2);
	if (playerInfo->GetCurrentWeapon() == 2)
	{
		modelStack.Scale(0.8, 0.2, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"), 0.6);
	}
	else
	{
		modelStack.Scale(0.5, 0.2, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("quad"), 0.3);
	}
	modelStack.PopMatrix();

	//slot 3
	modelStack.PushMatrix();
	if (playerInfo->GetCurrentWeapon() == 0)
	{
		modelStack.Translate(0.4, -0.1, -1);
	}
	else
	{
		modelStack.Translate(0.44, -0.1, -1);
	}
	modelStack.Scale(0.002, 0.002, 0.002);
	modelStack.Rotate(-90, 0, 1, 0);
	RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M9"));
	modelStack.PopMatrix();

	//slot 2
	if (playerInfo->Weapon2Name() == "M1911")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 1)
		{
			modelStack.Translate(0.42, 0, -1);
		}
		else
		{
			modelStack.Translate(0.46, 0, -1);
		}
		modelStack.Scale(0.012, 0.012, 0.012);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M1911"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "MPRex")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 1)
		{
			modelStack.Translate(0.42, -0.04, -1);
		}
		else
		{
			modelStack.Translate(0.46, -0.04, -1);
		}
		modelStack.Scale(0.015, 0.015, 0.015);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("MPRex"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "M4A1")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 1)
		{
			modelStack.Translate(0.48, 0.01, -1);
		}
		else
		{
			modelStack.Translate(0.52, 0.01, -1);
		}
		modelStack.Scale(0.005, 0.005, 0.005);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M4A1"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "Pump Shotgun")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 1)
		{
			modelStack.Translate(0.41, -0.01, -1);
		}
		else
		{
			modelStack.Translate(0.45, -0.01, -1);
		}
		modelStack.Scale(0.01, 0.01, 0.01);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("PumpShotgun"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "UMP")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 1)
		{
			modelStack.Translate(0.44, 0, -1);
		}
		else
		{
			modelStack.Translate(0.48, 0, -1);
		}
		modelStack.Scale(0.009, 0.009, 0.009);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("UMP"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "Scar-H")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 1)
		{
			modelStack.Translate(0.43, 0, -1);
		}
		else
		{
			modelStack.Translate(0.46, 0, -1);
		}
		modelStack.Scale(0.007, 0.007, 0.007);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Scar-H"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "AWP")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 1)
		{
			modelStack.Translate(0.43, -0.01, -1);
		}
		else
		{
			modelStack.Translate(0.45, -0.01, -1);
		}
		modelStack.Scale(0.008, 0.008, 0.008);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("AWP"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon2Name() == "Railgun")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 1)
		{
			modelStack.Translate(0.43, 0, -1);
		}
		else
		{
			modelStack.Translate(0.45, 0, -1);
		}
		modelStack.Scale(0.02, 0.02, 0.02);
		modelStack.Rotate(180, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Railgun"));
		modelStack.PopMatrix();
	}

	//slot 1
	if (playerInfo->Weapon3Name() == "M1911")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 2)
		{
			modelStack.Translate(0.42, 0.1, -1);
		}
		else
		{
			modelStack.Translate(0.46, 0.1, -1);
		}
		modelStack.Scale(0.012, 0.012, 0.012);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M1911"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "MPRex")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 2)
		{
			modelStack.Translate(0.42, 0.06, -1);
		}
		else
		{
			modelStack.Translate(0.46, 0.06, -1);
		}
		modelStack.Scale(0.015, 0.015, 0.015);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("MPRex"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "M4A1")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 2)
		{
			modelStack.Translate(0.48, 0.11, -1);
		}
		else
		{
			modelStack.Translate(0.52, 0.11, -1);
		}
		modelStack.Scale(0.005, 0.005, 0.005);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("M4A1"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "Pump Shotgun")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 2)
		{
			modelStack.Translate(0.41, 0.09, -1);
		}
		else
		{
			modelStack.Translate(0.45, 0.09, -1);
		}
		modelStack.Scale(0.01, 0.01, 0.01);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("PumpShotgun"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "UMP")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 2)
		{
			modelStack.Translate(0.44, 0.1, -1);
		}
		else
		{
			modelStack.Translate(0.48, 0.1, -1);
		}
		modelStack.Scale(0.009, 0.009, 0.009);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("UMP"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "Scar-H")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 2)
		{
			modelStack.Translate(0.43, 0.1, -1);
		}
		else
		{
			modelStack.Translate(0.46, 0.1, -1);
		}
		modelStack.Scale(0.007, 0.007, 0.007);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Scar-H"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "AWP")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 2)
		{
			modelStack.Translate(0.43, 0.09, -1);
		}
		else
		{
			modelStack.Translate(0.45, 0.09, -1);
		}
		modelStack.Scale(0.008, 0.008, 0.008);
		modelStack.Rotate(-90, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("AWP"));
		modelStack.PopMatrix();
	}

	else if (playerInfo->Weapon3Name() == "Railgun")
	{
		modelStack.PushMatrix();
		if (playerInfo->GetCurrentWeapon() == 2)
		{
			modelStack.Translate(0.43, 0.1, -1);
		}
		else
		{
			modelStack.Translate(0.45, 0.1, -1);
		}
		modelStack.Scale(0.02, 0.02, 0.02);
		modelStack.Rotate(180, 0, 1, 0);
		RenderHelper::RenderMeshOnScreen(MeshBuilder::GetInstance()->GetMesh("Railgun"));
		modelStack.PopMatrix();
	}

}

string SceneText::GetGunindexName(void)
{
	switch (GunIndex)
	{
	case 1:
	{
		return "M1911";
		break;
	}

	case 2:
	{
		return "MPRex";
		break;
	}

	case 3:
	{
		return "M4A1";
		break;
	}

	case 4:
	{
		return "Pump Shotgun";
		break;
	}

	case 5:
	{
		return "UMP";
		break;
	}

	case 6:
	{
		return "Scar-H";
		break;
	}

	case 7:
	{
		return "AWP";
		break;
	}

	case 8:
	{
		return "Railgun";
		break;
	}
	default:
		return "Fail";
		break;
	}
}

string SceneText::GetEquipmentName(void)
{
	switch (EquipmentIndex)
	{
	case 1:
	{
		return "9mm";
		break;
	}

	case 2:
	{
		return ".357 Magnum";
		break;
	}

	case 3:
	{
		return ".45 ACP";
		break;
	}

	case 4:
	{
		return "5.56mm";
		break;
	}

	case 5:
	{
		return "7.62x51mm";
		break;
	}

	case 6:
	{
		return "12 Gauge";
		break;
	}

	case 7:
	{
		return ".308 Lapua Magnum";
		break;
	}

	case 8:
	{
		return "2mm EC";
		break;
	}

	case 9:
	{
		return "MedKit";
		break;
	}

	case 10:
	{
		return "Grenade";
		break;
	}
	default:
		return "Fail";
		break;
	}
}

void SceneText::RenderClassSelection(void)
{
	for (int i = 25; i < 30; ++i)
	{
		textObj[i]->SetText("");
	}

	switch (ClassIndex)
	{
	case 1:
		ScoutClass->SetActive(true);
		TankClass->SetActive(false);
		ScavengerClass->SetActive(false);
		break;
	case 2:
		ScoutClass->SetActive(false);
		TankClass->SetActive(true);
		ScavengerClass->SetActive(false);
		break;
	case 3:
		ScoutClass->SetActive(false);
		TankClass->SetActive(false);
		ScavengerClass->SetActive(true);
		break;
	default:
		break;
	}
}

void SceneText::RenderClassAbility(void)
{
	switch (playerInfo->characterClass)
	{
	case 0:
		if (!playerInfo->GetCooldown())
		{
			if (!playerInfo->GetCharacterSkillActive())
			{
				textObj[60]->SetText("");
				ScoutAblity[0]->SetActive(true);
				ScoutAblity[0]->transparency = 1.0f;
				ScoutAblity[1]->SetActive(false);
			}

			else if (playerInfo->GetCharacterSkillActive())
			{
				textObj[60]->SetText("");
				ScoutAblity[0]->SetActive(false);
				ScoutAblity[1]->SetActive(true);
			}
		}

		else
		{
			std::ostringstream ss;
			ss.precision(2);
			ss << playerInfo->CooldownTimer;
			textObj[60]->SetText(ss.str());

			ScoutAblity[0]->SetActive(true);
			ScoutAblity[0]->transparency = 0.4f;
			ScoutAblity[1]->SetActive(false);
		}
		
		break;
	case 1:
		if (!playerInfo->GetCooldown())
		{
			if (!playerInfo->GetCharacterSkillActive())
			{
				textObj[60]->SetText("");
				TankAblity[0]->SetActive(true);
				TankAblity[0]->transparency = 1.0f;
				TankAblity[1]->SetActive(false);
			}
			else if (playerInfo->GetCharacterSkillActive())
			{
				textObj[60]->SetText("");
				TankAblity[0]->SetActive(false);
				TankAblity[1]->SetActive(true);
			}
		}
		else
		{
			std::ostringstream ss;
			ss.precision(2);
			ss << playerInfo->CooldownTimer;
			textObj[60]->SetText(ss.str());
			TankAblity[0]->SetActive(true);
			TankAblity[0]->transparency = 0.4f;
			TankAblity[1]->SetActive(false);
		}

		break;
	case 2:
		if (!playerInfo->GetCooldown())
		{
			if (!playerInfo->GetCharacterSkillActive())
			{
				textObj[60]->SetText("");
				ScavengerAblity[0]->SetActive(true);
				ScavengerAblity[0]->transparency = 1.0f;

				ScavengerAblity[1]->SetActive(false);
			}
			else if (playerInfo->GetCharacterSkillActive())
			{
				textObj[60]->SetText("");
				ScavengerAblity[0]->SetActive(false);
				ScavengerAblity[1]->SetActive(true);
			}
		}
		else
		{
			std::ostringstream ss;
			ss.precision(2);
			ss << playerInfo->GetCooldown();
			textObj[60]->SetText(ss.str());
			ScavengerAblity[0]->SetActive(true);
			ScavengerAblity[0]->transparency = 0.4f;
			ScavengerAblity[1]->SetActive(false);
		}
		break;
	default:
		break;
	}
}

void SceneText::Exit()
{
	// Detach camera from other entities
	GraphicsManager::GetInstance()->DetachCamera();
	playerInfo->DetachCamera();

	while (particleList.size() > 0)
	{
		ParticleObject *particle = particleList.back();
		delete particle;
		particleList.pop_back();
	}

	if (EntityManager::GetInstance()->entityList.size() > 0)
	{
		for (std::list<EntityBase*>::iterator it = EntityManager::GetInstance()->entityList.begin(); it != EntityManager::GetInstance()->entityList.end(); ++it)
		{
			EntityBase* eo = (EntityBase*)*it;
			if (eo)
			{
				delete eo;
				eo = NULL;
			}
		}
		std::list<EntityBase*>().swap(EntityManager::GetInstance()->entityList);
	}
	


	if (playerInfo->DropInstance() == false)
	{
#if _DEBUGMODE==1
		cout << "Unable to drop PlayerInfo class" << endl;
#endif
	}

	// Delete the lights
	/*if (lights[0])
	{
		delete lights[0];
		lights[0] = NULL;
	}
	if (lights[1])
	{
		delete lights[1];
		lights[1] = NULL;
	}*/
}
