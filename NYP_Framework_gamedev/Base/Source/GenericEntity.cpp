#include "GenericEntity.h"
#include "MeshBuilder.h"
#include "EntityManager.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"

GenericEntity::GenericEntity(Mesh* _modelMesh)
	: modelMesh(_modelMesh)
{
}

GenericEntity::~GenericEntity()
{
}

void GenericEntity::Update(double _dt)
{
	// Does nothing here, can inherit & override or create your own version of this class :D

	if (MoveGateX)
	{
		if (translateX < 1.3)
		{
			translateX += (float)(1 * _dt);
		}
		else
		{
			translateX = 0;
			MoveGateX = false;
			CanInteract = false;
		}

		position.x += translateX;

	}

	if (MoveGateZ)
	{
		if (translateZ > -1.3)
		{
			translateZ -= (float)(1 * _dt);
		}

		else
		{
			translateZ = 0;
			MoveGateZ = false;
			CanInteract = false;
		}

		position.z += translateZ;
	}

	if (SurpriseMove)
	{
		if (translateX < 1.3)
		{
			translateX += (float)(1 * _dt);
		}
		else
		{
			translateX = 0;
			SurpriseMove = false;
			CanInteract = false;
		}

		position.x -= translateX;
	}

		Collision();

		if (CanInteract)
		{
			Interaction();
		}
}

void GenericEntity::Render()
{
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	modelStack.PushMatrix();
	modelStack.Translate(position.x, position.y, position.z);
	modelStack.Scale(scale.x, scale.y, scale.z);
	modelStack.Rotate(angle, 0, 1, 0);
	RenderHelper::RenderMesh(modelMesh);
	modelStack.PopMatrix();
}

// Set the maxAABB and minAABB
void GenericEntity::SetAABB(Vector3 maxAABB, Vector3 minAABB)
{
	this->maxAABB = maxAABB;
	this->minAABB = minAABB;
}

bool GenericEntity::CheckOverlap(Vector3 thisMinAABB, Vector3 thisMaxAABB, Vector3 playerposMin,Vector3 playerposmax)
{
	if (((playerposMin >= thisMinAABB) && (playerposMin <= thisMaxAABB))
		||
		((playerposmax >= thisMinAABB) && (playerposmax <= thisMaxAABB)))
	{
		return true;
	}

	// Check if that object is overlapping this object
	if (((thisMinAABB >= playerposMin) && (thisMinAABB <= playerposmax))
		||
		((thisMaxAABB >= playerposMin) && (thisMaxAABB <= playerposmax)))
	{
		return true;
	}

	// Check if this object is within that object
	if (((thisMinAABB >= playerposMin) && (thisMaxAABB <= playerposmax))
		&&
		((thisMaxAABB >= playerposMin) && (thisMaxAABB <= playerposmax)))
		return true;

	// Check if that object is within this object
	if (((playerposMin >= thisMinAABB) && (playerposMin <= thisMaxAABB))
		&&
		((playerposmax >= thisMinAABB) && (playerposmax <= thisMaxAABB)))
		return true;

	return false;
}



bool GenericEntity::Collision(void)
{
	Vector3 thisMinAABB = position + this->GetMinAABB();
	Vector3 thisMaxAABB = position + this->GetMaxAABB();
	Vector3 playerposmin = CPlayerInfo::GetInstance()->GetPos() + CPlayerInfo::GetInstance()->GetMinAABB();
	Vector3 playerposmax = CPlayerInfo::GetInstance()->GetPos() + CPlayerInfo::GetInstance()->GetMaxAABB();


	if (CheckOverlap(thisMinAABB, thisMaxAABB, playerposmin, playerposmax))
	{
		Vector3 dist = CPlayerInfo::GetInstance()->GetPos() - position;
		CPlayerInfo::GetInstance()->SetVel(CPlayerInfo::GetInstance()->GetVel() + dist);
		if (CPlayerInfo::GetInstance()->GetVel().Length() > 1)
		{
			CPlayerInfo::GetInstance()->SetVel(CPlayerInfo::GetInstance()->GetVel().Normalized() * 1);
		}
		CPlayerInfo::GetInstance()->SetPos(currentpos);
		CPlayerInfo::GetInstance()->SetTarget(currenttarget);
		return true;
	}
	
	Vector3 altThisMinAABB = Vector3(thisMinAABB.x, thisMinAABB.y, thisMaxAABB.z);
	Vector3 altThisMaxAABB = Vector3(thisMaxAABB.x, thisMaxAABB.y, thisMinAABB.z);

	if (CheckOverlap(altThisMinAABB, altThisMaxAABB, playerposmin, playerposmax))
	{

		Vector3 dist = CPlayerInfo::GetInstance()->GetPos() - position;
		CPlayerInfo::GetInstance()->SetVel(CPlayerInfo::GetInstance()->GetVel() + dist);
		
		if (CPlayerInfo::GetInstance()->GetVel().Length() > 1)
		{
			CPlayerInfo::GetInstance()->SetVel(CPlayerInfo::GetInstance()->GetVel().Normalized() * 1);
		}
		CPlayerInfo::GetInstance()->SetPos(currentpos);
		CPlayerInfo::GetInstance()->SetTarget(currenttarget);
		return true;
	}

	else
	{
		currentpos = CPlayerInfo::GetInstance()->GetPos();
		currenttarget = CPlayerInfo::GetInstance()->GetTarget();
	}

	return false;

}

bool GenericEntity::Interaction(void)
{
	Vector3 thisMinAABBInteract = position + Interactionmin;
	Vector3 thisMaxAABBInteract = position + Interactionmax;

	Vector3 playerposmin = CPlayerInfo::GetInstance()->GetPos() + CPlayerInfo::GetInstance()->GetMinAABB();
	Vector3 playerposmax = CPlayerInfo::GetInstance()->GetPos() + CPlayerInfo::GetInstance()->GetMaxAABB();

	if (CheckOverlap(thisMinAABBInteract, thisMaxAABBInteract, playerposmin, playerposmax))
	{
		InInteractionRadius = true;
		return true;
	}

	else
	{
		InInteractionRadius = false;
	}

	Vector3 altThisMinAABBInteract = Vector3(thisMinAABBInteract.x, thisMinAABBInteract.y, thisMaxAABBInteract.z);
	Vector3 altThisMaxAABBInteract = Vector3(thisMaxAABBInteract.x, thisMaxAABBInteract.y, thisMinAABBInteract.z);

	if (CheckOverlap(altThisMinAABBInteract, altThisMaxAABBInteract, playerposmin, playerposmax))
	{
		InInteractionRadius = true;
		return true;
	}

	else
	{
		InInteractionRadius = false;
	}

	return false;
}

void GenericEntity::SetInteractionAABB(Vector3 InteractionMax, Vector3 InteractionMin)
{
	this->Interactionmax = InteractionMax;
	this->Interactionmin = InteractionMin;
}

GenericEntity* Create::Entity(	const std::string& _meshName, 
								const Vector3& _position,
								const Vector3& _scale)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	GenericEntity* result = new GenericEntity(modelMesh);
	result->SetPosition(_position);
	result->SetScale(_scale);
	result->SetCollider(true);
	result->SetisBullet(false);
	result->SetIsEnemy(false);
	result->SetIsNade(false);
	result->angle = 0;
	result->CanInteract = false;
	result->MoveGateX = false;
	result->MoveGateZ = false;
	result->translateX = 0;
	result->translateZ = 0;
	result->InInteractionRadius = false;
	result->SurpriseMove = false;
	EntityManager::GetInstance()->AddEntity(result);
	return result;
}
