#pragma once

#include "WeaponInfo.h"

class CRailgun :
	public CWeaponInfo
{
public:
	CRailgun();
	virtual ~CRailgun();

	// Initialise this instance to default values
	void Init(void);
};
