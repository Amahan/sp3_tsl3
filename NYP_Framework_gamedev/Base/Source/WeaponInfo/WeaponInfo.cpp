#include "WeaponInfo.h"
#include "../Projectile/Projectile.h"

#include <iostream>
using namespace std;

CWeaponInfo::CWeaponInfo()
	: magRounds(1)
	, maxMagRounds(1)
	, totalRounds(8)
	, maxTotalRounds(8)
	, totalClips(8)
	, maxTotalClips(8)
	, timeBetweenShots(0.5)
	, elapsedTime(0.0)
	, bFire(true)
	, m_sWeaponName("")
	, m_sAmmoType("")
{
}


CWeaponInfo::~CWeaponInfo()
{
}

// Set the number of ammunition in the magazine for this player
void CWeaponInfo::SetMagRound(const int magRounds)
{
	this->magRounds = magRounds;
}

// Set the maximum number of ammunition in the magazine for this weapon
void CWeaponInfo::SetMaxMagRound(const int magRounds)
{
	this->magRounds = magRounds;
}

// The current total number of rounds currently carried by this player
void CWeaponInfo::SetTotalRound(const int totalRounds)
{
	this->totalRounds = totalRounds;
}

// The max total number of rounds currently carried by this player
void CWeaponInfo::SetMaxTotalRound(const int maxTotalRounds)
{
	this->maxTotalRounds = maxTotalRounds;
}

// The current total number of clips currently carried by this player
void CWeaponInfo::SetTotalClips(const int totalClips)
{
	this->totalClips = totalClips;
}
// The max total number of clips currently carried by this player
void CWeaponInfo::SetMaxTotalClips(const int maxTotalClips)
{
	this->maxTotalClips = maxTotalClips;
}

void CWeaponInfo::SetWeaponType(const std::string type)
{
	this->m_sWeaponName = type;
}

void CWeaponInfo::SetAmmoType(const std::string type)
{
	m_sAmmoType = type;
}

// Get the number of ammunition in the magazine for this player
int CWeaponInfo::GetMagRound(void) const
{
	return magRounds;
}

// Get the maximum number of ammunition in the magazine for this weapon
int CWeaponInfo::GetMaxMagRound(void) const
{
	return maxMagRounds;
}

// Get the current total number of rounds currently carried by this player
int CWeaponInfo::GetTotalRound(void) const
{
	return totalRounds;
}

// Get the max total number of rounds currently carried by this player
int CWeaponInfo::GetMaxTotalRound(void) const
{
	return maxTotalRounds;
}

// Get the current total number of clips currently carried by this player
int CWeaponInfo::GetTotalClips(void) const
{
	return totalClips;
}
// Get the max total number of clips currently carried by this player
int CWeaponInfo::GetMaxTotalClips(void) const
{
	return maxTotalClips;
}

string CWeaponInfo::GetWeaponName(void) const
{
	return m_sWeaponName;
}

std::string CWeaponInfo::GetAmmoType(void) const
{
	return m_sAmmoType;
}

// Set the time between shots
void CWeaponInfo::SetTimeBetweenShots(const double timeBetweenShots)
{
	this->timeBetweenShots = timeBetweenShots;
}

// Set the firing rate in rounds per min
void CWeaponInfo::SetFiringRate(const int firingRate)
{
	timeBetweenShots = 60.0 / (double)firingRate;	// 60 seconds divided by firing rate
}

// Set the firing flag
void CWeaponInfo::SetCanFire(const bool bFire)
{
	this->bFire = bFire;
}

// Get the time between shots
double CWeaponInfo::GetTimeBetweenShots(void) const
{
	return timeBetweenShots;
}

// Get the firing rate
int CWeaponInfo::GetFiringRate(void) const
{
	return (int)(60.0 / timeBetweenShots);	// 60 seconds divided by timeBetweenShots
}

// Get the firing flag
bool CWeaponInfo::GetCanFire(void) const
{
	return bFire;
}

float CWeaponInfo::GetRecoil(void) const
{
	return Recoil;
}

float CWeaponInfo::GetProjSpeed(void) const
{
	return ProjSpeed;
}

float CWeaponInfo::GetWeaponWeight(void) const
{
	return weight;
}

float CWeaponInfo::GetWeaponScope(void) const
{
	return Scope;
}

int CWeaponInfo::GetWeaponDamage(void) const
{
	return damage;
}

// Initialise this instance to default values
void CWeaponInfo::Init(void)
{
	// The number of ammunition in a magazine for this weapon
	magRounds = 1;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 1;
	// The current total number of rounds currently carried by this player
	totalRounds = 8;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 8;
	// The current total number of clips currently carried by this player
	totalClips = 1;
	// The max total number of clips currently carried by this player
	maxTotalClips = 1;

	//Weapon Recoil
	Recoil = 1;

	// The time between shots
	timeBetweenShots = 0.5;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;
}

// Update the elapsed time
void CWeaponInfo::Update(const double dt)
{
	elapsedTime += dt;
	if (elapsedTime > timeBetweenShots)
	{
		bFire = true;
		elapsedTime = 0.0;
	}

	if (magRounds <= 0)
	{
		bFire = false;
	}
}

// Discharge this weapon
void CWeaponInfo::Discharge(Vector3 position, Vector3 target,float ProjSpeed, bool check, CPlayerInfo* _source)
{
	if (bFire)
	{
		// If there is still ammo in the magazine, then fire
		if (magRounds > 0)
		{
			// Create a projectile with a cube mesh. Its position and direction is same as the player.
			// It will last for 3.0 seconds and travel at 500 units per second
				CProjectile* aProjectile = Create::Projectile("Bullet",

					position,
					(target - position).Normalized(),
					10.0f,
					ProjSpeed,
					_source);

				aProjectile->SetCollider(true);
				aProjectile->SetisBullet(true);
				aProjectile->SetIsEnemy(false);
				aProjectile->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
				bFire = false;
				if (check == false)
				{
					magRounds--;
				}
				elapsedTime = 0;
		}
	}
}

// Reload this weapon
void CWeaponInfo::Reload(void)
{
	// This version is for using rounds, without clips
	//if (magRounds < maxMagRounds)
	//{
	//	if (maxMagRounds - magRounds <= totalRounds)
	//	{
	//		totalRounds -= maxMagRounds - magRounds;
	//		magRounds = maxMagRounds;
	//	}
	//	else
	//	{
	//		magRounds += totalRounds;
	//		totalRounds = 0;
	//	}
	//}

	// This version is for using clips
	// if the current rounds in weapon is less than the max, then allow reload
	if (magRounds < maxMagRounds)
	{
		if (totalClips > 0)
		{
			magRounds = maxMagRounds;	// Set the current round in weapon to max
			totalClips--;
		}
	}
}

// Add rounds
void CWeaponInfo::AddRounds(const int newRounds)
{
	if (totalRounds + newRounds > maxTotalRounds)
		totalRounds = maxTotalRounds;
	else
		totalRounds += newRounds;
}

// Add clip
void CWeaponInfo::AddClip(void)
{
	// This version is for using clips
	if (totalClips < maxTotalClips)
	{
		if (magRounds != maxMagRounds)
			magRounds = maxMagRounds;
		else
			totalClips++;
	}
}

// Print Self
void CWeaponInfo::PrintSelf(void)
{
	cout << "CWeaponInfo::PrintSelf()" << endl;
	cout << "========================" << endl;
	cout << "magRounds\t\t:\t" << magRounds << endl;
	cout << "maxMagRounds\t\t:\t" << maxMagRounds << endl;
	cout << "totalRounds\t\t:\t" << totalRounds << endl;
	cout << "maxTotalRounds\t\t:\t" << maxTotalRounds << endl;
	cout << "totalClips\t\t:\t" << totalClips << endl;
	cout << "maxTotalClips\t\t:\t" << maxTotalClips << endl;
	cout << "timeBetweenShots\t:\t" << timeBetweenShots << endl;
	cout << "elapsedTime\t\t:\t" << elapsedTime << endl;
	cout << "bFire\t\t:\t" << bFire << endl;
}
