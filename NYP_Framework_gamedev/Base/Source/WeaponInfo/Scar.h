#pragma once

#include "WeaponInfo.h"

class CScar :
	public CWeaponInfo
{
public:
	CScar();
	virtual ~CScar();

	// Initialise this instance to default values
	void Init(void);
};

