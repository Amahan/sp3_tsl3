#include "Sniper.h"


CSniper::CSniper()
{
}


CSniper::~CSniper()
{
}

// Initialise this instance to default values
void CSniper::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 2;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 2;
	// The current total number of rounds currently carried by this player
	totalRounds = 40;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 40;

	//Recoil
	Recoil = 3;

	//projspeed
	ProjSpeed = 900;

	//scope
	Scope = 40;

	//weight
	weight = 2;

	//damage
	damage = 100;

	//Reload Speed 3 s

	m_sWeaponName = "AWP";
	m_sAmmoType = ".308 Lapua Magnum";

	// The time between shots
	timeBetweenShots = 3.0f;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	CWeaponInfo::SetMaxTotalClips(100);
	CWeaponInfo::SetTotalClips(20);
}
