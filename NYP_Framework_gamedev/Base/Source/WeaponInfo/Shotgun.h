#pragma once

#include "WeaponInfo.h"

class CShotgun :
	public CWeaponInfo
{
public:
	CShotgun();
	virtual ~CShotgun();

	// Initialise this instance to default values
	void Init(void);
};
