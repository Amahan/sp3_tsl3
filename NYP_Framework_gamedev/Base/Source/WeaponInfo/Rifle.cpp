#include "Rifle.h"


CRifle::CRifle()
{
}


CRifle::~CRifle()
{
}

// Initialise this instance to default values
void CRifle::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 30;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 30;
	// The current total number of rounds currently carried by this player
	totalRounds = 40;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 40;

	//Recoil
	Recoil = 1.2;

	//Proj speed
	ProjSpeed = 700;

	//weight
	weight = 1.5;

	//scope
	Scope = 20;

	//damage
	damage = 20;


	//Reload Speed 2 s

	m_sWeaponName = "M4A1";
	m_sAmmoType = "5.56mm";

	// The time between shots
	timeBetweenShots = 0.15;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	CWeaponInfo::SetMaxTotalClips(8);
	CWeaponInfo::SetTotalClips(8);
}
