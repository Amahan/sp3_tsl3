#include "Pistol.h"


CPistol::CPistol()
{
}


CPistol::~CPistol()
{
}

// Initialise this instance to default values
void CPistol::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 12;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 12;
	// The current total number of rounds currently carried by this player
	totalRounds = 40;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 40;

	//Recoil
	Recoil = 0.5;

	//Projectile Speed
	ProjSpeed = 500;

	//scope
	Scope = 5;

	//weight
	weight = 1;

	//damage
	damage = 15;


	//Reload Speed 1.5 s

	m_sWeaponName = "M9";
	m_sAmmoType = "9mm";

	// The time between shots
	timeBetweenShots = 0.4;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	CWeaponInfo::SetMaxTotalClips(100);
	CWeaponInfo::SetTotalClips(5);
}
