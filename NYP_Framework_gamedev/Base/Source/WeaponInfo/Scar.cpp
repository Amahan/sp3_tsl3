#include "Scar.h"


CScar::CScar()
{
}


CScar::~CScar()
{
}

// Initialise this instance to default values
void CScar::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 20;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 20;
	// The current total number of rounds currently carried by this player
	totalRounds = 40;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 40;

	//Recoil
	Recoil = 1.8;

	//Proj speed
	ProjSpeed = 700;

	//weight
	weight = 1.8;

	//scope
	Scope = 20;

	//damage
	damage = 75;

	//Reload Speed 2 s

	m_sWeaponName = "Scar-H";
	m_sAmmoType = "7.62x51mm";

	// The time between shots
	timeBetweenShots = 0.2;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	CWeaponInfo::SetMaxTotalClips(8);
	CWeaponInfo::SetTotalClips(8);
}
