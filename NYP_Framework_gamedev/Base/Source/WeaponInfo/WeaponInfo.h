#pragma once

#include "Vector3.h"

class CPlayerInfo;

class CWeaponInfo
{
public:
	CWeaponInfo();
	virtual ~CWeaponInfo();
protected:
	// The number of ammunition in a magazine for this weapon
	int magRounds;
	// The maximum number of ammunition for this magazine for this weapon
	int maxMagRounds;
	// The current total number of rounds currently carried by this player
	int totalRounds;
	// The max total number of rounds currently carried by this player
	int maxTotalRounds;
	// The current total number of clips currently carried by this player
	int totalClips;
	// The max total number of clips currently carried by this player
	int maxTotalClips;

	// The time between shots in milliseconds
	double timeBetweenShots;
	// The elapsed time (between shots) in milliseconds
	double elapsedTime;
	// Boolean flag to indicate if weapon can fire now
	bool bFire;
	
	//projectile speed
	float ProjSpeed;

	//weight
	float weight;

	//scope
	float Scope;

	//Recoil
	float Recoil;

	//damage
	int damage;

	//weapon type
	std::string m_sWeaponName;
	// ammo type
	std::string m_sAmmoType;

public:
	// Set the number of ammunition in the magazine for this player
	virtual void SetMagRound(const int magRounds);
	// Set the maximum number of ammunition in the magazine for this weapon
	virtual void SetMaxMagRound(const int magRounds);
	// The current total number of rounds currently carried by this player
	virtual void SetTotalRound(const int totalRounds);
	// The max total number of rounds currently carried by this player
	virtual void SetMaxTotalRound(const int maxTotalRounds);
	// The current total number of clips currently carried by this player
	virtual void SetTotalClips(const int totalClips);
	// The max total number of clips currently carried by this player
	virtual void SetMaxTotalClips(const int maxTotalClips);
	// The weapon type
	virtual void SetWeaponType(const std::string type);
	// The ammo type
	virtual void SetAmmoType(const std::string type);

	// Get the number of ammunition in the magazine for this player
	virtual int GetMagRound(void) const;
	// Get the maximum number of ammunition in the magazine for this weapon
	virtual int GetMaxMagRound(void) const;
	// Get the current total number of rounds currently carried by this player
	virtual int GetTotalRound(void) const;
	// Get the max total number of rounds currently carried by this player
	virtual int GetMaxTotalRound(void) const;
	// Get the current total number of clips currently carried by this player
	virtual int GetTotalClips(void) const;
	// Get the max total number of clips currently carried by this player
	virtual int GetMaxTotalClips(void) const;
	// Get the weapon type
	virtual std::string GetWeaponName(void) const;
	// Get the ammo type
	virtual std::string GetAmmoType(void) const;

	// Set the time between shots
	virtual void SetTimeBetweenShots(const double timeBetweenShots);
	// Set the firing rate in rounds per min
	virtual void SetFiringRate(const int firingRate);
	// Set the firing flag
	virtual void SetCanFire(const bool bFire);

	// Get the time between shots
	virtual double GetTimeBetweenShots(void) const;
	// Get the firing rate
	virtual int GetFiringRate(void) const;
	// Get the firing flag
	virtual bool GetCanFire(void) const;
	
	//Get recoil
	virtual float GetRecoil(void) const;

	//Get projectile speed
	virtual float GetProjSpeed(void) const;

	//Get weapon weight
	virtual float GetWeaponWeight(void) const;

	//Get scope
	virtual float GetWeaponScope(void) const;

	//Get damage
	virtual int GetWeaponDamage(void)const;

	// Initialise this instance to default values
	virtual void Init(void);
	// Update the elapsed time
	void Update(const double dt);
	// Discharge this weapon
	void Discharge(Vector3 position, Vector3 target,float ProjSpeed, bool check, CPlayerInfo* _source = NULL);
	// Reload this weapon
	void Reload(void);
	// Add rounds
	void AddRounds(const int newRounds);
	// Add clip
	void AddClip(void);

	// Print Self
	void PrintSelf(void);
};
