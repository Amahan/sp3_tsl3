#include "Revolver.h"


CRevolver::CRevolver()
{
}


CRevolver::~CRevolver()
{
}

// Initialise this instance to default values
void CRevolver::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 6;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 6;
	// The current total number of rounds currently carried by this player
	totalRounds = 40;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 40;

	//Recoil
	Recoil = 1;

	//Projectile Speed
	ProjSpeed = 500;

	//scope
	Scope = 5;

	//weight
	weight = 1;

	//damage
	damage = 30 ;


	//Reload Speed 2 s

	m_sWeaponName = "MPRex";
	m_sAmmoType = ".357 Magnum";

	// The time between shots
	timeBetweenShots = 1;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	CWeaponInfo::SetMaxTotalClips(100);
	CWeaponInfo::SetTotalClips(5);
}
