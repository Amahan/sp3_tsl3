#pragma once

#include "WeaponInfo.h"

class CHandCannon :
	public CWeaponInfo
{
public:
	CHandCannon();
	virtual ~CHandCannon();

	// Initialise this instance to default values
	void Init(void);
};

