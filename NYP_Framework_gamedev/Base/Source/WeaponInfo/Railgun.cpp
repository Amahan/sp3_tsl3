#include "Railgun.h"


CRailgun::CRailgun()
{
}


CRailgun::~CRailgun()
{
}

// Initialise this instance to default values
void CRailgun::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 1;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 1;
	// The current total number of rounds currently carried by this player
	totalRounds = 40;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 40;

	//Recoil
	Recoil = 5;

	//projspeed
	ProjSpeed = 900;

	//scope
	Scope = 30;

	//weight
	weight = 3;

	//damage
	damage = 300;


	//Reload Speed 3 s

	m_sWeaponName = "Railgun";
	m_sAmmoType = "2mm EC";

	// The time between shots
	timeBetweenShots = 5.0f;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	CWeaponInfo::SetMaxTotalClips(100);
	CWeaponInfo::SetTotalClips(5);
}
