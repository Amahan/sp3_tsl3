#pragma once

#include "WeaponInfo.h"

class CRevolver :
	public CWeaponInfo
{
public:
	CRevolver();
	virtual ~CRevolver();

	// Initialise this instance to default values
	void Init(void);
};

