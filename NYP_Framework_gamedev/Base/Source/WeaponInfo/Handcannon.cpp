#include "Handcannon.h"


CHandCannon::CHandCannon()
{
}


CHandCannon::~CHandCannon()
{
}

// Initialise this instance to default values
void CHandCannon::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 8;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 8;
	// The current total number of rounds currently carried by this player
	totalRounds = 40;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 40;

	//Recoil
	Recoil = 1.3;

	//Projectile Speed
	ProjSpeed = 500;

	//scope
	Scope = 5;

	//weight
	weight = 1;

	//damage
	damage = 20;

	//Reload Speed 2 s

	m_sWeaponName = "M1911";
	m_sAmmoType = ".45 ACP";

	// The time between shots
	timeBetweenShots = 0.7;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	CWeaponInfo::SetMaxTotalClips(100);
	CWeaponInfo::SetTotalClips(5);
}
