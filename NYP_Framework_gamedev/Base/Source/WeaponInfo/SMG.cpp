#include "SMG.h"


CSMG::CSMG()
{
}


CSMG::~CSMG()
{
}

// Initialise this instance to default values
void CSMG::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 25;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 25;
	// The current total number of rounds currently carried by this player
	totalRounds = 40;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 40;

	//Recoil
	Recoil = 1;

	//Proj speed
	ProjSpeed = 800;

	//weight
	weight = 1.2;

	//scope
	Scope = 10;

	//damage
	damage = 30;

	//Reload Speed 2.5 s

	m_sWeaponName = "UMP";
	m_sAmmoType = ".45 ACP";

	// The time between shots
	timeBetweenShots = 0.1;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	CWeaponInfo::SetMaxTotalClips(100);
	CWeaponInfo::SetTotalClips(8);
}
