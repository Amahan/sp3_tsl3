#include "Shotgun.h"


CShotgun::CShotgun()
{
}


CShotgun::~CShotgun()
{
}

// Initialise this instance to default values
void CShotgun::Init(void)
{
	// Call the parent's Init method
	CWeaponInfo::Init();

	// The number of ammunition in a magazine for this weapon
	magRounds = 5;
	// The maximum number of ammunition for this magazine for this weapon
	maxMagRounds = 5;
	// The current total number of rounds currently carried by this player
	totalRounds = 40;
	// The max total number of rounds currently carried by this player
	maxTotalRounds = 40;

	//Recoil
	Recoil = 2;

	//Projectile Speed
	ProjSpeed = 600;

	//scope
	Scope = 10;

	//weight
	weight = 1.3;

	//damage
	damage = 30;
	//Reload Speed 4 s

	m_sWeaponName = "Pump Shotgun";
	m_sAmmoType = "12 Gauge";

	// The time between shots
	timeBetweenShots = 0.8;
	// The elapsed time (between shots)
	elapsedTime = 0.0;
	// Boolean flag to indicate if weapon can fire now
	bFire = true;

	CWeaponInfo::SetMaxTotalClips(100);
	CWeaponInfo::SetTotalClips(5);
}
