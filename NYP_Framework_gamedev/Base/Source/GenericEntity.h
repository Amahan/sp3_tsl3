#ifndef GENERIC_ENTITY_H
#define GENERIC_ENTITY_H

#include "EntityBase.h"
#include <string>
#include "Collider/Collider.h"
#include "PlayerInfo\PlayerInfo.h"
#include "CameraEffects\CameraEffects.h"

class Mesh;

class GenericEntity : public EntityBase, public CCollider
{
public:
	GenericEntity(Mesh* _modelMesh);
	virtual ~GenericEntity();

	virtual void Update(double _dt);
	virtual void Render();

	// Set the maxAABB and minAABB
	void SetAABB(Vector3 maxAABB, Vector3 minAABB);

	bool CheckOverlap(Vector3 thisMinAABB, Vector3 thisMaxAABB, Vector3 playerposmax,Vector3 playerposmin);
	bool Collision(void);
	bool Interaction(void);

	void SetInteractionAABB(Vector3 InteractionMax, Vector3 InteractionMin);

	float angle;

	bool CanInteract;

	bool InInteractionRadius;

	bool MoveGateX;
	bool MoveGateZ;

	float translateX;
	float translateZ;

	bool SurpriseMove;

private:
	Mesh* modelMesh;

	Vector3 currentpos;
	Vector3 currenttarget;

	Vector3 Interactionmin;
	Vector3 Interactionmax;
};

namespace Create
{
	GenericEntity* Entity(	const std::string& _meshName, 
							const Vector3& _position, 
							const Vector3& _scale = Vector3(1.0f, 1.0f, 1.0f));
};

#endif // GENERIC_ENTITY_H