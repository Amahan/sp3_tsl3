#include "EntityManager.h"
#include "EntityBase.h"
#include "Collider/Collider.h"
#include "GenericEntity.h"

#include <iostream>
using namespace std;

// Update all entities
void EntityManager::Update(double _dt)
{
	CheckForCollision();


	// Update all entities
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		if ((*it)->GetIsBullet() == true)
		{
	//		(*it)->SetVel((*it)->GetVel() + Vector3(0, -9.8, 0));
		}
		(*it)->Update(_dt);
	}

	// Clean up entities that are done
	it = entityList.begin();
	while (it != end)
	{
		if ((*it)->IsDone())
		{
			if (((*it))->GetIsEnemy() == true)
			{
				if ((CPlayerInfo::GetInstance()->characterClass == CPlayerInfo::GetInstance()->SCAVENGER) && (CPlayerInfo::GetInstance()->characterSkillLevel >= 1))
				{
					CPlayerInfo::GetInstance()->gibAmmo();
				}
			}
			// Delete if done
			delete *it;
			it = entityList.erase(it);
			
		}
		else
		{
			// Move on otherwise
			++it;
		}
	}

}

void EntityManager::Update(CWeatherInfo * weather, double dt)
{
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		if (*it)
		{
			if ((*it)->GetIsEnemy() == true)
			{
				(*it)->Update(weather, dt);
			}
		}
	}
}

// Render all entities
void EntityManager::Render()
{
	// Render all entities
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		(*it)->Render();
	}
}

// Render the UI entities
void EntityManager::RenderUI()
{
	// Render all entities UI
	std::list<EntityBase*>::iterator it, end;
	end = entityList.end();
	for (it = entityList.begin(); it != end; ++it)
	{
		(*it)->RenderUI();
	}
}

// Add an entity to this EntityManager
void EntityManager::AddEntity(EntityBase* _newEntity)
{
	entityList.push_back(_newEntity);
}

// Remove an entity from this EntityManager
bool EntityManager::RemoveEntity(EntityBase* _existingEntity)
{
	// Find the entity's iterator
//	std::list<EntityBase*>::iterator findIter = std::find(entityList.begin(), entityList.end(), _existingEntity);

	

	for (auto eo : entityList)
	{
		if (eo)
		{
			if (eo == _existingEntity)
			{
				delete eo;
				eo = NULL;
				return true;
			}
		}
	}

	return false;

	// Delete the entity if found
//	if (findIter != entityList.end())
//	{
//		delete *findIter;
//		findIter = entityList.erase(findIter);
//		return true;	
//	}
	// Return false if not found
//	return false;



}

// Constructor
EntityManager::EntityManager()
{
}

// Destructor
EntityManager::~EntityManager()
{
	if (entityList.size() > 0)
	{
		for (std::list<EntityBase*>::iterator it = entityList.begin(); it != entityList.end(); ++it)
		{
			EntityBase* eo = (EntityBase*)*it;
			if (eo)
			{
				delete eo;
				eo = NULL;
			}
		}
		std::list<EntityBase*>().swap(entityList);
	}
}

// Check for overlap
bool EntityManager::CheckOverlap(Vector3 thisMinAABB, Vector3 thisMaxAABB, Vector3 thatMinAABB, Vector3 thatMaxAABB)
{	
	// Check if this object is overlapping that object
	if (((thatMinAABB >= thisMinAABB) && (thatMinAABB <= thisMaxAABB))
		||
		((thatMaxAABB >= thisMinAABB) && (thatMaxAABB <= thisMaxAABB)))
	{
		return true;
	}

	// Check if that object is overlapping this object
	if (((thisMinAABB >= thatMinAABB) && (thisMinAABB <= thatMaxAABB))
		||
		((thisMaxAABB >= thatMinAABB) && (thisMaxAABB <= thatMaxAABB)))
	{
		return true;
	}

	// Check if this object is within that object
	if (((thisMinAABB >= thatMinAABB) && (thisMaxAABB <= thatMaxAABB))
		&&
		((thisMaxAABB >= thatMinAABB) && (thisMaxAABB <= thatMaxAABB)))
		return true;

	// Check if that object is within this object
	if (((thatMinAABB >= thisMinAABB) && (thatMinAABB <= thisMaxAABB))
		&&
		((thatMaxAABB >= thisMinAABB) && (thatMaxAABB <= thisMaxAABB)))
		return true;
	return false;
}

// Check if this entity's bounding sphere collided with that entity's bounding sphere 
bool EntityManager::CheckSphereCollision(EntityBase *ThisEntity, EntityBase *ThatEntity)
{
	// Get the colliders for the 2 entities
	CCollider *thisCollider = dynamic_cast<CCollider*>(ThisEntity);
	CCollider *thatCollider = dynamic_cast<CCollider*>(ThatEntity);

	// Get the minAABB and maxAABB for each entity
	Vector3 thisMinAABB = ThisEntity->GetPosition() + thisCollider->GetMinAABB();
	Vector3 thisMaxAABB = ThisEntity->GetPosition() + thisCollider->GetMaxAABB();
	Vector3 thatMinAABB = ThatEntity->GetPosition() + thatCollider->GetMinAABB();
	Vector3 thatMaxAABB = ThatEntity->GetPosition() + thatCollider->GetMaxAABB();

	// if Radius of bounding sphere of ThisEntity plus Radius of bounding sphere of ThatEntity is 
	// greater than the distance squared between the 2 reference points of the 2 entities,
	// then it could mean that they are colliding with each other.
	if (DistanceSquaredBetween(thisMinAABB, thisMaxAABB) + DistanceSquaredBetween(thatMinAABB, thatMaxAABB) >
		DistanceSquaredBetween(ThisEntity->GetPosition(), ThatEntity->GetPosition()) * 2.0)
	{
		return true;
	}

	return false;
}

// Check if this entity collided with another entity, but both must have collider
bool EntityManager::CheckAABBCollision(EntityBase *ThisEntity, EntityBase *ThatEntity)
{
	// Get the colliders for the 2 entities
	CCollider *thisCollider = dynamic_cast<CCollider*>(ThisEntity);
	CCollider *thatCollider = dynamic_cast<CCollider*>(ThatEntity);

	// Get the minAABB and maxAABB for each entity
	Vector3 thisMinAABB = ThisEntity->GetPosition() + thisCollider->GetMinAABB();
	Vector3 thisMaxAABB = ThisEntity->GetPosition() + thisCollider->GetMaxAABB();
	Vector3 thatMinAABB = ThatEntity->GetPosition() + thatCollider->GetMinAABB();
	Vector3 thatMaxAABB = ThatEntity->GetPosition() + thatCollider->GetMaxAABB();

	// Check for overlap
	if (CheckOverlap(thisMinAABB, thisMaxAABB, thatMinAABB, thatMaxAABB))
		return true;

	// if AABB collision check fails, then we need to check the other corners of the bounding boxes to 
	// Do more collision checks with other points on each bounding box
	Vector3 altThisMinAABB = Vector3(thisMinAABB.x, thisMinAABB.y, thisMaxAABB.z);
	Vector3 altThisMaxAABB = Vector3(thisMaxAABB.x, thisMaxAABB.y, thisMinAABB.z);
	//	Vector3 altThatMinAABB = Vector3(thatMinAABB.x, thatMinAABB.y, thatMaxAABB.z);
	//	Vector3 altThatMaxAABB = Vector3(thatMaxAABB.x, thatMaxAABB.y, thatMinAABB.z);

	// Check for overlap
	if (CheckOverlap(altThisMinAABB, altThisMaxAABB, thatMinAABB, thatMaxAABB))
		return true;

	return false;
}

// Check if any Collider is colliding with another Collider
bool EntityManager::CheckForCollision(void)
{
	// Check for Collision
	std::list<EntityBase*>::iterator colliderThis, colliderThisEnd;
	std::list<EntityBase*>::iterator colliderThat, colliderThatEnd;

	colliderThisEnd = entityList.end();
	for (colliderThis = entityList.begin(); colliderThis != colliderThisEnd; ++colliderThis)
	{
		if ((*colliderThis)->HasCollider())
		{
			// This object was derived from a CCollider class, then it will have Collision Detection methods
			//CCollider *thisCollider = dynamic_cast<CCollider*>(*colliderThis);
			EntityBase *thisEntity = dynamic_cast<EntityBase*>(*colliderThis);
			// Check for collision with another collider class
			colliderThatEnd = entityList.end();
			int counter = 0;
			if (thisEntity->GetIsNade())
			{
				if (thisEntity->GetPosition().y - thisEntity->GetScale().y < -10)
				{
					Vector3 u1 = thisEntity->GetVel();

					Vector3 N = Vector3(0, 1, 0).Normalized();
					thisEntity->SetVel(((u1 - ((2 * u1.Dot(N)) * N))));
				}
			}

			for (colliderThat = colliderThis; colliderThat != colliderThatEnd; ++colliderThat)
			{
				if (colliderThat == colliderThis)
					continue;

				if ((*colliderThat)->HasCollider())
				{
					// This object was derived from a CCollider class, then it will have Collision Detection methods
					EntityBase *thatEntity = dynamic_cast<EntityBase*>(*colliderThat);

					//dont bother collsion
					if (thisEntity->GetIsBullet() && thatEntity->GetIsBullet())
						continue;

					if (thisEntity->GetIsEnemy() && thatEntity->GetIsEnemy())
					{
						if (CheckAABBCollision(thisEntity, thatEntity))
						{
							if (thatEntity->GetPosition() == thisEntity->GetPosition())
							{
								Vector3 dist = (thatEntity->GetScale() + thisEntity->GetScale()).Normalized();
								thisEntity->SetVel(thisEntity->GetVel() - dist);
								thatEntity->SetVel(thatEntity->GetVel() + dist);
							}
							else
							{
								Vector3 dist = (thatEntity->GetPos() - thisEntity->GetPos()).Normalized();
								thisEntity->SetVel(thisEntity->GetVel() - dist);
								thatEntity->SetVel(thatEntity->GetVel() + dist);
							}
						}
					}
						

					if (!thisEntity->GetIsEnemy() && !thatEntity->GetIsEnemy() && !thisEntity->GetIsBullet() && !thatEntity->GetIsBullet())
						continue;

					if (thisEntity->GetIsBullet() && thatEntity->GetIsBullet())
						continue;

					if (thisEntity->IsDone() && thatEntity->IsDone())
						continue;

					//enemy and object collision
					if (!thisEntity->GetIsEnemy() && thatEntity->GetIsEnemy())
					{
						if (CheckAABBCollision(thisEntity, thatEntity) == true)
						{
							if (thatEntity->GetPos() == thisEntity->GetPos())
							{
								thisEntity->SetPos(thisEntity->GetPos() + Vector3(thisEntity->GetScale().x, 0, thisEntity->GetScale().z));
								thatEntity->SetPos(thatEntity->GetPos() + Vector3(thatEntity->GetScale().x, 0, thatEntity->GetScale().z));
							}
							else
							{
								Vector3 distbetween = (thatEntity->GetPosition() - thisEntity->GetPosition()).Normalized();
								thatEntity->SetVel(thatEntity->GetVel() + distbetween);
								
							}
						}
					
					}

					if (thisEntity != thatEntity)
					{
						if ((!thisEntity->GetIsEnemy() && !thisEntity->GetIsBullet()) && (thatEntity->GetIsNade()))
						{
							bool isGoing = false;
							if ((thatEntity->GetPos() - thisEntity->GetPos()).LengthSquared() < 30 * 30)
							{
								Vector3 dir = (thisEntity->GetPosition() - thatEntity->GetPosition()).Normalized();

								float check = thatEntity->GetVel().Dot(dir);

								if (check < 0)
								{
									isGoing = false;
								}
								else
								{
									isGoing = true;
								}
							}
							if (!isGoing)
							{
								break;
							}
							else
							{
								if (CheckAABBCollision(thisEntity, thatEntity))
								{
									CCollider *thisCollider = dynamic_cast<CCollider*>(thisEntity);
									CCollider *thatCollider = dynamic_cast<CCollider*>(thatEntity);
									if (thatEntity->GetPos().x + thatCollider->GetMinAABB().x > thisEntity->GetPos().x - thisCollider->GetMaxAABB().x || thatEntity->GetPos().x - thatCollider->GetMaxAABB().x > thisEntity->GetPos().x + thisCollider->GetMinAABB().x )
									{
										Vector3 u1 = thatEntity->GetVel();

										

										Vector3 dir = (thisEntity->GetPosition() - thatEntity->GetPosition()).Normalized();

										float check = thatEntity->GetVel().Dot(dir);

										Vector3 N = Vector3(1, 0, 0).Normalized();

										thatEntity->SetVel((u1 - ((2 * u1.Dot(N)) * N)));

									}
									else if (thatEntity->GetPos().z + thatCollider->GetMinAABB().z > thisEntity->GetPos().z - thisCollider->GetMaxAABB().z || thatEntity->GetPos().z - thatCollider->GetMaxAABB().z > thisEntity->GetPos().z + thisCollider->GetMinAABB().z)
									{
										Vector3 u1 = thatEntity->GetVel();

										Vector3 dir = (thisEntity->GetPosition() - thatEntity->GetPosition()).Normalized();

										float check = thatEntity->GetVel().Dot(dir);

										Vector3 N = Vector3(0, 0, 1).Normalized();
										thatEntity->SetVel((u1 - ((2 * u1.Dot(N)) * N)));
									}

									if (thatEntity->GetPos().z + thatCollider->GetMinAABB().z > thisEntity->GetPos().z - thisCollider->GetMaxAABB().z || thatEntity->GetPos().z - thatCollider->GetMaxAABB().z > thisEntity->GetPos().z + thisCollider->GetMinAABB().z && thatEntity->GetPos().x + thatCollider->GetMinAABB().x < thisEntity->GetPos().x - thisCollider->GetMaxAABB().x || thatEntity->GetPos().z - thatCollider->GetMaxAABB().x < thisEntity->GetPos().x + thisCollider->GetMinAABB().x)
									{
										Vector3 u1 = thatEntity->GetVel();

										Vector3 N = Vector3(0, 0, 1).Normalized();
										thatEntity->SetVel((u1 - ((2 * u1.Dot(N)) * N)));
									}
									else if (thatEntity->GetPosition().y + thatEntity->GetScale().y > thisEntity->GetPosition().y - thisEntity->GetScale().y)
									{
										Vector3 u1 = thatEntity->GetVel();

										Vector3 N = Vector3(0, 1, 0).Normalized();
										thatEntity->SetVel((u1 - ((2 * u1.Dot(N)) * N)));
									}
								}
							}
						}
					}
					

					//bullet collision
					if (CheckSphereCollision(thisEntity, thatEntity) == true)
					{

						if (CheckAABBCollision(thisEntity, thatEntity) == true)
						{
							//bullet collision and enemy
							if (thisEntity->GetIsBullet() && thatEntity->GetIsEnemy())
							{
								if (thatEntity->Gethp() > 0)
								{
									thatEntity->Sethp(thatEntity->Gethp() - (CPlayerInfo::GetInstance()->GetWeaponDamage() * CPlayerInfo::GetInstance()->getCharacterSkillDamageModifier()));
									if (CPlayerInfo::GetInstance()->GetWeaponName() != "Railgun")
									{
										thisEntity->SetIsDone(true);
									}
								}
							}

							//bullet collision and enemy
							else if (thatEntity->GetIsBullet() && thisEntity->GetIsEnemy())
							{
								if (thisEntity->Gethp() > 0)
								{
									thisEntity->Sethp(thisEntity->Gethp() - (CPlayerInfo::GetInstance()->GetWeaponDamage() * CPlayerInfo::GetInstance()->getCharacterSkillDamageModifier()));
									if (CPlayerInfo::GetInstance()->GetWeaponName() != "Railgun")
									{
										thatEntity->SetIsDone(true);
									}
								}
							}

							//object and bullet collision
							if (thisEntity->GetIsBullet() && !thatEntity->GetIsEnemy() && !thisEntity->GetIsNade())
							{
								thisEntity->SetIsDone(true);
							}
							else if (!thisEntity->GetIsEnemy() && thatEntity->GetIsBullet() && !thatEntity->GetIsNade())
							{
								thatEntity->SetIsDone(true);
							}

							if (thatEntity->GetIsEnemy() && thatEntity->Gethp() <=0)
							{
								thatEntity->SetIsDone(true);
							}

							if (thisEntity->GetIsEnemy() && thisEntity->Gethp() <= 0)
							{
								thisEntity->SetIsDone(true);
							}

							 break;
							 


						}
					}


				}
			
			}
		}
	}
	return false;
}
