#pragma once

#include "../PlayerInfo/PlayerInfo.h"

class CController
{
protected:
	CPlayerInfo* thePlayerInfo;

public:
	CController();
	virtual ~CController();

	// Create this controller
	virtual bool Create(CPlayerInfo* thePlayerInfo = NULL);
	// Read from the controller
	virtual int Read(const float deltaTime);

	// Detect and process front / back movement on the controller
	virtual bool Move_FrontBack(const float deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and process left / right movement on the controller
	virtual bool Move_LeftRight(const float deltaTime, const bool direction);
	// Detect and process look up / down on the controller
	virtual bool Look_UpDown(const float deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and process look left / right on the controller
	virtual bool Look_LeftRight(const float deltaTime, const bool direction, const float speedMultiplier = 1.0f);
	// Detect and process item usage on the controller
	virtual bool Use_Item(const float deltaTime);
	// Detect and process throw grenade on the controller
	virtual bool Use_Grenade(const float deltaTime);
	// Stop sway
	virtual bool StopSway(const float deltaTime);
	// Posture change
	virtual bool ChangePosture(const float deltaTime);

	// Jump
	virtual bool Jump(const float deltaTime);

	// Reload current weapon
	virtual bool Reload(const float deltaTime);
	// Change current weapon (for primary only)
	virtual bool Change(const float deltaTime);
	// Fire primary weapon
	virtual bool FirePrimary(const float deltaTime);
	// Fire secondary weapon
	virtual bool FireSecondary(const float deltaTime);

	//zoom
	virtual bool Zoom(const bool scoped, const bool deltatime);

	// Reset the PlayerInfo
	virtual bool Reset(void);

	//jetpack
	virtual bool JetpackEnabled(const bool fly, const float dt);

	//Activate Ability
	virtual bool CallAbility(void);

	// Load this class
	bool Load(const string saveFileName = ".//Image//Controls.sav");
	// Save this class
	bool Save(const string saveFileName = ".//Image//Controls.sav");

	//movement keys
	char MoveForward, MoveBackwards, MoveLeft, MoveRight, Crouch, Reloading;
};

