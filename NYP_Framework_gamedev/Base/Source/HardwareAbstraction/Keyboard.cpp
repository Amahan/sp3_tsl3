#include "Keyboard.h"
#include <iostream>
using namespace std;

#include "KeyboardController.h"
#include "../PlayerInfo/PlayerInfo.h"

const bool _CONTROLLER_KEYBOARD_DEBUG = false;

CKeyboard::CKeyboard()
{
}


CKeyboard::~CKeyboard()
{
}

// Create this controller
bool CKeyboard::Create(CPlayerInfo* thePlayerInfo)
{
	CController::Create(thePlayerInfo);
	if (_CONTROLLER_KEYBOARD_DEBUG)
		cout << "CKeyboard::Create()" << endl;
	return false;
}


// Read from the controller
int CKeyboard::Read(const float deltaTime)
{
	CController::Read(deltaTime);
	CController::Load();
	if (_CONTROLLER_KEYBOARD_DEBUG)
		cout << "CKeyboard::Read()" << endl;
	
	// Process the keys for customisation
	if ((KeyboardController::GetInstance()->IsKeyDown(MoveForward)) && (KeyboardController::GetInstance()->IsKeyDown(VK_SHIFT)) && CPlayerInfo::GetInstance()->Stamina > 0)
		Move_FrontBack(deltaTime, true, 2.0f);
	else if (KeyboardController::GetInstance()->IsKeyDown(MoveForward))
		Move_FrontBack(deltaTime, true);
	else if (KeyboardController::GetInstance()->IsKeyReleased(MoveForward))
		StopSway(deltaTime);
	if (KeyboardController::GetInstance()->IsKeyDown(MoveBackwards))
		Move_FrontBack(deltaTime, false);
	else if (KeyboardController::GetInstance()->IsKeyReleased(MoveBackwards))
		StopSway(deltaTime);
	if (KeyboardController::GetInstance()->IsKeyReleased(VK_SHIFT))
		StopSway(deltaTime);
	if (KeyboardController::GetInstance()->IsKeyReleased(Crouch))
	{
		ChangePosture(deltaTime);
		StopSway(deltaTime);
	}

	if (KeyboardController::GetInstance()->IsKeyDown(MoveLeft))
		Move_LeftRight(deltaTime, true);
	if (KeyboardController::GetInstance()->IsKeyDown(MoveRight))
		Move_LeftRight(deltaTime, false);

	if (KeyboardController::GetInstance()->IsKeyDown(VK_UP))
		Look_UpDown(deltaTime, true);
	if (KeyboardController::GetInstance()->IsKeyDown(VK_DOWN))
		Look_UpDown(deltaTime, false);
	if (KeyboardController::GetInstance()->IsKeyDown(VK_LEFT))
		Look_LeftRight(deltaTime, true);
	if (KeyboardController::GetInstance()->IsKeyDown(VK_RIGHT))
		Look_LeftRight(deltaTime, false);

	// If the user presses SPACEBAR, then make him jump
	if (KeyboardController::GetInstance()->IsKeyPressed(VK_SPACE) && thePlayerInfo->isOnGround())
	{
		Jump(true);
	}

	if (KeyboardController::GetInstance()->IsKeyDown(VK_SPACE) && !thePlayerInfo->isOnGround())
	{
		JetpackEnabled(true, deltaTime);
	}

	else if (KeyboardController::GetInstance()->IsKeyReleased(VK_SPACE))
	{
		JetpackEnabled(false, deltaTime);
	}

	if (KeyboardController::GetInstance()->IsKeyPressed('F'))
	{
		Use_Item(deltaTime);
	}

	if (KeyboardController::GetInstance()->IsKeyPressed('G'))
	{
		Use_Grenade(deltaTime);
	}

	if (KeyboardController::GetInstance()->IsKeyPressed('Q'))
	{
		CallAbility();
	}

	// Update the weapons
	if (KeyboardController::GetInstance()->IsKeyReleased(Reloading))
		Reload(deltaTime);

	// If the user presses R key, then reset the view to default values
	if (KeyboardController::GetInstance()->IsKeyDown('P'))
		Reset();

	return 0;
}
