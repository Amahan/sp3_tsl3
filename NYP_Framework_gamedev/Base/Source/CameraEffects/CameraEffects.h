#pragma once

#include "EntityBase.h"
#include "SingletonTemplate.h"
#include "Mesh.h"
#include "../Application.h"

class CCameraEffects : public EntityBase, public Singleton<CCameraEffects>
{
public:
	enum SPRITE_RENDERMODE
	{
		MODE_2D,
		MODE_3D,
		NUM_MODE
	};

	CCameraEffects(void);
	virtual ~CCameraEffects(void);

	Mesh* m_CameraEffects_BloodyScreen;
	Mesh* m_CameraEffects_Scope;
	Mesh* m_CameraEffects_RailScope;
	Mesh* m_SmokeScreen;

	// Initialise this class instance
	bool Init(void);

	// Set the BloodScreen
	bool SetBloodScreen(Mesh* aTarget);
	// Get the BloodScreen
	Mesh* GetBloodScreen(void) const;

	// Set the Scope
	bool SetScope(Mesh* aTarget);

	// Set the Railgun Scope
	bool SetRailgunScope(Mesh* aTarget);

	// Set the SmokeScreen Scope
	bool SetSmokeScreen(Mesh* aTarget);

	//Get Railgun scope
	Mesh* GetRailScope(void) const;

	//Get scope
	Mesh* GetScope(void) const;

	//Get scope
	Mesh* GetSmokeScreen(void) const;

	// Set rotation angle of CCameraEffect
	bool SetAngle(const int angle);
	// Get rotation angle of CCameraEffect
	int GetAngle(void) const;

	// Set boolean flag for BloodScreen
	bool SetStatus_BloodScreen(const bool m_bStatusBloodScreen);

	// Set boolean flag for scope
	bool SetStatus_Scope(const bool m_Scope);

	// Set boolean flag for scope
	bool SetStatus_RailgunScope(const bool m_RailScope);

	// Set boolean flag for scope
	bool SetStatus_SmokeScreen(const bool smokescreen);

	// Set Countdown Timer for BloodScreen
	bool SetTimer_BloodScreen(const float m_fCountdownTimer_BloodScreen = 2.0f);
	// Get Countdown Timer for BloodScreen
	float GetTimer_BloodScreen(void) const;

	// Set Countdown Timer for SmokeScreen
	bool SetTimer_SmokeScreen(const float m_fCountdownTimer_SmokeScreen = 2.0f);
	// Get Countdown Timer for SmokeScreen
	float GetTimer_SmokeScreen(void) const;

	// Update the camera effects
	virtual void Update(const float dt = 0.0333f);
	virtual void Update(CWeatherInfo* weather, double dt);
	// Render the UI
	virtual void RenderUI();

protected:
	// Rotation from First Angle
	int m_iAngle;
	// Boolean flag to indicate if the BloodScreen is rendered
	bool m_bStatusBloodScreen;
	//bool for scope
	bool m_bStatusScope;
	//bool for scope
	bool m_bStatusRailgunScope;

	//bool for smokescreen
	bool m_bStatusSmokeScreen;

	// Countdown Timer for BloodScreen
	float m_fCountdownTimer_BloodScreen;

	// Countdown Timer for SmokeScreen
	float m_fCountdownTimer_SmokeScreen;



	SPRITE_RENDERMODE mode;
};

namespace Create
{
	CCameraEffects* CameraEffects(const bool m_bAddToLibrary = true);
};
