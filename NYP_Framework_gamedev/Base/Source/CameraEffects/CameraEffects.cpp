#include "CameraEffects.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"
#include "../EntityManager.h"
#include "GL\glew.h"

CCameraEffects::CCameraEffects(void)
	: m_iAngle(-90)
	, m_bStatusBloodScreen(false)
	, m_CameraEffects_BloodyScreen(NULL)
	, m_fCountdownTimer_BloodScreen(2.0f)
	, m_fCountdownTimer_SmokeScreen(2.0f)
	, mode(MODE_2D)
{
	Init();
}
CCameraEffects::~CCameraEffects(void)
{
	if (m_CameraEffects_BloodyScreen)
	{
		delete m_CameraEffects_BloodyScreen;
		m_CameraEffects_BloodyScreen = NULL;
	}
}

// Initialise this class instance
bool CCameraEffects::Init(void)
{
	m_iAngle = -90;
	position.Set(0.0f, 0.0f, 0.0f);
	scale.Set(800.0f, 600.0f, 100.0f);

	return true;
}

bool CCameraEffects::SetBloodScreen(Mesh* aBloodScreen)
{
	if(aBloodScreen != NULL)
	{
		m_CameraEffects_BloodyScreen = aBloodScreen;
		return true;
	}
	return false;
}
Mesh* CCameraEffects::GetBloodScreen(void) const
{
	return m_CameraEffects_BloodyScreen;
}

bool CCameraEffects::SetScope(Mesh * aScope)
{
	if (aScope != NULL)
	{
		m_CameraEffects_Scope = aScope;
		return true;
	}
	return false;
}

bool CCameraEffects::SetRailgunScope(Mesh * arailScope)
{
	if (arailScope != NULL)
	{
		m_CameraEffects_RailScope = arailScope;
		return true;
	}
	return false;
}

bool CCameraEffects::SetSmokeScreen(Mesh * Smoke)
{
	if (Smoke != NULL)
	{
		m_SmokeScreen = Smoke;
		return true;
	}
	return false;
}

Mesh * CCameraEffects::GetRailScope(void) const
{
	return m_CameraEffects_RailScope;
}

Mesh * CCameraEffects::GetScope(void) const
{
	return m_CameraEffects_Scope;
}

Mesh * CCameraEffects::GetSmokeScreen(void) const
{
	return m_SmokeScreen;
}

// Set m_iAngle of avatar
bool CCameraEffects::SetAngle(const int m_iAngle)
{
	this->m_iAngle = m_iAngle;
	return true;
}
// Get m_iAngle
int CCameraEffects::GetAngle(void) const
{
	return m_iAngle;
}

// Set boolean flag for BloodScreen
bool CCameraEffects::SetStatus_BloodScreen(const bool m_bStatusBloodScreen)
{
	this->m_bStatusBloodScreen = m_bStatusBloodScreen;
	return true;
}

bool CCameraEffects::SetStatus_Scope(const bool m_Scope)
{
	this->m_bStatusScope = m_Scope;
	return true;
}

bool CCameraEffects::SetStatus_RailgunScope(const bool m_RailScope)
{
	this->m_bStatusRailgunScope = m_RailScope;
	return true;
}

bool CCameraEffects::SetStatus_SmokeScreen(const bool smokescreen)
{
	this->m_bStatusSmokeScreen = smokescreen;
	return true;
}

// Set Countdown Timer for BloodScreen
bool CCameraEffects::SetTimer_BloodScreen(const float m_fCountdownTimer_BloodScreen)
{
	this->m_fCountdownTimer_BloodScreen = m_fCountdownTimer_BloodScreen;
	return true;
}

// Get Countdown Timer for BloodScreen
float CCameraEffects::GetTimer_BloodScreen(void) const
{
	return m_fCountdownTimer_BloodScreen;
}

bool CCameraEffects::SetTimer_SmokeScreen(const float m_fCountdownTimer_SmokeScreen)
{
	this->m_fCountdownTimer_SmokeScreen = m_fCountdownTimer_SmokeScreen;
	return true;
}

float CCameraEffects::GetTimer_SmokeScreen(void) const
{
	return m_fCountdownTimer_SmokeScreen;
}

// Update the camera effects
void CCameraEffects::Update(const float dt)
{
	if (m_bStatusBloodScreen)
	{
		m_fCountdownTimer_BloodScreen -= dt;
		if (m_fCountdownTimer_BloodScreen <= 0.0f)
		{
			// Set the bool flag for Render BloodScreen to false
			m_bStatusBloodScreen = false;
			// Reset the countdown timer to the default value
			m_fCountdownTimer_BloodScreen = 2.0f;
		}
	}

	if (m_bStatusSmokeScreen)
	{
		m_fCountdownTimer_SmokeScreen -= dt;
		if (m_fCountdownTimer_SmokeScreen <= 0.0f)
		{
			// Set the bool flag for Render SmokeScreen to false
			m_bStatusSmokeScreen = false;
			// Reset the countdown timer to the default value
			m_fCountdownTimer_SmokeScreen = 2.0f;
		}
	}
}

void CCameraEffects::Update(CWeatherInfo * weather, double dt)
{
}

// Render the UI
void CCameraEffects::RenderUI()
{
	if (mode == MODE_3D)
		return;

	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();

	// Push the current transformation into the modelStack
	modelStack.PushMatrix();
		// Translate the current transformation
		modelStack.Translate(position.x, position.y, position.z);
		// Scale the current transformation
		modelStack.Scale(scale.x, scale.y, scale.z);

		// Push the current transformation into the modelStack
		modelStack.PushMatrix();
			// Display the Avatar
			if ((m_bStatusBloodScreen) && (m_CameraEffects_BloodyScreen))
			{
				RenderHelper::RenderMesh(m_CameraEffects_BloodyScreen);
			}
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		// Display the Avatar
		if ((m_bStatusScope) && (m_CameraEffects_Scope))
		{
			RenderHelper::RenderMesh(m_CameraEffects_Scope);
		}
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		// Display the Avatar
		if ((m_bStatusRailgunScope) && (m_CameraEffects_RailScope))
		{
			RenderHelper::RenderMesh(m_CameraEffects_RailScope);
		}
		modelStack.PopMatrix();

		modelStack.PushMatrix();
		// Display the Avatar
		if ((m_bStatusSmokeScreen) && (m_SmokeScreen))
		{
			RenderHelper::RenderMesh(m_SmokeScreen,0.3);
		}
		modelStack.PopMatrix();

	modelStack.PopMatrix();

}

CCameraEffects* Create::CameraEffects(const bool m_bAddToLibrary)
{
	CCameraEffects* result = CCameraEffects::GetInstance();
	if (m_bAddToLibrary)
		EntityManager::GetInstance()->AddEntity(result);
	return result;
}
