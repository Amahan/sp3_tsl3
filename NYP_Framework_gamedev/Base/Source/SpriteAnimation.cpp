#include "SpriteAnimation.h"

SpriteAnimation::SpriteAnimation(const std::string &meshName, int row, int col) : Mesh(meshName)
, m_row(row)
, m_col(col)
, m_currentTime(0)
, m_currentFrame(0)
, m_playCount(0) 
{
	m_anim = NULL;
}

SpriteAnimation::~SpriteAnimation() 
{ 
	if (m_anim)   
	delete m_anim; 
	m_anim = NULL;
}

void SpriteAnimation::Update(double dt)
{
	if (m_anim->animActive == true)
	{
		m_currentTime += static_cast<float>(dt);

		//Find int numframe, myhMath.h -- Max
		int numFrame = Math::Max(1, m_anim->endFrame - m_anim->startFrame + 1);
		//endframe-startframe = a number that is less than or 0

		//Get float frameTime
		//FrameTime = animTime / numFrame
		float FrameTime = m_anim->animTime / numFrame;

		//Get Animation's current frame, m_currentFrame
		m_currentFrame = Math::Min(m_anim->endFrame, m_anim->startFrame + static_cast<int> (m_currentTime / FrameTime));

		//Check if m_currentTime is greater than or equal animTime
		if (m_currentTime >= m_anim->animTime)
		{
			//check if repeatCount is 0
			if (m_anim->repeatCount == 0)
			{
				//set AnimActive = false
				//reset m_currentTime
				//set m_currentFrame = startFrame
				m_anim->animActive = false;
				m_currentTime = 0.0f;
				m_currentFrame = m_anim->startFrame;
				
				//Set animation end flag to true
				m_anim->ended = true;
			}

			if (m_anim->repeatCount == 1)
			{
				//reset m_currentTime
				//set m_currentFrame = startFrame
				m_currentTime = 0.0f;
				m_currentFrame = m_anim->startFrame;
			}
		}
	}
}

void SpriteAnimation::Render() 
{
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)sizeof(Position));
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(Position) + sizeof(Color)));
	//if(textureID > 0)
	{
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(Position) + sizeof(Color) + sizeof(Vector3)));
	}


	//glDrawArrays(GL_TRIANGLES, 0, 3);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);

	if (mode == DRAW_LINES)

		glDrawElements(GL_LINES, 6, GL_UNSIGNED_INT, (void*)(m_currentFrame * 6 * sizeof(GLuint)));

	else if (mode == DRAW_TRIANGLE_STRIP)

		glDrawElements(GL_TRIANGLE_STRIP, 6, GL_UNSIGNED_INT, (void*)(m_currentFrame * 6 * sizeof(GLuint)));

	else   glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void*)(m_currentFrame * 6 * sizeof(GLuint)));



	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);

	//if(textureID > 0)
	{
		glDisableVertexAttribArray(3);
	}
}