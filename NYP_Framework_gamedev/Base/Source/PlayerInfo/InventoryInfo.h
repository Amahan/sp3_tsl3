#pragma once

#include <vector>
#include "Vector3.h"
#include "../ItemInfo/ItemInfo.h"

class CPlayerInfo;

class CInventoryInfo
{
public:
	CInventoryInfo();
	virtual ~CInventoryInfo();
protected:
	std::vector<CItemInfo *>theItemList;
	

public:

	// Fetch the item list
	CItemInfo *FetchItem(void);
	// Pickup Item
	void AddToInventory(CItemInfo* add);
	// Initialise this instance to default values
	virtual void Init(void);
	// Update the elapsed time
	void Update(const double dt);

	// Print Self
	void PrintSelf(void);


	virtual void Delete(void);
};
