#include "PlayerInfo.h"
#include <iostream>

#include "MouseController.h"
#include "KeyboardController.h"
#include "Mtx44.h"
#include "../Projectile/Projectile.h"
#include "../WeaponInfo/Pistol.h"
#include "../WeaponInfo/Revolver.h"
#include "../WeaponInfo/Handcannon.h"
#include "../WeaponInfo/Rifle.h"
#include "../WeaponInfo/Scar.h"
#include "../WeaponInfo/SMG.h"
#include "../WeaponInfo/Sniper.h"
#include "../WeaponInfo/Shotgun.h"
#include "../WeaponInfo/Railgun.h"

#include "../ItemInfo/Bandage.h"
#include "../ItemInfo/Grenade.h"
#include "../ItemInfo/Ammo.h"

#include "../Minimap/Minimap.h"

// Allocating and initializing CPlayerInfo's static data member.  
// The pointer is allocated but not the object's constructor.
CPlayerInfo *CPlayerInfo::s_instance = 0;

CPlayerInfo::CPlayerInfo(void)
	: m_dSpeed(40.0)
	, m_dAcceleration(10.0)
	, m_bJumpUpwards(false)
	, m_dJumpSpeed(30.0)
	, m_dJumpAcceleration(-10.0)
	, m_bFallDownwards(false)
	, m_dFallSpeed(0.0)
	, m_dFallAcceleration(-10.0)
	, m_dElapsedTime(0.0)
	, attachedCamera(NULL)
	, m_pTerrain(NULL)
	, primaryWeapon(NULL)
	, secondaryWeapon(NULL)
	, theCurrentPosture(STAND)
	, weaponManager(NULL)
	, m_iCurrentWeapon(0)
	, itemManager(NULL)
	, m_iCurrentItem(0)
	, clipManager(NULL)
	, m_fCameraSwayAngle(0.0f)
	, m_fCameraSwayDeltaAngle(0.1f)
	, m_fCameraSwayAngle_LeftLimit(-0.3f)
	, m_fCameraSwayAngle_RightLimit(0.3f)
	, m_bCameraSwayDirection(false)
	, characterClass(SCAVENGER)
	, characterSkillLevel(0)
	, timeElapsed(0)
	, CooldownTimer(0)
	, Cooldown(0)
	, characterSkillSpeedModifier(1.0)
	, characterSkillStaminaDrainModifier(1.0)
	, characterSkillDamageResistance(1.0)
	, characterSkillDamageModifier(1.0)
	, weatherSpeedModifier(1.0)
	, enemySpeedModifier(1.0)
	, characterSkill(false)
	, characterSkillActive(false)
	, m_bUse(false)
	, m_bGUse(false)
	, usetimer(0)
	, usegtimer(0)
	, theCurrentSlot(FIRST)
	, Money(0)
	, Intututorial(true)
	, score(0)
{
}

CPlayerInfo::~CPlayerInfo(void)
{
	if (weaponManager)
	{
		for (int i = 0; i < m_iNumOfWeapon; i++)
		{
			delete weaponManager[i];
		}
		delete [] weaponManager;
		weaponManager = NULL;
	}
	if (itemManager.size())
	{
		for (int i = 0; i < itemManager.size(); i++)
		{
			delete itemManager[i];
		}
		vector<CItemInfo*>().swap(itemManager);
	}
	if (secondaryWeapon)
	{
		delete secondaryWeapon;
		secondaryWeapon = NULL;
	}
	if (primaryWeapon)
	{
		delete primaryWeapon;
		primaryWeapon = NULL;
	}
	m_pTerrain = NULL;

	if (clipManager.size() > 0)
	{
		for (int i = 0; i < clipManager.size(); i++)
		{
			delete clipManager[i];
		}
		vector<CItemInfo*>().swap(clipManager);
	}
	if (attachedCamera)
	{
		attachedCamera = NULL;
	}
}

// Initialise this class instance
void CPlayerInfo::Init(void)
{
	// Set the default values
	defaultPosition.Set(-480, 0, -350);
	defaultTarget.Set(0, 0, 0);
	defaultUp.Set(0, 1, 0);

	// Set the current values
	position.Set(-480, 0, -350);
	target.Set(0, 0, 0);
	up.Set(0, 1, 0);
	vel.Set(0, 0, 0);

	// Set Boundary
	maxBoundary.Set(1,1,1);
	minBoundary.Set(-1, -1, -1);

	// Set the pistol as the primary weapon
	/*primaryWeapon = new CPistol();
	primaryWeapon->Init();*/

	weaponManager = new CWeaponInfo*[m_iNumOfWeapon];

	weaponManager[0] = new CPistol();
	weaponManager[0]->Init();

	weaponManager[1] = NULL;
	//weaponManager[1] = new CPistol();
	//weaponManager[1]->Init();

	weaponManager[2] = NULL;
	//weaponManager[2] = new CPistol();
	//weaponManager[2]->Init();

	AddItemInit("Bandage");
	AddItemInit("Grenade");

	AddAmmoType("9mm");
	AddAmmoType(".357 Magnum");
	AddAmmoType(".45 ACP");
	AddAmmoType("5.56mm");
	AddAmmoType("7.62x51mm");
	AddAmmoType("12 Gauge");
	AddAmmoType(".308 Lapua Magnum");
	AddAmmoType("2mm EC");

	if (clipManager.capacity() > 0)
	{
		UpdateAmmo(true);
	}

	theCurrentPosture = STAND;
	m_fCameraSwayAngle = 0.0f;
	m_fCameraSwayDeltaAngle = 1.0f;
	m_fCameraSwayAngle_LeftLimit = -2.0f;
	m_fCameraSwayAngle_RightLimit = 2.0f;
	m_bCameraSwayDirection = false;

	jetpackfuel = 20;
	isflying = false;
	if (characterClass == TANK)
	{
		Health = 150;
		maxHealth = 150;
	}
	else if (characterClass == SCOUT)
	{
		Health = 80;
		maxHealth = 80;
	}
	else
	{
		Health = 100;
		maxHealth = 100;
	}

	scopedin = false;

	reload = false;
	reloadtimer = 1.2;

	Stamina = 100;
	recovertimer = 5;

	characterSkillLevel = 0;

	characterSkillActive = false;

	Cooldown = false;

	ScavengerDiscount = 1;

	Intututorial = true;

	CooldownTimer = 20;

	score = 0;

	CSoundEngine::GetInstance()->Init();
	CSoundEngine::GetInstance()->AddSound("9mm", "Image//SoundEffects//9mm.mp3");
	CSoundEngine::GetInstance()->AddSound("9mm2", "Image//SoundEffects//9mm2.mp3");
	CSoundEngine::GetInstance()->AddSound(".45ACP", "Image//SoundEffects//45Acp.mp3");
	CSoundEngine::GetInstance()->AddSound(".45ACP2", "Image//SoundEffects//45Acp2.mp3");
	CSoundEngine::GetInstance()->AddSound(".357Magnum", "Image//SoundEffects//357Magnum.mp3");
	CSoundEngine::GetInstance()->AddSound(".357Magnum2", "Image//SoundEffects//357Magnum2.mp3");

	CSoundEngine::GetInstance()->AddSound("5.56mm", "Image//SoundEffects//556mm.mp3");
	CSoundEngine::GetInstance()->AddSound("5.56mm2", "Image//SoundEffects//556mm2.mp3");
	CSoundEngine::GetInstance()->AddSound("762x51mm", "Image//SoundEffects//762x51mm.mp3");
	CSoundEngine::GetInstance()->AddSound("762x51mm2", "Image//SoundEffects//762x51mm2.mp3");

	CSoundEngine::GetInstance()->AddSound("12Gauge", "Image//SoundEffects//12Gauge.mp3");
	CSoundEngine::GetInstance()->AddSound("12Gauge2", "Image//SoundEffects//12Gauge2.mp3");

	CSoundEngine::GetInstance()->AddSound(".308Lapua", "Image//SoundEffects//308Lapua.mp3");
	CSoundEngine::GetInstance()->AddSound(".308Lapua2", "Image//SoundEffects//308Lapua.mp3");
	CSoundEngine::GetInstance()->AddSound("2mmEC", "Image//SoundEffects//2mmEC.mp3");
	CSoundEngine::GetInstance()->AddSound("2mmEC2", "Image//SoundEffects//2mmEC.mp3");

	CSoundEngine::GetInstance()->AddSound("ShotgunReload", "Image//SoundEffects//ShotgunReload.mp3");
	CSoundEngine::GetInstance()->AddSound("Reload", "Image//SoundEffects//EveryOtherReload.mp3");

	SetAABB(Vector3(1, 0, 1), Vector3(-1, -10, -1));

	Math::InitRNG();
}

// Returns true if the player is on ground
bool CPlayerInfo::isOnGround(void)
{
	if (m_bJumpUpwards == false && m_bFallDownwards == false)
		return true;

	return false;
}

// Returns true if the player is jumping upwards
bool CPlayerInfo::isJumpUpwards(void)
{
	if (m_bJumpUpwards == true && m_bFallDownwards == false)
		return true;

	return false;
}

// Returns true if the player is on freefall
bool CPlayerInfo::isFreeFall(void)
{
	if (m_bJumpUpwards == false && m_bFallDownwards == true)
		return true;

	return false;
}

// Set the player's status to free fall mode
void CPlayerInfo::SetOnFreeFall(bool isOnFreeFall)
{
	if (isOnFreeFall == true)
	{
		m_bJumpUpwards = false;
		m_bFallDownwards = true;
		m_dFallSpeed = 0.0;
	}
}

// Set the player to jumping upwards
void CPlayerInfo::SetToJumpUpwards(bool isOnJumpUpwards)
{
	if (isOnJumpUpwards == true && theCurrentPosture == STAND)
	{
		m_bJumpUpwards = true;
		m_bFallDownwards = false;
		m_dJumpSpeed = 10.0;
	}
}

// Set position
void CPlayerInfo::SetPos(const Vector3& pos)
{
	position = pos;
}

// Set target
void CPlayerInfo::SetTarget(const Vector3& target)
{
	this->target = target;
}

// Set position
void CPlayerInfo::SetUp(const Vector3& up)
{
	this->up = up;
}

void CPlayerInfo::SetVel(const Vector3 & vel)
{
	this->vel = vel;
}

// Set m_dJumpAcceleration of the player
void CPlayerInfo::SetJumpAcceleration(const double m_dJumpAcceleration)
{
	this->m_dJumpAcceleration = m_dJumpAcceleration;
}

// Set Fall Acceleration of the player
void CPlayerInfo::SetFallAcceleration(const double m_dFallAcceleration)
{
	this->m_dFallAcceleration = m_dFallAcceleration;
}

// Set the boundary for the player info
void CPlayerInfo::SetBoundary(Vector3 max, Vector3 min)
{
	maxBoundary = max;
	minBoundary = min;
}

// Set the terrain for the player info
void CPlayerInfo::SetTerrain(GroundEntity* m_pTerrain)
{
	if (m_pTerrain != NULL)
	{
		this->m_pTerrain = m_pTerrain;

		SetBoundary(this->m_pTerrain->GetMaxBoundary(), this->m_pTerrain->GetMinBoundary());
	}
}

// Stop the player's movement
void CPlayerInfo::StopVerticalMovement(void)
{
	m_bJumpUpwards = false;
	m_bFallDownwards = false;
}

// Reset this player instance to default
void CPlayerInfo::Reset(void)
{
	// Set the current values to default values
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;

	// Stop vertical movement too
	StopVerticalMovement();
}

// Get position x of the player
Vector3 CPlayerInfo::GetPos(void) const
{
	return position;
}

// Get target
Vector3 CPlayerInfo::GetTarget(void) const
{
	return target;
}
// Get Up
Vector3 CPlayerInfo::GetUp(void) const
{
	return up;
}

Vector3 CPlayerInfo::GetVel(void) const
{
	return vel;
}

// Get m_dJumpAcceleration of the player
double CPlayerInfo::GetJumpAcceleration(void) const
{
	return m_dJumpAcceleration;
}


int CPlayerInfo::GetClassUpgradeCost(void)
{
	switch (characterSkillLevel)
	{
	case 0:
	{
		return 40;
		break;
	}
	case 1:
	{
		return 60;
		break;
	}
	case 2:
	{
		return 80;
		break;
	}
	default:
		return 0;
		break;
	}
}

bool CPlayerInfo::GetCooldown(void)
{
	return Cooldown;
}

bool CPlayerInfo::GetUsingBandages(void)
{
	return m_bUse;
}

bool CPlayerInfo::GetUsingGrenades(void)
{
	return m_bGUse;
}

float CPlayerInfo::GetWeatherSpeedModifier(void)
{
	return weatherSpeedModifier;
}

void CPlayerInfo::SetWeatherSpeedModifier(float newFloat)
{
	weatherSpeedModifier = newFloat;
}

// true = wep to ammo; false = ammo to wep
void CPlayerInfo::UpdateAmmo(bool isWep)
{
	switch (isWep)
	{
	case true:
	{
		for (int i = 0; i < m_iNumOfWeapon; i++)
		{
			if (weaponManager[i] != NULL)
			{
				if (weaponManager[i]->GetWeaponName() == "M9")
				{
					for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
					{
						CAmmo* ao = (CAmmo*)*it;
						if (ao)
						{
							if (ao->GetAmmoType() == "9mm")
							{
								ao->SetCurrentAmount(weaponManager[i]->GetTotalClips());
								break;
							}
						}
					}
				}
				else if (weaponManager[i]->GetWeaponName() == "MPRex")
				{
					for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
					{
						CAmmo* ao = (CAmmo*)*it;
						if (ao)
						{
							if (ao->GetAmmoType() == ".357 Magnum")
							{
								ao->SetCurrentAmount(weaponManager[i]->GetTotalClips());
								break;
							}
						}
					}
				}
				else if (weaponManager[i]->GetWeaponName() == "M1911" || weaponManager[i]->GetWeaponName() == "UMP")
				{
					for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
					{
						CAmmo* ao = (CAmmo*)*it;
						if (ao)
						{
							if (ao->GetAmmoType() == ".45 ACP")
							{
								ao->SetCurrentAmount(weaponManager[i]->GetTotalClips());
								break;
							}
						}
					}
				}
				else if (weaponManager[i]->GetWeaponName() == "M4A1")
				{
					for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
					{
						CAmmo* ao = (CAmmo*)*it;
						if (ao)
						{
							if (ao->GetAmmoType() == "5.56mm")
							{
								ao->SetCurrentAmount(weaponManager[i]->GetTotalClips());
								break;
							}
						}
					}
				}
				else if (weaponManager[i]->GetWeaponName() == "Scar-H")
				{
					for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
					{
						CAmmo* ao = (CAmmo*)*it;
						if (ao)
						{
							if (ao->GetAmmoType() == "7.62x51mm")
							{
								ao->SetCurrentAmount(weaponManager[i]->GetTotalClips());
								break;
							}
						}
					}
				}
				else if (weaponManager[i]->GetWeaponName() == "Pump Shotgun")
				{
					for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
					{
						CAmmo* ao = (CAmmo*)*it;
						if (ao)
						{
							if (ao->GetAmmoType() == "12 Gauge")
							{
								ao->SetCurrentAmount(weaponManager[i]->GetTotalClips());
								break;
							}
						}
					}
				}
				else if (weaponManager[i]->GetWeaponName() == "AWP")
				{
					for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
					{
						CAmmo* ao = (CAmmo*)*it;
						if (ao)
						{
							if (ao->GetAmmoType() == ".308 Lapua Magnum")
							{
								ao->SetCurrentAmount(weaponManager[i]->GetTotalClips());
								break;
							}
						}
					}
				}
				else if (weaponManager[i]->GetWeaponName() == "Railgun")
				{
					for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
					{
						CAmmo* ao = (CAmmo*)*it;
						if (ao)
						{
							if (ao->GetAmmoType() == "2mm EC")
							{
								ao->SetCurrentAmount(weaponManager[i]->GetTotalClips());
								break;
							}
						}
					}
				}

			}
		}
	}
	break;
	case false:
	{
		for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
		{
			CAmmo* ao = (CAmmo*)*it;
			if (ao)
			{
				for (int i = 0; i < m_iNumOfWeapon; ++i)
				{
					if (weaponManager[i])
					{
						if (ao->GetAmmoType() == weaponManager[i]->GetAmmoType())
						{
							weaponManager[i]->SetTotalClips(ao->GetCurrentAmount());
						}
					}
				}
			}
		}
	}
	break;
	default:
		break;
	}
	
}

void CPlayerInfo::UpdateAmmo(std::string AmmoType, int AddClip)
{
	for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
	{
		CAmmo* ao = (CAmmo*)*it;
		if (ao)
		{
			if (ao->GetAmmoType() == AmmoType)
			{
				ao->SetCurrentAmount(ao->GetCurrentAmount() + AddClip);
				UpdateAmmo(false);
			}

		}
	}
}



// Update Jump Upwards
void CPlayerInfo::UpdateJumpUpwards(double dt)
{
	if (m_bJumpUpwards == false)
		return;

	// Update the jump's elapsed time
	m_dElapsedTime += dt;

	// Update position and target y values
	// Use SUVAT equation to update the change in position and target
	// s = u * t + 0.5 * a * t ^ 2
	position.y += (float)(m_dJumpSpeed * m_dElapsedTime + 
						  0.5 * m_dJumpAcceleration * m_dElapsedTime * m_dElapsedTime);
	target.y += (float)(m_dJumpSpeed * m_dElapsedTime + 
						0.5 * m_dJumpAcceleration * m_dElapsedTime * m_dElapsedTime);
	// Use this equation to calculate final velocity, v
	// SUVAT: v = u + a * t; v is m_dJumpSpeed AFTER updating using SUVAT where u is 
	// the initial speed and is equal to m_dJumpSpeed
	m_dJumpSpeed = m_dJumpSpeed + m_dJumpAcceleration * m_dElapsedTime;
	// Check if the jump speed is less than zero, then it should be falling
	if (m_dJumpSpeed < 0.0)
	{
		m_dJumpSpeed = 0.0;
		m_bJumpUpwards = false;
		m_dFallSpeed = 0.0;
		m_bFallDownwards = true;
		m_dElapsedTime = 0.0;
	}
}

// Update FreeFall
void CPlayerInfo::UpdateFreeFall(double dt)
{
	if (m_bFallDownwards == false)
		return;

	// Update the jump's elapsed time
	m_dElapsedTime += dt;

	// Update position and target y values.
	// Use SUVAT equation to update the change in position and target
	// s = u * t + 0.5 * a * t ^ 2
	position.y += (float)(m_dFallSpeed * m_dElapsedTime + 
						  0.5 * m_dJumpAcceleration * m_dElapsedTime * m_dElapsedTime);
	target.y += (float)(m_dFallSpeed * m_dElapsedTime + 
						0.5 * m_dJumpAcceleration * m_dElapsedTime * m_dElapsedTime);
	// Use this equation to calculate final velocity, v
	// SUVAT: v = u + a * t;
	// v is m_dJumpSpeed AFTER updating using SUVAT where u is the initial speed and is equal to m_dJumpSpeed
	m_dFallSpeed = m_dFallSpeed + m_dFallAcceleration * m_dElapsedTime;
	// Check if the jump speed is below terrain, then it should be reset to terrain height
	if (position.y < m_pTerrain->GetTerrainHeight(position))
	{
		Vector3 viewDirection = target - position;
		position.y = m_pTerrain->GetTerrainHeight(position);
		target = position + viewDirection;
		m_dFallSpeed = 0.0;
		m_bFallDownwards = false;
		m_dElapsedTime = 0.0;
	}
}

/********************************************************************************
 Hero Update
 ********************************************************************************/
void CPlayerInfo::Update(double dt)
{
	//double mouse_diff_x, mouse_diff_y;
	//MouseController::GetInstance()->GetMouseDelta(mouse_diff_x, mouse_diff_y);

	//double camera_yaw = mouse_diff_x * 0.0174555555555556;		// 3.142 / 180.0
	//double camera_pitch = mouse_diff_y * 0.0174555555555556;	// 3.142 / 180.0

	//// Update the position if the WASD buttons were activated
	
	if (weaponManager[m_iCurrentWeapon]->GetWeaponName() == "Pump Shotgun" && reload == false)
	{
		reloadtimer = 3;
	}
	else if(weaponManager[m_iCurrentWeapon]->GetWeaponName() != "Pump Shotgun" && reload == false)
	{
		reloadtimer = 1.2;
	}

	if (!KeyboardController::GetInstance()->IsKeyDown('W') || !KeyboardController::GetInstance()->IsKeyDown('D') || !KeyboardController::GetInstance()->IsKeyDown('A') || !KeyboardController::GetInstance()->IsKeyDown('D'))
	{
		if (!vel.IsZero())
		{
			vel += -vel.Normalized() * 0.09;

			if (vel.LengthSquared() <= 0.1 * 0.1)
			{
				vel.SetZero();
			}
		}
	}
	

	Vector3 viewVector = (target - position).Normalized();
	position += vel  * characterSkillSpeedModifier * enemySpeedModifier * weatherSpeedModifier;
	target = position + viewVector;

	if (isflying)
	{
		StopSway(dt);
	}

	if (jetpackfuel < 20 && isflying == false)
	{
		jetpackfuel += (float)(1 * dt);
	}

	else if (jetpackfuel <= 0)
	{
		jetpackfuel = 0;
		SetOnFreeFall(true);
	}

	if (jetpackfuel > 20)
	{
		jetpackfuel = 20;
	}

	//ability cooldown
	if (Cooldown)
	{
		CooldownTimer -= (float)(1 * dt);
		if (CooldownTimer <= 0)
		{
			Cooldown = false;
			CooldownTimer = 20;
		}
	}

	//stamina recovery
	if (Stamina < 100)
	{
		recovertimer -= (float)(1 * dt);
	}

	else if (Stamina >= 100)
	{
		recovertimer = 5;
	}

	if (recovertimer <= 0 && Stamina < 100)
	{
		Stamina +=1;
	}

	if (Stamina <= 0)
	{
		Stamina = 0;
		StopSway(dt);
	}

	//reloading
	if (reload == true)
	{
		reloadtimer -= (float)(1 * dt);
		if (weaponManager[m_iCurrentWeapon])
		{
			weaponManager[m_iCurrentWeapon]->SetCanFire(false);
		}
	}

	if (reloadtimer <= 0)
	{
		if (weaponManager[m_iCurrentWeapon])
		{
			weaponManager[m_iCurrentWeapon]->Reload();
			weaponManager[m_iCurrentWeapon]->SetCanFire(true);
			if (Intututorial)
			{
				weaponManager[m_iCurrentWeapon]->AddClip();
			}
		}
		reload = false;
	}

	if (clipManager.capacity() > 0)
	{
		UpdateAmmo(true);
	}

	if (m_bUse)
	{
		usetimer += dt;
	}

	if (m_bGUse)
	{
		usegtimer += dt;
	}

	if (usetimer > 5)
	{
		for (std::vector<CItemInfo*>::iterator it = itemManager.begin(); it != itemManager.end(); ++it)
		{
			CItemInfo* ao = (CItemInfo*)*it;
			if (ao)
			{
				if (m_bUse)
				{
					if (ao->GetItemType() == CItemInfo::I_CONSUME)
					{
						CBandage* ao1 = (CBandage*)*it;
						Health += ao1->UseItem();
						if (Health + ao1->GetRestoreAmount() > maxHealth)
						{
							Health = maxHealth;
						}

						m_bUse = false;
						break;
					}
				}
				
			}
		}


		usetimer = 0.f;
	}

	if (usegtimer > 1.5)
	{
		for (std::vector<CItemInfo*>::iterator it = itemManager.begin(); it != itemManager.end(); ++it)
		{
			CItemInfo* ao = (CItemInfo*)*it;
			if (ao)
			{
				if (m_bGUse)
				{
					if (ao->GetItemType() == CItemInfo::I_GRENADE)
					{

						CGrenade* ao2 = (CGrenade*)*it;
						ao2->UseItem(position, target, this);
						m_bGUse = false;
						break;
					}
				}
			}
		}

		usegtimer = 0.f;
	}

	if (weaponManager[m_iCurrentWeapon])
	{
		weaponManager[m_iCurrentWeapon]->Update(dt);
	}

	if (itemManager[m_iCurrentItem])
	{
		
	}

	{
		// Constrain the position
		Constrain();
		UpdateJumpUpwards(dt);
		UpdateFreeFall(dt);
	}

	// Do camera sway
	Vector3 viewUV = (target - position).Normalized();
	if (m_fCameraSwayAngle != 0.0f && theCurrentPosture == STAND)
	{
		Mtx44 rotation;
		if (m_bCameraSwayDirection == false)
			rotation.SetToRotation(-m_fCameraSwayDeltaAngle, viewUV.x, viewUV.y, viewUV.z);
		else if (m_bCameraSwayDirection == true)
			rotation.SetToRotation(m_fCameraSwayDeltaAngle, viewUV.x, viewUV.y, viewUV.z);
		up = rotation * up;
	}

	// Update minimap rotation angle
	CMinimap::GetInstance()->SetAngle(atan2(viewUV.z, viewUV.x) * 57.2883513685549146);

	// If a camera is attached to this playerInfo class, then update it
	if (attachedCamera)
	{
		attachedCamera->SetCameraPos(position);
		attachedCamera->SetCameraTarget(target);
		attachedCamera->SetCameraUp(up);
	}

	if (scopedin)
	{
		StopSway(dt);
	}

	switch (characterClass)
	{
	case SCOUT:
		if (characterSkillLevel == 0)
		{
			characterSkillSpeedModifier = 1.05;
		}
		if (characterSkillLevel >= 1)
		{
			characterSkillSpeedModifier = 1.25;
		}
		if (characterSkillLevel >= 2)
		{
			characterSkillStaminaDrainModifier = 0.2;
		}
		break;
	case TANK:
		if (characterSkillLevel >= 0)
		{
			characterSkillSpeedModifier = 0.9;
		}
		if (characterSkillLevel >= 1)
		{
			characterSkillDamageResistance = 0.8;
		}
		if (characterSkillLevel >= 2)
		{
			characterSkillDamageModifier = 1.1;
		}
		break;
	case SCAVENGER:
		break;
	}

	if (characterSkillLevel == 3)
	{
		characterSkill = true;
	}

	if (characterSkillActive == true)
		HandleAbility(dt);
}

// Detect and process front / back movement on the controller
bool CPlayerInfo::Move_FrontBack(const float deltaTime, const bool direction, const float speedMultiplier)
{
	if (speedMultiplier == 2.0f)
	{
		Stamina -= ((float)(10 * deltaTime) * characterSkillStaminaDrainModifier);
		recovertimer = 5;
		// Add camera sway
		if (m_bCameraSwayDirection == false)
		{
			m_fCameraSwayAngle -= m_fCameraSwayDeltaAngle;
			if (m_fCameraSwayAngle < m_fCameraSwayAngle_LeftLimit * speedMultiplier)
				m_bCameraSwayDirection = !m_bCameraSwayDirection;
		}
		else
		{
			m_fCameraSwayAngle += m_fCameraSwayDeltaAngle;
			if (m_fCameraSwayAngle > m_fCameraSwayAngle_RightLimit * speedMultiplier)
				m_bCameraSwayDirection = !m_bCameraSwayDirection;
		}
	}
	Vector3 viewVector = (target - position).Normalized();
	if (direction)
	{

		if (theCurrentPosture == STAND)
		{
			Vector3 a = viewVector * (float)m_dSpeed * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 1 * speedMultiplier)
			{
				vel = vel.Normalized() * 1 * speedMultiplier;
			}
			
		}

		else if (theCurrentPosture == CROUCH)
		{
			Vector3 a = viewVector * (float)m_dSpeed * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 0.5)
			{
				vel = vel.Normalized() * 0.5;
			}
		}

		else if (theCurrentPosture == PRONE)
		{
			Vector3 a = viewVector * (float)m_dSpeed * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 0.2)
			{
				vel = vel.Normalized() * 0.2;
			}
		}
		//	 Constrain the position
		Constrain();
		// Update the target
		target = position + viewVector;
		return true;
	}
	else
	{
		if (theCurrentPosture == STAND)
		{
			Vector3 a = -viewVector * (float)m_dSpeed * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 1 * speedMultiplier)
			{
				vel = vel.Normalized() * 1 * speedMultiplier;
			}
		}
		
		else if (theCurrentPosture == CROUCH)
		{
			Vector3 a = -viewVector * (float)m_dSpeed * speedMultiplier * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 0.5)
			{
				vel = vel.Normalized() * 0.5;
			}
		}

		else if (theCurrentPosture == PRONE)
		{
			Vector3 a = -viewVector * (float)m_dSpeed * speedMultiplier * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 0.2)
			{
				vel = vel.Normalized() * 0.2;
			}
		}
		//	 Constrain the position
		Constrain();
		// Update the target
		target = position + viewVector;
		return true;
	}

	return false;
}
// Detect and process left / right movement on the controller
bool CPlayerInfo::Move_LeftRight(const float deltaTime, const bool direction, const float speedMultiplier)
{
	Vector3 viewVector = target - position;
	Vector3 rightUV;
	if (direction)
	{
		rightUV = (viewVector.Normalized()).Cross(up);
		rightUV.y = 0;
		rightUV.Normalize();
		if (theCurrentPosture == STAND)
		{
			Vector3 a = -rightUV * (float)m_dSpeed * speedMultiplier * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 1)
			{
				vel = vel.Normalized() * 1;
			}
		}

		else if (theCurrentPosture == CROUCH)
		{
			Vector3 a = -rightUV * (float)m_dSpeed * speedMultiplier * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 0.5)
			{
				vel = vel.Normalized() * 0.5;
			}
		}

		else if (theCurrentPosture == PRONE)
		{
			Vector3 a = -rightUV * (float)m_dSpeed * speedMultiplier * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 0.2)
			{
				vel = vel.Normalized() * 0.2;
			}
		}
		// Update the target
		target = position + viewVector;
		return true;
	}
	else
	{
		rightUV = (viewVector.Normalized()).Cross(up);
		rightUV.y = 0;
		rightUV.Normalize();
		if (theCurrentPosture == STAND)
		{
			Vector3 a = rightUV * (float)m_dSpeed * speedMultiplier * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 1)
			{
				vel = vel.Normalized() * 1;
			}
		}

		else if (theCurrentPosture == CROUCH)
		{
			Vector3 a = rightUV * (float)m_dSpeed * speedMultiplier * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 0.5)
			{
				vel = vel.Normalized() * 0.5;
			}
		}

		else if (theCurrentPosture == PRONE)
		{
			Vector3 a = rightUV * (float)m_dSpeed * speedMultiplier * (float)deltaTime * (1 / weaponManager[m_iCurrentWeapon]->GetWeaponWeight());
			vel += a;
			if (vel.LengthSquared() > 0.2)
			{
				vel = vel.Normalized() * 0.2;
			}
		}
		// Update the target
		target = position + viewVector;
		return true;
	}
	return false;
}

// Detect and process look up / down on the controller
bool CPlayerInfo::Look_UpDown(const float deltaTime, const bool direction, const float speedMultiplier)
{
	if (speedMultiplier == 0.0f)
		return false;

	Vector3 viewUV = (target - position).Normalized();
	Vector3 rightUV;

	float pitch = (float)(-m_dSpeed * speedMultiplier * (float)deltaTime * 0.05);
	rightUV = viewUV.Cross(up);
	rightUV.y = 0;
	rightUV.Normalize();
	up = rightUV.Cross(viewUV).Normalized();
	Mtx44 rotation;
	rotation.SetToRotation(pitch, rightUV.x, rightUV.y, rightUV.z);
	viewUV = rotation * viewUV;
	target = position + viewUV;

	return true;
}
// Detect and process look left / right on the controller
bool CPlayerInfo::Look_LeftRight(const float deltaTime, const bool direction, const float speedMultiplier)
{
	if (speedMultiplier == 0.0f)
		return false;

	Vector3 viewUV = (target - position).Normalized();
	Vector3 rightUV;

	float yaw = (float)-m_dSpeed * speedMultiplier * (float)deltaTime * 0.05;
	Mtx44 rotation;
	rotation.SetToRotation(yaw, 0, 1, 0);
	viewUV = rotation * viewUV;
	target = position + viewUV;
	rightUV = viewUV.Cross(up);
	rightUV.y = 0;
	rightUV.Normalize();
	up = rightUV.Cross(viewUV).Normalized();

	return true;
}

// Stop sway
bool CPlayerInfo::StopSway(const float deltaTime)
{
	m_bCameraSwayDirection = false;
	m_fCameraSwayAngle = 0.0f;
	up = Vector3(0.0f, 1.0f, 0.0f);
	return true;
}

bool CPlayerInfo::Use_Item(const float deltaTime)
{
	if (!m_bUse && usetimer == 0.f && !reload && !m_bGUse && GetNumOfBandages() > 0)
	{
		m_bUse = true;
	}
	return false;
}

bool CPlayerInfo::Use_Grenade(const float deltaTime)
{
	if (!m_bGUse && usetimer == 0.f && !reload && !m_bUse && GetNumOfGrenades() > 0)
	{
		m_bGUse = true;
	}
	return false;
}

// Reload current weapon
bool CPlayerInfo::ReloadWeapon(void)
{
	if (weaponManager[m_iCurrentWeapon]->GetMagRound() < weaponManager[m_iCurrentWeapon]->GetMaxMagRound() && weaponManager[m_iCurrentWeapon]->GetTotalClips() > 0 && !reload && !m_bGUse && !m_bUse)
	{
		reload = true;

		if (weaponManager[m_iCurrentWeapon]->GetWeaponName() == "Pump Shotgun")
		{
			CSoundEngine::GetInstance()->PlayASound("ShotgunReload",false);
		}
		else
		{
			CSoundEngine::GetInstance()->PlayASound("Reload",false);
		}
		return true;
	}

	return false;
}

// Change current weapon
bool CPlayerInfo::ChangeWeapon(void)
{
	switch (theCurrentSlot)
	{
	case FIRST:
		if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) > 0)
		{
			if (weaponManager[m_iCurrentWeapon + 1])
			{
				theCurrentSlot = SECOND;
				MouseController::GetInstance()->SetMouseYoffset(0);
				m_iCurrentWeapon++;
				break;
			}
			else
			{
				MouseController::GetInstance()->SetMouseYoffset(0);
				break;
			}
		}
		if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) < 0)
		{
			if (weaponManager[2])
			{
				theCurrentSlot = THIRD;
				MouseController::GetInstance()->SetMouseYoffset(0);
				m_iCurrentWeapon = 2;
				break;
			}

			else if (weaponManager[m_iCurrentWeapon + 1])
			{
				theCurrentSlot = SECOND;
				MouseController::GetInstance()->SetMouseYoffset(0);
				m_iCurrentWeapon = 1;
				break;
			}
		}
		break;
	case SECOND:
		if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) > 0)
		{
			if (weaponManager[m_iCurrentWeapon + 1])
			{
				theCurrentSlot = THIRD;
				MouseController::GetInstance()->SetMouseYoffset(0);
				m_iCurrentWeapon++;
				break;
			}
			else if (weaponManager[m_iCurrentWeapon - 1])
			{
				theCurrentSlot = FIRST;
				MouseController::GetInstance()->SetMouseYoffset(0);
				m_iCurrentWeapon--;
				break;
			}
		}
		if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) < 0)
		{
			if (weaponManager[m_iCurrentWeapon - 1])
			{
				theCurrentSlot = FIRST;
				MouseController::GetInstance()->SetMouseYoffset(0);
				m_iCurrentWeapon--;
				break;
			}
			else
			{
				MouseController::GetInstance()->SetMouseYoffset(0);
				break;
			}
		}
		break;
	case THIRD:
		if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) > 0)
		{
			if (weaponManager[0])
			{
				theCurrentSlot = FIRST;
				MouseController::GetInstance()->SetMouseYoffset(0);
				m_iCurrentWeapon = 0;
				break;
			}
			else
			{
				MouseController::GetInstance()->SetMouseYoffset(0);
				break;
			}
		}
		if (MouseController::GetInstance()->GetMouseScrollStatus(MouseController::SCROLL_TYPE_YOFFSET) < 0)
		{
			if (weaponManager[m_iCurrentWeapon - 1])
			{
				theCurrentSlot = SECOND;
				MouseController::GetInstance()->SetMouseYoffset(0);
				m_iCurrentWeapon--;
				break;
			}
			else
			{
				MouseController::GetInstance()->SetMouseYoffset(0);
				break;
			}
		}
		break;
	default:
		break;
	}

	return true;
}

// Discharge Primary Weapon
bool CPlayerInfo::DischargePrimaryWeapon(const float deltaTime)
{

	Vector3 viewUV = (target - position).Normalized();


	if (weaponManager[m_iCurrentWeapon]->GetCanFire())
	{
		if (!scopedin)
		{
			//special behaviour for shotun
			if (weaponManager[m_iCurrentWeapon]->GetWeaponName() == "Pump Shotgun")
			{
				
				for (int i = 0; i < 5; i++)
				{
					if (characterClass == SCAVENGER)
					{
						Vector3 randomizer(Math::RandFloatMinMax(-0.05, 0.05), Math::RandFloatMinMax(-0.05, 0.05), Math::RandFloatMinMax(-0.05, 0.05));
						weaponManager[m_iCurrentWeapon]->SetCanFire(true);
						weaponManager[m_iCurrentWeapon]->Discharge(position, position + viewUV + randomizer, weaponManager[m_iCurrentWeapon]->GetProjSpeed(), characterSkillActive, this);
					}
					else
					{
						Vector3 randomizer(Math::RandFloatMinMax(-0.05, 0.05), Math::RandFloatMinMax(-0.05, 0.05), Math::RandFloatMinMax(-0.05, 0.05));
						weaponManager[m_iCurrentWeapon]->SetCanFire(true);
						weaponManager[m_iCurrentWeapon]->Discharge(position, position + viewUV + randomizer, weaponManager[m_iCurrentWeapon]->GetProjSpeed(), false, this);
						
					}
					if (i != 4)
					{
						weaponManager[m_iCurrentWeapon]->SetMagRound(weaponManager[m_iCurrentWeapon]->GetMagRound() + 1);
					}
				}
				handleFiringSound();
			}
			else
			{
				if (characterClass == SCAVENGER)
				{
					weaponManager[m_iCurrentWeapon]->Discharge(position, position + viewUV, weaponManager[m_iCurrentWeapon]->GetProjSpeed(), characterSkillActive, this);
					handleFiringSound();
				}
				else
				{
					weaponManager[m_iCurrentWeapon]->Discharge(position, position + viewUV, weaponManager[m_iCurrentWeapon]->GetProjSpeed(), false, this);
					handleFiringSound();
				}
			}
		}

		else if (scopedin)
		{
			//special behaviour for shotun
			if (weaponManager[m_iCurrentWeapon]->GetWeaponName() == "Pump Shotgun")
			{
				for (int i = 0; i < 5; i++)
				{
					Vector3 randomizer(Math::RandFloatMinMax(-0.05, 0.05), Math::RandFloatMinMax(-0.05, 0.05), Math::RandFloatMinMax(-0.05, 0.05));
					weaponManager[m_iCurrentWeapon]->SetCanFire(true);
					weaponManager[m_iCurrentWeapon]->Discharge(position, position + viewUV + randomizer, weaponManager[m_iCurrentWeapon]->GetProjSpeed(), false, this);

					if (i != 4)
					{
						weaponManager[m_iCurrentWeapon]->SetMagRound(weaponManager[m_iCurrentWeapon]->GetMagRound() + 1);
					}
				}
				handleFiringSound();

			}
			else
			{
				
				weaponManager[m_iCurrentWeapon]->Discharge(position, position + viewUV, weaponManager[m_iCurrentWeapon]->GetProjSpeed(), false, this);
				handleFiringSound();
			}
		}

		WeaponRecoil(deltaTime);
		return true;
	}

	return false;
}

// Discharge Secondary Weapon
bool CPlayerInfo::DischargeSecondaryWeapon(const float deltaTime)
{
	if (secondaryWeapon)
	{
		secondaryWeapon->Discharge(position, target, weaponManager[m_iCurrentWeapon]->GetProjSpeed(),this);
		return true;
	}

	return false;
}
#include "../CameraEffects/CameraEffects.h"

// Constrain the position within the borders
void CPlayerInfo::Constrain(void)
{
	// Constrain player within the boundary
	if (position.x > maxBoundary.x - 1.0f)
	{
		position.x = maxBoundary.x - 1.0f;
		CCameraEffects::GetInstance()->SetStatus_BloodScreen(true);
	}
	if (position.y > maxBoundary.y - 1.0f)
	{
		m_dElapsedTime = 0.2;
		position.y = maxBoundary.y - 1.0f;
		target.y = maxBoundary.y - 1.0f;
		m_dJumpSpeed = 0.0;
		m_bJumpUpwards = false;
		m_dFallSpeed = 0.0;
		m_bFallDownwards = true;
	}
	if (position.z > maxBoundary.z - 1.0f)
	{
		position.z = maxBoundary.z - 1.0f;
		CCameraEffects::GetInstance()->SetStatus_BloodScreen(true);
	}
	if (position.x < minBoundary.x + 1.0f)
	{
		position.x = minBoundary.x + 1.0f;
		CCameraEffects::GetInstance()->SetStatus_BloodScreen(true);
	}
	if (position.y < minBoundary.y + 1.0f)
		position.y = minBoundary.y + 1.0f;
	if (position.z < minBoundary.z + 1.0f)
	{
		position.z = minBoundary.z + 1.0f;
		CCameraEffects::GetInstance()->SetStatus_BloodScreen(true);
	}
	// if the player is not jumping nor falling, then adjust his y position
	if ((m_bJumpUpwards == false) && (m_bFallDownwards == false))
	{
		// if the y position is not equal to terrain height at that position, 
		// then update y position to the terrain height
		/*if (position.y != m_pTerrain->GetTerrainHeight(position))
			position.y = m_pTerrain->GetTerrainHeight(position);*/

		Vector3 viewDirection = target - position;
		switch (theCurrentPosture)
		{
		case STAND:
			position.y = m_pTerrain->GetTerrainHeight(Vector3(position.x, 0.0f, position.z));
			target = position + viewDirection;
			break;
		case CROUCH:
			position.y = m_pTerrain->GetTerrainHeight(Vector3(position.x, 0.0f, position.z));
			position.y -= 5.0f;
			target = position + viewDirection;
			break;
		case PRONE:
			position.y = m_pTerrain->GetTerrainHeight(Vector3(position.x, 0.0f, position.z));
			position.y -= 8.0f;
			target = position + viewDirection;
			break;
		default:
			break;
		}
	}

	
}

void CPlayerInfo::AttachCamera(FPSCamera* _cameraPtr)
{
	attachedCamera = _cameraPtr;
}

void CPlayerInfo::DetachCamera()
{
	attachedCamera = nullptr;
}

int CPlayerInfo::GetCurrentWeapon(void) const
{
	return m_iCurrentWeapon;
}

int CPlayerInfo::GetCurrentWeaponAmmo(void) const
{
	return weaponManager[m_iCurrentWeapon]->GetMagRound();
}

int CPlayerInfo::GetCurrentWeaponMaxAmmo(void) const
{
	return weaponManager[m_iCurrentWeapon]->GetMaxMagRound();
}

int CPlayerInfo::GetCurrentWeaponClips(void) const
{
	return weaponManager[m_iCurrentWeapon]->GetTotalClips();
}

int CPlayerInfo::GetCurrentWeaponMaxClips(void) const
{
	return weaponManager[m_iCurrentWeapon]->GetMaxTotalClips();
}

float CPlayerInfo::GetJetpackfuel(void) const
{
	return jetpackfuel;
}

int CPlayerInfo::GetWeaponDamage(void) const
{
	return weaponManager[m_iCurrentWeapon]->GetWeaponDamage();
}

void CPlayerInfo::ReplaceWeapon(std::string WeaponName, int choice)
{
	if (weaponManager[choice])
	{
		delete weaponManager[choice];
		weaponManager[choice] = NULL;
	}


	if (WeaponName == "M9")
	{
		weaponManager[choice] = new CPistol();
	}
	else if (WeaponName == "MPRex")
	{
		weaponManager[choice] = new CRevolver();
	}
	else if (WeaponName == "M1911")
	{
		weaponManager[choice] = new CHandCannon();
	}
	else if (WeaponName == "M4A1")
	{
		weaponManager[choice] = new CRifle();
	}
	else if (WeaponName == "Scar-H")
	{
		weaponManager[choice] = new CScar();
	}
	else if (WeaponName == "UMP")
	{
		weaponManager[choice] = new CSMG();
	}
	else if (WeaponName == "Pump Shotgun")
	{
		weaponManager[choice] = new CShotgun();
	}
	else if (WeaponName == "AWP")
	{
		weaponManager[choice] = new CSniper();
	}
	else if (WeaponName == "Railgun")
	{
		weaponManager[choice] = new CRailgun();
	}
	weaponManager[choice]->Init();
	UpdateAmmo(WeaponName, weaponManager[choice]->GetTotalClips());

}

void CPlayerInfo::AddWeapon(std::string WeaponName)
{
	for (int i = 0; i < m_iNumOfWeapon; ++i)
	{
		if (weaponManager[i] == NULL)
		{
			if (WeaponName == "M9")
			{
				weaponManager[i] = new CPistol();
				break;
			}
			else if (WeaponName == "MPRex")
			{
				weaponManager[i] = new CRevolver();
			}
			else if (WeaponName == "M1911")
			{
				weaponManager[i] = new CHandCannon();
			}
			else if (WeaponName == "M4A1")
			{
				weaponManager[i] = new CRifle();
			}
			else if (WeaponName == "Scar-H")
			{
				weaponManager[i] = new CScar();
			}
			else if (WeaponName == "UMP")
			{
				weaponManager[i] = new CSMG();
			}
			else if (WeaponName == "Pump Shotgun")
			{
				weaponManager[i] = new CShotgun();
			}
			else if (WeaponName == "AWP")
			{
				weaponManager[i] = new CSniper();
			}
			else if (WeaponName == "Railgun")
			{
				weaponManager[i] = new CRailgun();
			}
			weaponManager[i]->Init();
			UpdateAmmo(WeaponName, weaponManager[i]->GetTotalClips());
			break;
		}
	}
}

int CPlayerInfo::GetWeaponCost(std::string WeaponName)
{

	if (WeaponName == "M1911")
	{
		return 30 * ScavengerDiscount;
	}
	if (WeaponName == "MPRex")
	{
		return 35 * ScavengerDiscount;
	}
	else if (WeaponName == "M4A1")
	{
		return 100 * ScavengerDiscount;
	}
	else if (WeaponName == "Scar-H")
	{
		return 120 * ScavengerDiscount;
	}
	else if (WeaponName == "UMP")
	{
		return 70 * ScavengerDiscount;
	}
	else if (WeaponName == "Pump Shotgun")
	{
		return 70 * ScavengerDiscount;
	}
	else if (WeaponName == "AWP")
	{
		return 150 * ScavengerDiscount;
	}
	else if (WeaponName == "Railgun")
	{
		return 220 * ScavengerDiscount;
	}
	else
	{
		return 0;
	}
}

int CPlayerInfo::GetMoney(void) const
{
	return Money;
}

void CPlayerInfo::SetMoney(int Money)
{
	this->Money = Money;
}

void CPlayerInfo::AddMoney(int Money)
{
	this->Money += Money;
}

void CPlayerInfo::SubtractMoney(int Money)
{
	this->Money -= Money;
}

void CPlayerInfo::AddItem(string itemName, int choice)
{
	for (int i = 0; i < itemManager.size(); ++i)
	{
		if (itemManager[i])
		{
			if (itemName == "MedKit")
			{
				if (itemManager[i]->GetItemType() == CItemInfo::I_CONSUME && GetNumOfBandages() < 5)
				{
					if (itemManager[i]->GetCurrentAmount() + choice <= itemManager[i]->GetMaxCarryAmount())
					{
						itemManager[i]->SetCurrentAmount(itemManager[i]->GetCurrentAmount() + choice);
						SubtractMoney(10);
					}
					else
					{
						//custom error
					}
				}
			}
			else if (itemName == "Grenade")
			{
				if (itemManager[i]->GetItemType() == CItemInfo::I_GRENADE && GetNumOfGrenades() < 5)
				{
					if (itemManager[i]->GetCurrentAmount() + choice <= itemManager[i]->GetMaxCarryAmount())
					{
						itemManager[i]->SetCurrentAmount(itemManager[i]->GetCurrentAmount() + choice);
						SubtractMoney(10);
					}
					else
					{
						//custom error
					}
				}
			}
		}
	}
}


int CPlayerInfo::GetNumOfBandages(void)
{
	return itemManager[0]->GetCurrentAmount();
}

int CPlayerInfo::GetEquipmentCost(string EquipmentName)
{
	if (EquipmentName == "9mm")
	{
		return 2 * ScavengerDiscount;
	}
	if (EquipmentName == ".357 Magnum")
	{
		return 10 * ScavengerDiscount;
	}
	else if (EquipmentName == ".45 ACP")
	{
		return 10 * ScavengerDiscount;
	}
	else if (EquipmentName == "5.56mm")
	{
		return 20 * ScavengerDiscount;
	}
	else if (EquipmentName == "7.62x51mm")
	{
		return 30 * ScavengerDiscount;
	}
	else if (EquipmentName == "12 Gauge")
	{
		return 25 * ScavengerDiscount;
	}
	else if (EquipmentName == ".308 Lapua Magnum")
	{
		return 40 * ScavengerDiscount;
	}
	else if (EquipmentName == "2mm EC")
	{
		return 50 * ScavengerDiscount;
	}

	else if (EquipmentName == "MedKit")
	{
		return 10 * ScavengerDiscount;
	}

	else if (EquipmentName == "Grenade")
	{
		return 10 * ScavengerDiscount;
	}
	else
	{
		return 0;
	}
}

int CPlayerInfo::GetNumOfGrenades(void)
{
	return itemManager[1]->GetCurrentAmount();
}

bool CPlayerInfo::CheckFullInventory(void)
{
	if (weaponManager[1] != NULL && weaponManager[2] != NULL)
	{
		return true;
	}
	else
		return false;
}

string CPlayerInfo::Weapon2Name(void)
{
	if (weaponManager[1])
	{
		return weaponManager[1]->GetWeaponName();
	}

	else return "fail";
}

string CPlayerInfo::Weapon3Name(void)
{
	if (weaponManager[2])
	{
		return weaponManager[2]->GetWeaponName();
	}

	else return "fail";
}

float CPlayerInfo::getCharacterSkillDamageResistance(void) const
{
	return characterSkillDamageResistance;
}

float CPlayerInfo::getCharacterSkillDamageModifier(void) const
{
	return characterSkillDamageModifier;
}

string CPlayerInfo::GetWeaponName(void)
{
	return weaponManager[m_iCurrentWeapon]->GetWeaponName();
}

bool CPlayerInfo::WeaponRecoil(const float deltaTime)
{
	if (weaponManager[m_iCurrentWeapon])
	{
		Vector3 viewUV = (target - position).Normalized();
		Vector3 rightUV;

		float pitch = (float)(m_dSpeed * deltaTime * weaponManager[m_iCurrentWeapon]->GetRecoil());
		
		if (theCurrentPosture == CROUCH)
		{
			pitch = pitch * 0.5;
		}

		else if (theCurrentPosture == PRONE)
		{
			pitch = pitch * 0.2;
		}

		if (scopedin == true)
		{
			pitch = pitch * 0.6;
		}

		rightUV = viewUV.Cross(up);
		rightUV.y = 0;
		rightUV.Normalize();
		up = rightUV.Cross(viewUV).Normalized();
		Mtx44 rotation;
		rotation.SetToRotation(pitch, rightUV.x, rightUV.y, rightUV.z);
		viewUV = rotation * viewUV;
		target = position + viewUV;


		float yaw = Math::RandFloatMinMax((float)-m_dSpeed * (float)deltaTime, (float)m_dSpeed * (float)deltaTime);

		if (scopedin == true)
		{
			yaw = yaw * 0.5;
		}

		rotation.SetToRotation(yaw, 0, 1, 0);
		viewUV = rotation * viewUV;
		target = position + viewUV;
		rightUV = viewUV.Cross(up);
		rightUV.y = 0;
		rightUV.Normalize();
		up = rightUV.Cross(viewUV).Normalized();

		return true;
	}

	return false;

}

bool CPlayerInfo::PostureChange(const float deltaTime)
{
	theCurrentPosture = (CURRENT_POSTURE)(theCurrentPosture + 1);
	if (theCurrentPosture == NUM_POSTURE)
	{
		theCurrentPosture = STAND;
	}
	return true;
}

bool CPlayerInfo::Scope(const bool scoped, const float deltaTime)
{

	if (scoped)
	{
		scopedin = true;
		return true;
	}
	else if (!scoped)
	{
		scopedin = false;
		return false;
	}

	return false;
}

bool CPlayerInfo::JetpackEnabled(const bool fly, const float dt)
{
	if (fly && jetpackfuel > 0 && theCurrentPosture == STAND)
	{
		SetToJumpUpwards(true);
		SetOnFreeFall(false);
		jetpackfuel -= (float) (5 * dt);
		isflying = true;
		return true;
	}

	else if(fly == false)
	{
		SetToJumpUpwards(false);
		SetOnFreeFall(true);
		isflying = false;
		return true;
	}
	return false;

}

float CPlayerInfo::getWeaponScope(void)
{
	return weaponManager[m_iCurrentWeapon]->GetWeaponScope();
}

CItemInfo * CPlayerInfo::FetchAM(void)
{
	CItemInfo* ao = clipManager.back();
	return ao;
}

void CPlayerInfo::AddAmmoType(std::string nameType)
{
	CAmmo* temp = new CAmmo(nameType);
	clipManager.push_back(temp);
}

int CPlayerInfo::GetAmmoAmt(std::string nameType)
{
	for (std::vector<CItemInfo*>::iterator it = clipManager.begin(); it != clipManager.end(); ++it)
	{
		CAmmo* ao = (CAmmo*)*it;
		if (ao)
		{
			if (ao->GetAmmoType() == nameType)
			{
				UpdateAmmo(true);
				return ao->GetCurrentAmount();
			}

		}
		else
		{
			return 0;
		}
	}
}

void CPlayerInfo::AddItemInit(std::string ItemType)
{
	if (ItemType == "Bandage")
	{
		CBandage* temp = new CBandage();
		temp->SetCurrentAmount(3);
		temp->SetRestoreAmount(35);
		temp->SetMaxCarryAmount(5);
		temp->SetItemType(CItemInfo::I_CONSUME);
		itemManager.push_back(temp);
	}
	else if (ItemType == "Grenade")
	{
		CGrenade* temp1 = new CGrenade();
		temp1->SetCurrentAmount(4);
		temp1->SetMaxCarryAmount(5);
		temp1->SetItemType(CItemInfo::I_GRENADE);
		itemManager.push_back(temp1);
	}
}


void CPlayerInfo::CallAbility(void)
{
	if (!characterSkillActive && characterSkillLevel == 3)
	{
		if (!Cooldown)
		{
			characterSkillActive = true;
		}
	}
}

float CPlayerInfo::GetEnemySpeedModifier(void)
{
	return enemySpeedModifier;
}

void CPlayerInfo::handleFiringSound(void)
{
	if (weaponManager[m_iCurrentWeapon]->GetAmmoType() == "9mm")
	{
		CSoundEngine::GetInstance()->PlayASound("9mm",true);
	}
	else if (weaponManager[m_iCurrentWeapon]->GetAmmoType() == ".45 ACP")
	{
		CSoundEngine::GetInstance()->PlayASound(".45ACP", true);
	}
	else if (weaponManager[m_iCurrentWeapon]->GetAmmoType() == ".357 Magnum")
	{
		CSoundEngine::GetInstance()->PlayASound(".357Magnum", true);
	}
	else if (weaponManager[m_iCurrentWeapon]->GetAmmoType() == "5.56mm")
	{
		CSoundEngine::GetInstance()->PlayASound("5.56mm", true);
	}
	else if (weaponManager[m_iCurrentWeapon]->GetAmmoType() == "7.62x51mm")
	{
		CSoundEngine::GetInstance()->PlayASound("762x51mm", true);
	}
	else if (weaponManager[m_iCurrentWeapon]->GetAmmoType() == "12 Gauge")
	{
		CSoundEngine::GetInstance()->PlayASound("12Gauge", true);
	}
	else if (weaponManager[m_iCurrentWeapon]->GetAmmoType() == ".308 Lapua Magnum")
	{
		CSoundEngine::GetInstance()->PlayASound(".308Lapua", true);
	}
	else if (weaponManager[m_iCurrentWeapon]->GetAmmoType() == "2mm EC")
	{
		CSoundEngine::GetInstance()->PlayASound("2mmEC", true);
	}
}

void CPlayerInfo::SetEnemySpeedModifier(float newFloat)
{
	enemySpeedModifier = newFloat;
}

void CPlayerInfo::HandleAbility(double dt)
{
	timeElapsed += dt;
	switch (characterClass)
	{
	case SCOUT:
		if (Health + 1 >= maxHealth)
		{
			Health = maxHealth;
		}
		else
		{
			Health += 0.5;
		}
	
		if (timeElapsed >= 10)
		{
			timeElapsed = 0;
			characterSkillActive = false;
			Cooldown = true;
		}
		break;
	case TANK:
		characterSkillDamageResistance = 0.0;
		if (timeElapsed >= 5)
		{
			timeElapsed = 0;
			characterSkillDamageResistance = 0.8;
			characterSkillActive = false;
			Cooldown = true;
		}
		break;
	case SCAVENGER:
		if (timeElapsed >= 10)
		{
			timeElapsed = 0;
			characterSkillActive = false;
			Cooldown = true;
		}
		break;
	}
}

bool CPlayerInfo::GetCharacterSkillActive(void)
{
	return characterSkillActive;
}

void CPlayerInfo::handleYoink()
{
	int yoinkChoice = Math::RandIntMinMax(1, 8);
	int yoinkAmount = Math::RandIntMinMax(1, 3);
	switch (yoinkChoice)
	{
	case 1:
		if (GetAmmoAmt("9mm") - yoinkAmount < 0)
		{
			UpdateAmmo("9mm", -GetAmmoAmt("9mm"));
		}
		else
		{
			UpdateAmmo("9mm", -yoinkAmount);
		}
		break;
	case 2:
		if (GetAmmoAmt(".357 Magnum") - yoinkAmount < 0)
		{
			UpdateAmmo(".357 Magnum", -GetAmmoAmt(".357 Magnum"));
		}
		else
		{
			UpdateAmmo(".357 Magnum", -yoinkAmount);
		}
		break;
	case 3:
		if (GetAmmoAmt(".45 ACP") - yoinkAmount < 0)
		{
			UpdateAmmo(".45 ACP", -GetAmmoAmt(".45 ACP"));
		}
		else
		{
			UpdateAmmo(".45 ACP", -yoinkAmount);
		}
		break;
	case 4:
		if (GetAmmoAmt("5.56mm") - yoinkAmount < 0)
		{
			UpdateAmmo("5.56mm", -GetAmmoAmt("5.56mm"));
		}
		else
		{
			UpdateAmmo("5.56mm", -yoinkAmount);
		}
		break;
	case 5:
		if (GetAmmoAmt("7.62x51mm") - yoinkAmount < 0)
		{
			UpdateAmmo("7.62x51mm", -GetAmmoAmt("7.62x51mm"));
		}
		else
		{
			UpdateAmmo("7.62x51mm", -yoinkAmount);
		}
		break;
	case 6:
		if (GetAmmoAmt("12 Gauge") - yoinkAmount < 0)
		{
			UpdateAmmo("12 Gauge", -GetAmmoAmt("12 Gauge"));
		}
		else
		{
			UpdateAmmo("12 Gauge", -yoinkAmount);
		}
		break;
	case 7:
		if (GetAmmoAmt(".308 Lapua Magnum") - yoinkAmount < 0)
		{
			UpdateAmmo(".308 Lapua Magnum", -GetAmmoAmt(".308 Lapua Magnum"));
		}
		else
		{
			UpdateAmmo(".308 Lapua Magnum", -yoinkAmount);
		}
		break;
	case 8:
		if (GetAmmoAmt("2mm EC") - yoinkAmount < 0)
		{
			UpdateAmmo("2mm EC", -GetAmmoAmt("2mm EC"));
		}
		else
		{
			UpdateAmmo("2mm EC", -yoinkAmount);
		}
		break;
	}
}


void CPlayerInfo::gibAmmo(void)
{
	int gibType = Math::RandIntMinMax(1, 8);
	int gibAmount = Math::RandIntMinMax(1, 2);
	
	switch (gibType)
	{
	case 1:
		UpdateAmmo("9mm", gibAmount);
		break;
	case 2:
		UpdateAmmo(".357 Magnum", gibAmount);
		break;
	case 3:
		UpdateAmmo(".45 ACP", gibAmount);
		break;
	case 4:
		UpdateAmmo("5.56mm", gibAmount);
		break;
	case 5:
		UpdateAmmo("7.62x51mm", gibAmount);
		break;
	case 6:
		UpdateAmmo("12 Gauge", gibAmount);
		break;
	case 7:
		UpdateAmmo(".308 Lapua Magnum", gibAmount);
		break;
	case 8:
		UpdateAmmo("2mm EC", gibAmount);
		break;
	}
}