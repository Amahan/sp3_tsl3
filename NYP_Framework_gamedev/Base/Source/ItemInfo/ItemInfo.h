#pragma once

#include "Vector3.h"

class CPlayerInfo;

class CItemInfo
{
public:
	CItemInfo();
	virtual ~CItemInfo();

	enum ITEMTYPE
	{
		I_CONSUME,
		I_AMMO,
		I_WEAPON,
		I_GRENADE,

		NUM_ITEM_TYPE
	};
protected:
	
	int m_iRestoreAmount;
	int m_iRandDropAmount;
	int m_iMaxCarryAmount;
	int m_iCurrentAmount;

	bool m_bUse;
	
	float m_fElaspedTime;
	float m_fTimeBetweenUse;

	ITEMTYPE theItemType;

public:
	// Set the item's restore amount
	void SetRestoreAmount(int amount);
	// Set the item's random drop amount
	void SetRandDropAmount(int amount);
	// Set the item's max carry amount
	void SetMaxCarryAmount(int amount);
	// Set the item's currently carried amount
	void SetCurrentAmount(int amount);
	// Set the item's type
	void SetItemType(ITEMTYPE type);
	// Set the item's allowed to use status
	void SetItemUse(bool use);

	// Get the item's restore amount
	int GetRestoreAmount(void);
	// Get the item's random drop amount
	int GetRandDropAmount(void);
	// Get the item's max carry amount in inventory
	int GetMaxCarryAmount(void);
	// Get the item's currently carried amount
	int GetCurrentAmount(void);
	// Get the item type
	ITEMTYPE GetItemType(void);
	// Get the item's use status
	bool GetItemUse(void);

	// Initialise this instance to default values
	virtual void Init(void);
	// Update the elapsed time
	void Update(const double dt);

	// Print Self
	void PrintSelf(void);
};
