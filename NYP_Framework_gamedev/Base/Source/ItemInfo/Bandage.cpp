#include "Bandage.h"

#include <iostream>
using namespace std;

CBandage::CBandage()
{

}

CBandage::~CBandage()
{

}

void CBandage::Init()
{
	// Call the parent's init method
	CItemInfo::Init();

	theItemType = CItemInfo::I_CONSUME;
	m_iRestoreAmount = 35;
	m_iMaxCarryAmount = 5;
	m_iRandDropAmount = Math::RandIntMinMax(1, 5);
	m_iCurrentAmount = 3;

	m_fElaspedTime = 0.f;
	m_fTimeBetweenUse = 5.f;

	m_bUse = true;
}

int CBandage::UseItem(void)
{
	if (m_iCurrentAmount > 0)
	{
		m_bUse = false;
		--m_iCurrentAmount;
		return GetRestoreAmount();

	}
	else
	{
		return 0;
	}
}
