#include "Grenade.h"
#include "../Projectile/Projectile.h"

#include <iostream>
using namespace std;

CGrenade::CGrenade()
{

}

CGrenade::~CGrenade()
{

}

void CGrenade::Init()
{
	// Call the parent's init method
	CItemInfo::Init();

	theItemType = CItemInfo::I_GRENADE;
	m_iCurrentAmount = 4;
	m_iMaxCarryAmount = 5;

	m_fTimeBetweenUse = 4;
}

void CGrenade::UseItem(Vector3 position, Vector3 target, CPlayerInfo* _source)
{
	if (m_iCurrentAmount > 0)
	{

		CProjectile* aProjectile = Create::Projectile("Grenade", position, (target - position).Normalized(), 2.5f, 100.0f, _source);

		aProjectile->SetCollider(true);
		aProjectile->SetisBullet(false);
		aProjectile->SetIsNade(true);
		aProjectile->SetIsEnemy(false);
		aProjectile->SetAABB(Vector3(0.5f, 0.5f, 0.5f), Vector3(-0.5f, -0.5f, -0.5f));
		m_bUse = false;
		--m_iCurrentAmount;
		m_fElaspedTime = 0;

	}
}
