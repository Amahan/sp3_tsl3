#pragma once

#include "Vector3.h"
#include "ItemInfo.h"

class CAmmo : public CItemInfo
{
public:
	CAmmo();
	CAmmo(std::string type);
	virtual ~CAmmo();
	// Initialize default values
	void Init(void);
	
	void SetAmmoType(std::string type);
	std::string GetAmmoType(void);

	std::string m_sAmmoType;
};

