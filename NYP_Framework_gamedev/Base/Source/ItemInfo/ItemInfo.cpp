#include "ItemInfo.h"

#include <iostream>
using namespace std;

CItemInfo::CItemInfo()
	: m_iRestoreAmount(0)
	, m_iRandDropAmount(0)
	, m_iMaxCarryAmount(0)
	, m_iCurrentAmount(0)
	, m_bUse(true)
	, m_fElaspedTime(0.0f)
	, m_fTimeBetweenUse(0.0f)
{
}


CItemInfo::~CItemInfo()
{
}

void CItemInfo::SetRestoreAmount(int amount)
{
	m_iRestoreAmount = amount;
}

void CItemInfo::SetRandDropAmount(int amount)
{
	m_iRandDropAmount = amount;
}

void CItemInfo::SetMaxCarryAmount(int amount)
{
	m_iMaxCarryAmount = amount;
}

void CItemInfo::SetCurrentAmount(int amount)
{
	m_iCurrentAmount = amount;
}

void CItemInfo::SetItemType(CItemInfo::ITEMTYPE type)
{
	theItemType = type;
}

void CItemInfo::SetItemUse(bool use)
{
	m_bUse = use;
}

int CItemInfo::GetRestoreAmount(void)
{
	return m_iRestoreAmount;
}

int CItemInfo::GetRandDropAmount(void)
{
	return m_iRandDropAmount;
}

int CItemInfo::GetMaxCarryAmount(void)
{
	return m_iMaxCarryAmount;
}

int CItemInfo::GetCurrentAmount(void)
{
	return m_iCurrentAmount;
}

CItemInfo::ITEMTYPE CItemInfo::GetItemType(void)
{
	return theItemType;
}

bool CItemInfo::GetItemUse(void)
{
	return m_bUse;
}

// Initialise this instance to default values
void CItemInfo::Init(void)
{
	m_iRestoreAmount = 0;
	m_iRandDropAmount = 0;
	m_iMaxCarryAmount = 0;
}

// Update the elapsed time
void CItemInfo::Update(const double dt)
{
	m_fElaspedTime += dt;
	if (m_fElaspedTime > m_fTimeBetweenUse)
	{
		m_bUse = true;
		m_fElaspedTime = 0.0;
	}

	if (m_iCurrentAmount <= 0)
	{
		m_bUse = false;

	}
}

// Print Self
void CItemInfo::PrintSelf(void)
{
	
}
