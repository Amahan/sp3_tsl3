#pragma once

#include "Vector3.h"
#include "ItemInfo.h"
#include "../WeaponInfo/WeaponInfo.h"

class CGrenade : public CItemInfo
{
public:
	CGrenade();

	virtual ~CGrenade();
	// Initialize default values
	void Init(void);

	void UseItem(Vector3 position, Vector3 target, CPlayerInfo* _source);
};

