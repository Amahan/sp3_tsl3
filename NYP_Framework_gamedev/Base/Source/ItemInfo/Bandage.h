#pragma once

#include "Vector3.h"
#include "ItemInfo.h"

class CBandage : public CItemInfo
{
public:
	CBandage();
	virtual ~CBandage();
	// Initialize default values
	void Init(void);

	int UseItem(void);
};

