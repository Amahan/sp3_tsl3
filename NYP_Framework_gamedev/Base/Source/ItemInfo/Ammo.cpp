#include "Ammo.h"
#include "../WeaponInfo/WeaponInfo.h"

#include <iostream>
using namespace std;

CAmmo::CAmmo()
{

}

CAmmo::CAmmo(std::string type)
{
	m_sAmmoType = type;
}

CAmmo::~CAmmo()
{

}

void CAmmo::Init(void)
{
	// Call the parent's init method
	CItemInfo::Init();

	theItemType = CItemInfo::I_AMMO;
	m_iMaxCarryAmount = 15;
	m_iRandDropAmount = Math::RandIntMinMax(1, 5);
	m_iCurrentAmount = 0;

}

void CAmmo::SetAmmoType(std::string type)
{
	m_sAmmoType = type;
}

std::string CAmmo::GetAmmoType(void)
{
	return m_sAmmoType;
}


