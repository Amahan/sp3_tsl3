#include "Smoke.h"


CSmoke::CSmoke()
{
}


CSmoke::~CSmoke()
{
}

// Initialise this instance to default values
void CSmoke::Init(void)
{
	// Call the parent's Init method
	CWeatherInfo::Init();

	// Weather Effect's status
	m_bEffectActive = false;
	// Duration of effect
	m_fEffectLength = 0.f;
	// Total num of particles for effect
	m_iParticleNum = 0;

	// Position of effect
	position.Set(280, 0, -150);
	
	// Scale of effect
	m_vEffectScale.Set(20, 1, 20);
	// Scale of particles
	m_vParticleScale.Set(4, 4, 4);
	// Veloctiy of particles
	vel.Set(0, 5, 0);
	
}
