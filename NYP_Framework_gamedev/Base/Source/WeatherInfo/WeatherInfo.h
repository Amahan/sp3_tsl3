#pragma once

#include "Vector3.h"
#include "../Particle.h"

#include <vector>

class CPlayerInfo;

class CWeatherInfo
{
public:
	CWeatherInfo();
	virtual ~CWeatherInfo();

	virtual void Init(void);

	// Weather Control
	virtual bool GetEffectActive(void);
	virtual float GetEffectLength(void);
	virtual int GetTotalParticles(void);
	virtual float GetEffectTimer(void);
	virtual Vector3 GetPos(void);
	virtual Vector3 GetEffectScale(void);
	virtual Vector3 GetParticleScale(void);
	virtual Vector3 GetVel(void);

	virtual void SetEffectActive(bool check);
	virtual void SetEffectLength(float val);
	virtual void SetTotalParticles(int num);
	virtual void SetEffectTimer(float timer);
	virtual void SetPos(Vector3 pos);
	virtual void SetEffectScale(Vector3 scale);
	virtual void SetParticleScale(Vector3 scale);
	virtual void SetVel(Vector3 vel);

	virtual ParticleObject* GetParticle(void);
	virtual void AddParticle(ParticleObject_TYPE m_pType);
	virtual void RemoveParticle(ParticleObject_TYPE m_pType);

	virtual void SpawnParticles(float dt);
	virtual void Update(float dt);


protected:

	// Effect active
	bool m_bEffectActive;
	// Duration of effect
	float m_fEffectLength;
	// Total number of particles
	int m_iParticleNum;
	// Effect Timer
	float m_fTimer;
	// Particle of effect control

	// Position of effect
	Vector3 position;
	// Scale of weather effect
	Vector3 m_vEffectScale;
	// Scale of particles
	Vector3 m_vParticleScale;
	// Velocity of particles
	Vector3 vel;
	// Gravity for particles
	Vector3 m_gravity;
	// Original Position of effect
	Vector3 m_vOriginalPos;
public:
	// Particle vector
	std::vector<ParticleObject*>m_poList;

};
