#pragma once

#include "WeatherInfo.h"

class CRain :
	public CWeatherInfo
{
public:
	CRain();
	virtual ~CRain();

	// Initialise this instance to default values
	void Init(void);
};

