#include "Rain.h"


CRain::CRain()
{
}


CRain::~CRain()
{
}

// Initialise this instance to default values
void CRain::Init(void)
{
	// Call the parent's Init method
	CWeatherInfo::Init();

	// Weather Effect's status
	m_bEffectActive = false;
	// Duration of effect
	m_fEffectLength = 100.f;
	// Total num of particles for effect
	m_iParticleNum = 0;

	// Position of effect
	position.Set(0, 0, 0);
	// Scale of effect
	m_vEffectScale.Set(400, 1, 400);
	// Scale of particles
	m_vParticleScale.Set(3, 5, 3);
	// Veloctiy of particles
	vel.Set(1, -80, 1);
	
}
