#include "WeatherInfo.h"

#include <iostream>
using namespace std;

CWeatherInfo::CWeatherInfo()
	: m_bEffectActive(false)
	, m_fEffectLength(0.f)
	, m_iParticleNum(0)
	, m_fTimer(0.f)
	, position(0, 0, 0)
	, m_vEffectScale(1, 1, 1)
	, m_vParticleScale(1, 1, 1)
	, vel(0, 0, 0)
	, m_gravity(0, -9.8f, 0)
	, m_vOriginalPos(0, 0, 0)
{
}


CWeatherInfo::~CWeatherInfo()
{

}

void CWeatherInfo::Init(void)
{
	// Weather Effect's status
	m_bEffectActive = false;
	// Duration of effect
	m_fEffectLength = 0.f;
	// Total num of particles for effect
	m_iParticleNum = 0;

	// Position of effect
	position.Set(0, 0, 0);
	// Scale of effect
	m_vEffectScale.Set(1, 1, 1);
	// Scale of particles
	m_vParticleScale.Set(1, 1, 1);
	// Veloctiy of particles
	vel.Set(0, 0, 0);
	// Original pos of effect
	m_vOriginalPos = position;
}

bool CWeatherInfo::GetEffectActive(void)
{
	return m_bEffectActive;
}

void CWeatherInfo::SetEffectActive(bool check)
{
	this->m_bEffectActive = check;
}

float CWeatherInfo::GetEffectLength(void)
{
	return m_fEffectLength;
}

void CWeatherInfo::SetEffectLength(float val)
{
	this->m_fEffectLength = val;
}

int CWeatherInfo::GetTotalParticles(void)
{
	return m_iParticleNum;
}

float CWeatherInfo::GetEffectTimer(void)
{
	return m_fTimer;
}

void CWeatherInfo::SetTotalParticles(int num)
{
	this->m_iParticleNum = num;
}

void CWeatherInfo::SetEffectTimer(float timer)
{
	this->m_fTimer = timer;
}

Vector3 CWeatherInfo::GetPos(void)
{
	return position;
}

void CWeatherInfo::SetPos(Vector3 pos)
{
	this->position = pos;
}

Vector3 CWeatherInfo::GetEffectScale(void)
{
	return m_vEffectScale;
}

void CWeatherInfo::SetEffectScale(Vector3 scale)
{
	this->m_vEffectScale = scale;
}

Vector3 CWeatherInfo::GetParticleScale(void)
{
	return m_vParticleScale;
}

void CWeatherInfo::SetParticleScale(Vector3 scale)
{
	this->m_vParticleScale = scale;
}

Vector3 CWeatherInfo::GetVel(void)
{
	return vel;
}

void CWeatherInfo::SetVel(Vector3 vel)
{
	this->vel = vel;
}

ParticleObject * CWeatherInfo::GetParticle(void)
{
	for (std::vector<ParticleObject *>::iterator it = m_poList.begin(); it != m_poList.end(); ++it)
	{
		ParticleObject *particle = (ParticleObject *)*it;
		if (!particle->active)
		{
			particle->active = true;
			m_iParticleNum++;
			return particle;
		}
	}
	for (unsigned i = 0; i < 10; ++i)
	{
		ParticleObject *particle = new ParticleObject(ParticleObject_TYPE::P_WATER);
		m_poList.push_back(particle);
	}
	ParticleObject *particle = m_poList.back();
	particle->active = true;
	m_iParticleNum++;
	return particle;
}

void CWeatherInfo::AddParticle(ParticleObject_TYPE m_pType)
{
	ParticleObject* particle = GetParticle();
	particle->active = true;
	particle->type = m_pType;
	particle->scale = this->m_vParticleScale;
	particle->vel = this->vel;
	particle->pos.Set(Math::RandFloatMinMax(position.x - m_vEffectScale.x, position.x + m_vEffectScale.x), position.y, Math::RandFloatMinMax(position.z - m_vEffectScale.z, position.z + m_vEffectScale.z));
	particle->decreaseSize = Math::RandFloatMinMax(1, 2);
}

void CWeatherInfo::RemoveParticle(ParticleObject_TYPE m_pType)
{
	if (m_poList.size() > 0)
	{
		for (auto po : m_poList)
		{
			if (po)
			{
				if (po->active)
				{
					if (po->type == m_pType)
					{
						po->active = false;
						m_iParticleNum--;
					}
				}
			}
		}
	}
}

void CWeatherInfo::SpawnParticles(float dt)
{

}

void CWeatherInfo::Update(float dt)
{
	for (std::vector<ParticleObject *>::iterator it = m_poList.begin(); it != m_poList.end(); ++it)
	{
		ParticleObject *particle = (ParticleObject *)*it;
		if (particle->active)
		{
			if (particle->type == ParticleObject_TYPE::P_WATER)
			{
				particle->vel += m_gravity * (float)dt;

				particle->pos += particle->vel * (float)dt * 10.f;

				particle->rotation += particle->rotationSpeed * (float)dt;
			}

			if (particle->type == ParticleObject_TYPE::P_SMOKE)
			{
				particle->disappeartimer -= static_cast<float>(0.2 * dt);
				if (particle->disappeartimer <= 0)
				{
					particle->active = false;
					particle->disappeartimer = 1;
					m_iParticleNum--;
				}

				particle->pos += particle->vel * (float)dt * 10.f;

				if (!particle->scale.IsZero())
				{
					particle->scale.x -= (float)(particle->decreaseSize * dt);
					particle->scale.y -= (float)(particle->decreaseSize * dt);
					particle->scale.z -= (float)(particle->decreaseSize * dt);
				}

				particle->rotation += particle->rotationSpeed * (float)dt;
			}
			if (particle->type == ParticleObject_TYPE::P_GAS)
			{
				particle->vel.Set(Math::RandFloatMinMax(-1, 1), Math::RandFloatMinMax(-5, 5), Math::RandFloatMinMax(-1, 1) * dt);

				particle->pos += particle->vel * (float)dt * 10.f;

				particle->rotation += particle->rotationSpeed * (float)dt;

				//particle->disappeartimer -= static_cast<float>(0.1 * dt);
				//if (particle->disappeartimer <= 0)
				//{
				//	particle->active = false;
				//	particle->disappeartimer = 2;
				//	m_iParticleNum--;
				//}

				if (!particle->scale.IsZero())
				{
					particle->scale.x -= (float)(particle->decreaseSize * dt);
					particle->scale.y -= (float)(particle->decreaseSize * dt);
					particle->scale.z -= (float)(particle->decreaseSize * dt);
				}
			}

			if (particle->pos.y < 0 && particle->type == ParticleObject_TYPE::P_WATER)
			{
				particle->active = false;
				m_iParticleNum--;
			}

			if (particle->type == ParticleObject_TYPE::P_SMOKE && particle->scale.x < 0)
			{
				particle->scale.SetZero();
				particle->active = false;
				m_iParticleNum--;
			}

			if (((particle->pos - position).LengthSquared() > 20 * 20) && particle->type == ParticleObject_TYPE::P_GAS)
			{
				particle->active = false;
				m_iParticleNum--;
			}

		}
	}
}

