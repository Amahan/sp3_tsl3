#include "Gas.h"


CGas::CGas()
{
}


CGas::~CGas()
{
}

// Initialise this instance to default values
void CGas::Init(void)
{
	// Call the parent's Init method
	CWeatherInfo::Init();

//	Math::InitRNG();

	// Weather Effect's status
	m_bEffectActive = false;
	// Duration of effect
	m_fEffectLength = 0.f;
	// Total num of particles for effect
	m_iParticleNum = 0;

	// Position of effect
	position.Set(0, 5, -300);
	// Scale of effect
	m_vEffectScale.Set(20, 1, 20);
	// Scale of particles
	m_vParticleScale.Set(1.5, 1.5, 1.5);
	// Veloctiy of particles
	vel.Set(Math::RandFloatMinMax(-20, 20), Math::RandFloatMinMax(-20, 20), Math::RandFloatMinMax(-20, 20));

	m_vOriginalPos = position;
}
