#ifndef SCENE_TEXT_H
#define SCENE_TEXT_H

#include "Scene.h"
#include "Mtx44.h"
#include "PlayerInfo/PlayerInfo.h"
#include "GroundEntity.h"
#include "FPSCamera.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "GenericEntity.h"
#include "SpriteEntity.h"
#include "Enemy/Enemy3D.h"
#include "HardwareAbstraction\Keyboard.h"
#include "Minimap\Minimap.h"
#include "CameraEffects\CameraEffects.h"
#include "HardwareAbstraction\Mouse.h"
#include "WeatherInfo\WeatherInfo.h"
#include "Highscore.h"

class ShaderProgram;
class SceneManager;
class TextEntity;
class Light;
class SceneText : public Scene
{	
public:
	SceneText();
	~SceneText();

	virtual void Init();
	virtual void Update(double dt);
	virtual void Render();
	virtual void Exit();

	static int Enemycount;

private:
	SceneText(SceneManager* _sceneMgr); // This is used to register to SceneManager
	ShaderProgram* currProg;
	CPlayerInfo* playerInfo;
	GroundEntity* groundEntity;
	FPSCamera camera;
	TextEntity* textObj[62];

	//Light* lights[2];

	CKeyboard* theKeyboard;
	CMouse* theMouse;
	CMinimap* theMinimap;
	CCameraEffects* theCameraEffects;


	SpriteEntity* AmmoCount;
	SpriteEntity* Health;
	SpriteEntity* Grenades;
	SpriteEntity* MedKits;
	SpriteEntity* CrossHairNormal;
	SpriteEntity* CrossHairShotgun;
	SpriteEntity* Money;
	SpriteEntity* Death;
	SpriteEntity* Win;
	SpriteEntity* ScoutClass;
	SpriteEntity* TankClass;
	SpriteEntity* ScavengerClass;
	SpriteEntity* Controls;
	SpriteEntity* Title;
	SpriteEntity* ScoutAblity[2];
	SpriteEntity* TankAblity[2];
	SpriteEntity* ScavengerAblity[2];
	SpriteEntity* Stamina[3];
	SpriteEntity* HealthBar[3];
	SpriteEntity* StaminaBar[3];
	SpriteEntity* GunDamage[10];
	SpriteEntity* GunFirerate[10];
	SpriteEntity* GunRecoil[10];
	SpriteEntity* GunScope[10];
	SpriteEntity* GunWeight[10];
	SpriteEntity* GunMag[10];

	//houses
	GenericEntity* VendingMachine[4];
	GenericEntity* blacksmith;
	GenericEntity* cabin;
	GenericEntity* church;
	GenericEntity* stall;
	GenericEntity* well;

	GenericEntity* wall;

	GenericEntity* gate[4];


	//indoor houses

	CEnemy3D* BasicEnemy;	// This is the CEnemy class for 3D use.
	CEnemy3D* TankEnemy;
	CEnemy3D* SpeedyEnemy;
	CEnemy3D* AmmoSnatcher;
	CEnemy3D* FlyingEnemy;
	CEnemy3D* Target;

	CWeatherInfo* weatherManager[3];

	Highscore* highscore;


	static SceneText* sInstance; // The pointer to the object that gets registered

	int GunIndex;
	int EquipmentIndex;
	bool EnterShop;
	bool ReplaceNeeded;
	int ReplacementIndex;
	int ShopTab;
	float rotation;
	bool Rain;
	float RainTimer;
	bool menu;
	int MenuIndex;
	int waves;
	float EnemySpawntimer;
	float EnemySpawntimer2;
	float EnemySpawntimer3;
	float EnemySpawntimer4;;
	float preparetimer;
	bool ClassSelect;
	bool EnterControl;
	bool EnterHighScore;

	int Area1temp;
	int Area2temp;
	int Area3temp;
	int Area4temp;
	bool Area1WaveSpawned;
	bool Area2WaveSpawned;
	bool Area3WaveSpawned;
	bool Area4WaveSpawned;
	bool Area1Unlocked;
	bool Area2Unlocked;
	bool Area3Unlocked;
	bool Area4Unlocked;

	bool Area4Surprise;
	float countdownTimer;

	int ClassIndex;

	bool WinGame;

	int m_particleCount; //num of particles
	unsigned MAX_PARTICLE; //max no. of particles

	Vector3 m_gravity; //gravity that affects the particles

	//gun stats
	int damage, fireRate, recoil, scope, weight, magazine;

	//create indoor house
	void Boundary(void);
	void ShootingRange(void);
	void AreaOne(void);
	void AreaTwo(void);
	void AreaThree(void);
	void AreaFour(void);

	void Shop(void);
	void ReplaceGun(void);
	void RenderInventory(void);
	void RenderClassSelection(void);

	void RenderClassAbility(void);

	void RenderCurrentWeapon(void);


	//spawn waves
	void SpawnArea1Waves(float dt);
	void SpawnArea2Waves(float dt);
	void SpawnArea3Waves(float dt);
	void SpawnArea4Waves(float dt);

	string GetGunindexName(void);
	string GetEquipmentName(void);

	float AngleCalc(float, float);

	void UpdateParticles(double dt);

	ParticleObject * GetParticle(void);
	void RenderParticles(ParticleObject*particle);

	std::vector<ParticleObject*> particleList;
};

#endif