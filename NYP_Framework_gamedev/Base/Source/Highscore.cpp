#include "highscore.h"


Highscore::Highscore()
{
	highScore = 0;
}


Highscore::~Highscore()
{
}

void Highscore::LoadData()
{
	ifstream scoreData;
	scoreData.open("HighScore.txt");
	if (scoreData.is_open())
	{
		string hScore;
		getline(scoreData, hScore);
		highScore = atoi(hScore.c_str());
	}
	else
	{
	}
	scoreData.close();	
}

void Highscore::SaveData()
{
	ofstream saveData;
	saveData.open("HighScore.txt");
	if (saveData.is_open())
	{
		saveData << highScore << endl;
	}
	else
	{
	}
	saveData.close();
}

void Highscore::SetData(int highS)
{
	highScore = highS;
	SaveData();
}