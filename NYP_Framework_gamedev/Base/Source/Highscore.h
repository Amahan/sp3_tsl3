#pragma once
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

using namespace std;

class Highscore
{
public:
	int highScore;
	Highscore();
	~Highscore();
	void LoadData();
	void SaveData();
	void SetData(int);
};
