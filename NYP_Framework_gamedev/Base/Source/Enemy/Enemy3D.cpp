#include "Enemy3D.h"
#include "../EntityManager.h"
#include "GraphicsManager.h"
#include "RenderHelper.h"
#include "MeshBuilder.h"

CEnemy3D::CEnemy3D(Mesh* _modelMesh)
	: GenericEntity(NULL)
	, defaultPosition(Vector3(0.0f,0.0f,0.0f))
	, defaultTarget(Vector3(0.0f, 0.0f, 0.0f))
	, defaultUp(Vector3(0.0f, 0.0f, 0.0f))
	, target(Vector3(0.0f, 0.0f, 0.0f))
	, up(Vector3(0.0f, 0.0f, 0.0f))
	, maxBoundary(Vector3(0.0f, 0.0f, 0.0f))
	, minBoundary(Vector3(0.0f, 0.0f, 0.0f))
	, m_pTerrain(NULL)
	, m_fElapsedTimeBeforeUpdate(0.0f)
{
	this->modelMesh = _modelMesh;
}


CEnemy3D::~CEnemy3D()
{
}

void CEnemy3D::Init(void)
{
	// Set the default values
	defaultPosition.Set(0, 0, 10);
	defaultTarget.Set(0, 0, 0);
	defaultUp.Set(0, 1, 0);

	// Set the current values
	position.Set(-20.0f, 0.0f, -200.0f);
	target.Set(10.0f, 0.0f, 0.0f);
	up.Set(0.0f, 1.0f, 0.0f);

	// Set Boundary
	maxBoundary.Set(1, 1, 1);
	minBoundary.Set(-1, -1, -1);


	// Set speed
	m_dSpeed = 10.0;

	switch (enemyType)
	{
	case Basic:
		Sethp(40);
		break;
	case Tank:
		Sethp(200);
		break;
	case Speed:
		Sethp(20);
		break;
	case Snatch:
		Sethp(15);
	case FlyBoi:
		Sethp(10);
		break;
	case Target:
		Sethp(10);
		break;

	}

	basicState = basicIdle;
	tankState = tankIdle;
	speedState = speedIdle;
	snatcherState = hunting;
	flyboiState = chase;
	chargeTimer = 0;
	prepChargeTimer = 0;

	targetState = targetIdleL;
	targetTimer = 0;

	runningaway = false;
}

// Reset this player instance to default
void CEnemy3D::Reset(void)
{
	// Set the current values to default values
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}

// Set position
void CEnemy3D::SetPos(const Vector3& pos)
{
	position = pos;
}

// Set target
void CEnemy3D::SetTarget(const Vector3& target)
{
	this->target = target;
}
// Set Up
void CEnemy3D::SetUp(const Vector3& up)
{
	this->up = up;
}
// Set the boundary for the player info
void CEnemy3D::SetBoundary(Vector3 max, Vector3 min)
{
	maxBoundary = max;
	minBoundary = min;
}
// Set the terrain for the player info
void CEnemy3D::SetTerrain(GroundEntity* m_pTerrain)
{
	if (m_pTerrain != NULL)
	{
		this->m_pTerrain = m_pTerrain;

		SetBoundary(this->m_pTerrain->GetMaxBoundary(), this->m_pTerrain->GetMinBoundary());
	}
}

// Get position
Vector3 CEnemy3D::GetPos(void) const
{
	return position;
}

// Get target
Vector3 CEnemy3D::GetTarget(void) const
{
	return target;
}
// Get Up
Vector3 CEnemy3D::GetUp(void) const
{
	return up;
}
// Get the terrain for the player info
GroundEntity* CEnemy3D::GetTerrain(void)
{
	return m_pTerrain;
}

// Update
void CEnemy3D::Update(double dt)
{
	switch (enemyType)
	{
	case Basic:
		updateBasic(dt);
		break;
	case Tank:
		updateTank(dt);
		break;
	case Speed:
		updateSpeed(dt);
		break;
	case Snatch:
		updateSnatchers(dt);
		break;
	case FlyBoi:
		updateFlyboi(dt);
		break;
	case Target:
		updateTarget(dt);
		break;

	default:
		break;
	}
	// Constrain the position
	Constrain();

	// This is RealTime Loop control
	// Update the target once every 5 seconds. 
	// Doing more of these calculations will not affect the outcome.
	m_fElapsedTimeBeforeUpdate += dt;
	if (m_fElapsedTimeBeforeUpdate > 5.0f)
	{
		//cout << m_fElapsedTimeBeforeUpdate << endl;
		m_fElapsedTimeBeforeUpdate = 0.0f;
		if (position.z > 400.0f)
			target.z = position.z * -1;
		else if (position.z < -400.0f)
			target.z = position.z * -1;
	}

	//enemy with player collision
	Vector3 altThisMinAABB = Vector3(this->GetMinAABB().x, this->GetMinAABB().y, this->GetMaxAABB().z);
	Vector3 altThisMaxAABB = Vector3(this->GetMaxAABB().x, this->GetMaxAABB().y, this->GetMinAABB().z);
	Vector3 playerposmin = CPlayerInfo::GetInstance()->GetPos() + CPlayerInfo::GetInstance()->GetMinAABB();
	Vector3 playerposmax = CPlayerInfo::GetInstance()->GetPos() + CPlayerInfo::GetInstance()->GetMaxAABB();

	
	if (CheckOverlap(position + this->minAABB, position + this->maxAABB, playerposmin, playerposmax) || (CheckOverlap(position + altThisMinAABB, position + altThisMaxAABB, playerposmin,playerposmax)))
	{
		CCameraEffects::GetInstance()->SetStatus_BloodScreen(true);
		CPlayerInfo::GetInstance()->Health -= (1 * CPlayerInfo::GetInstance()->getCharacterSkillDamageResistance());
		if (enemyType == Tank && tankState == charge)
		{
			CPlayerInfo::GetInstance()->Health -= (7 * CPlayerInfo::GetInstance()->getCharacterSkillDamageResistance());
		}
	}
}

//Update basic enemies
void CEnemy3D::updateBasic(double dt)
{
	target = CPlayerInfo::GetInstance()->GetPos();
	Vector3 viewVector = (target - position).Normalized();
	float distance = (position - CPlayerInfo::GetInstance()->GetPos()).Length();
	Vector3 a = viewVector * (float)m_dSpeed * (float)dt;
	switch (basicState)
	{
	case basicIdle:
		basicState = basicAttack;
		break;
	case basicAttack:
		vel += a;
		if (vel.LengthSquared() > 0.2)
		{
			vel = vel.Normalized() * 0.2;
		}
		position += vel;
		if (Gethp() <= 20)
		{
			basicState = aggressive;
		}
		break;
	case aggressive:
		vel += a;
		if (vel.LengthSquared() > 0.3)
		{
			vel = vel.Normalized() * 0.3;
		}
		position += vel * 1.8;
		break;
	}

	if (this->isDone)
	{
		CPlayerInfo::GetInstance()->AddMoney(15);
		SceneText::Enemycount--;
		CPlayerInfo::GetInstance()->score += 3;
	}
}

void CEnemy3D::updateTank(double dt)
{
	target = CPlayerInfo::GetInstance()->GetPos();
	Vector3 viewVector = (target - position).Normalized();
	float distance = (position - CPlayerInfo::GetInstance()->GetPos()).Length();
	Vector3 a = viewVector * (float)m_dSpeed * (float)dt;
	// 0.03
	// 0.07
	switch (tankState)
	{
	case tankIdle:
		tankState = tankAttack;
		break;
	case tankAttack:
		vel += a;
		if (vel.LengthSquared() > 0.3)
		{
			vel = vel.Normalized() * 0.3;
		}
		position += vel * 0.8;
		if (Gethp() <= 100)
		{
			tankState = prepCharge;
		}
		break;
	case prepCharge:
		prepChargeTimer += dt;
		chargeTarget = target;
		if (prepChargeTimer >= 5)
		{
			tankState = charge;
		}
		break;
	case charge:
	{
		a = ((chargeTarget - position).Normalized()) * (float)m_dSpeed * (float)dt;
		vel += a;
		if (vel.LengthSquared() > 0.2)
		{
			vel = vel.Normalized() * 0.2;
		}
		position += vel * 18;
		chargeTimer += (float)(1 * dt);
		if (chargeTimer >= 5)
		{
			tankState = sluggish;
		}
		break;
	}
	case sluggish:
		vel += a;
		if (vel.LengthSquared() * 0.25)
		{
			vel = vel.Normalized() * 0.25;
		}
		position += vel * 0.6;
		break;
	}

	if (this->isDone)
	{
		CPlayerInfo::GetInstance()->AddMoney(30);
		SceneText::Enemycount--;
		CPlayerInfo::GetInstance()->score += 5;
	}
}

void CEnemy3D::updateSpeed(double dt)
{
	target = CPlayerInfo::GetInstance()->GetPos();
	Vector3 viewVector = (target - position).Normalized();
	float distance = (position - CPlayerInfo::GetInstance()->GetPos()).Length();
	Vector3 a = viewVector * (float)m_dSpeed * (float)dt;
	switch (speedState)
	{
	case speedIdle:
		speedState = speedAttack;
		break;
	case speedAttack:
		vel += a;
		if (vel.LengthSquared() > 0.3)
		{
			vel = vel.Normalized() * 0.3;
		}
		position += vel * 2;
		if (Gethp() <= 5)
		{
			speedState = speedRunning;
		}
		break;
	case speedRunning:
		a = -a;
		vel += a;
		if (vel.LengthSquared() > 0.3)
		{
			vel = vel.Normalized() * 0.3;
		}
		position += vel * 2;
		runningaway = true;
		if (distance >= 300)
		{
			Sethp(20);
			speedState = speedAttack;
			runningaway = false;
		}
		break;
	}

	if (this->isDone)
	{
		CPlayerInfo::GetInstance()->AddMoney(8);
		SceneText::Enemycount--;
		CPlayerInfo::GetInstance()->score += 2;
	}
}

void CEnemy3D::updateFlyboi(double dt)
{
	target = CPlayerInfo::GetInstance()->GetPos();
	Vector3 viewVector = (target - position).Normalized();
	float distance = (position - CPlayerInfo::GetInstance()->GetPos()).Length();
	Vector3 a = viewVector * (float)m_dSpeed * (float)dt;
	switch (flyboiState)
	{
	case chase:
		if (CPlayerInfo::GetInstance()->GetEnemySpeedModifier() != 1.0)
		{
			CPlayerInfo::GetInstance()->SetEnemySpeedModifier(1.0);
		}
		vel += a;
		if (vel.LengthSquared() > 0.25)
		{
			vel = vel.Normalized() * 0.25;
		}
		position += vel * 2;
		if (distance <= 100)
		{
			flyboiState = idle;
		}
		break;
	case idle:
		if (CPlayerInfo::GetInstance()->GetEnemySpeedModifier() != 0.5)
		{
			CPlayerInfo::GetInstance()->SetEnemySpeedModifier(0.5);
		}
		if (distance >= 100)
		{
			flyboiState = chase;
		}
		else if (distance <= 60)
		{
			flyboiState = retreat;
		}
		break;
	case retreat:
		a = -a;
		vel += a;
		if (vel.LengthSquared() > 0.25)
		{
			vel = vel.Normalized() * 0.25;
		}
		position += vel * 2;
		if (CPlayerInfo::GetInstance()->GetEnemySpeedModifier() != 1.0)
		{
			CPlayerInfo::GetInstance()->SetEnemySpeedModifier(1.0);
		}
		position += viewVector * (float)m_dSpeed * (float)dt * 2;
		runningaway = true;
		if (distance >= 200)
		{
			flyboiState = chase;
			runningaway = false;
		}
		break;
	}

	if (this->isDone)
	{
		CPlayerInfo::GetInstance()->AddMoney(8);
		CPlayerInfo::GetInstance()->SetEnemySpeedModifier(1.0);
		SceneText::Enemycount--;
		CPlayerInfo::GetInstance()->score += 1;
	}
}

void CEnemy3D::updateSnatchers(double dt)
{
	if (snatcherState == burrow)
	{
		target = (Vector3(CPlayerInfo::GetInstance()->GetPos().x, CPlayerInfo::GetInstance()->GetPos().y - 30, CPlayerInfo::GetInstance()->GetPos().z));
	}
	else
	{
		target = CPlayerInfo::GetInstance()->GetPos();
	}
	Vector3 viewVector = (target - position).Normalized();
	float distance = (position - CPlayerInfo::GetInstance()->GetPos()).Length();
	Vector3 a = viewVector * (float)m_dSpeed * (float)dt;
	switch (snatcherState)
	{
	case hunting:
		vel += a;
		if (vel.LengthSquared() > 0.25)
		{
			vel = vel.Normalized() * 0.25;
		}
		position += vel * 1.2;
		if (distance <= 200)
		{
			snatcherState = burrow;
		}
		break;
	case burrow:
		vel += a;
		if (vel.LengthSquared() > 0.25)
		{
			vel = vel.Normalized() * 0.25;
		}
		position += vel * 1.2;
		if (distance <= 50)
		{
			snatcherState = burrowup;
		}
		break;
	case burrowup:
		vel += a;
		if (vel.LengthSquared() > 0.25)
		{
			vel = vel.Normalized() * 0.25;
		}
		position += vel * 1.2;
		if (distance <= 15)
		{
			snatcherState = yoink;
		}
		break;
	case yoink:
		callYoink();
		snatcherState = snatcherRunning;
		break;
	case snatcherRunning:
		a = -a;
		vel += a;
		if (vel.LengthSquared() > 0.25)
		{
			vel = vel.Normalized() * 0.25;
		}
		position += vel * 1.5;
		runningaway = true;
		if (distance >= 300)
		{
			Sethp(0);
			isDone = true;
			runningaway = false;
		}
		break;
	}

	if (this->isDone)
	{
		CPlayerInfo::GetInstance()->AddMoney(15);
		SceneText::Enemycount--;
		CPlayerInfo::GetInstance()->score += 2;
	}
}

void CEnemy3D::updateTarget(double dt)
{
	switch (targetState)
	{
	case targetIdleL:
		targetTimer += (float)(1 * dt);
		if (targetTimer > 5)
		{
			targetState = targetMoveR;
		}
		break;

	case targetMoveR:
		targetTimer = 0;
		if (position.z < -280)
		{
			position.z += 1;
		}
		if (position.z >= -280)
		{
			targetState = targetIdleR;
		}
		break;

	case targetIdleR:
		targetTimer += (float)(1 * dt);
		if (targetTimer > 5)
		{
			targetState = targetMoveL;
		}
		break;

	case targetMoveL:
		targetTimer = 0;
		if (position.z > -420)
		{
			position.z -= 1;
		}
		if (position.z <= -420)
		{
			targetState = targetIdleL;
		}
		break;
	}
}

void CEnemy3D::Update(CWeatherInfo* weather , double dt)
{
	if (weather)
	{
		for (auto po : weather->m_poList)
		{
			if (po)
			{
				if (po->active)
				{
					if (hp > 0)
					{
						Vector3 dist = position - po->pos;
						float distBetween = dist.LengthSquared();
						if (distBetween < 20 * 20)
						{
							hp -= 1 * dt;
							m_dSpeed = 7;
						}
						else
						{
							m_dSpeed = 10;
						}
					}

					if (hp <= 0)
					{
						isDone = true;
					}
					
					Vector3 dist = CPlayerInfo::GetInstance()->GetPos() - po->pos;
					float distBetween = dist.LengthSquared();
					if (distBetween < 20 * 20)
					{
						switch (po->type)
						{
						case P_GAS:
						{
							CPlayerInfo::GetInstance()->Health -= 1 * dt;
						}
						break;
						case P_SMOKE:
						{
							CCameraEffects::GetInstance()->SetStatus_SmokeScreen(true);
						}
						break;

						case P_WATER:
						{
							
						}
						break;

						default:
							break;
						}
					}

				}
			}
		}
	}
}


//Call yoink in CPlayerinfo
void CEnemy3D::callYoink()
{
	CPlayerInfo::GetInstance()->handleYoink();
}

// Constrain the position within the borders
void CEnemy3D::Constrain(void)
{
	// Constrain player within the boundary
	if (position.x > maxBoundary.x - 1.0f)
		position.x = maxBoundary.x - 1.0f;
	if (position.z > maxBoundary.z - 1.0f)
		position.z = maxBoundary.z - 1.0f;
	if (position.x < minBoundary.x + 1.0f)
		position.x = minBoundary.x + 1.0f;
	if (position.z < minBoundary.z + 1.0f)
		position.z = minBoundary.z + 1.0f;

	// if the y position is not equal to terrain height at that position, 
	// then update y position to the terrain height
	if (!(enemyType == FlyBoi || enemyType == Snatch))
	{
		if (position.y != m_pTerrain->GetTerrainHeight(position) - 10)
		{
			position.y = m_pTerrain->GetTerrainHeight(position) - 10;
		}
	}

	if (enemyType == FlyBoi)
	{
		if (position.y <= m_pTerrain->GetTerrainHeight(position) - 10)
		{
			position.y = m_pTerrain->GetTerrainHeight(position) - 10;
		}
	}

	if (enemyType == Snatch && (snatcherState == hunting || snatcherState == snatcherRunning))
	{
		if (position.y != m_pTerrain->GetTerrainHeight(position) - 10)
		{
			position.y = m_pTerrain->GetTerrainHeight(position) - 10;
		}
	}

	else if (enemyType == Snatch)
	{
		if (position.y >= m_pTerrain->GetTerrainHeight(position) - 10)
		{
			position.y = m_pTerrain->GetTerrainHeight(position) - 10;
		}
	}

}

// Render
void CEnemy3D::Render(void)
{
	Vector3 viewVector = (target - position);
	if (runningaway)
	{
		viewVector = -viewVector;
	}

	float rotate = atan2(viewVector.x, viewVector.z);
	float rotate2 = atan2(CPlayerInfo::GetInstance()->GetPos().x - position.x, CPlayerInfo::GetInstance()->GetPos().z - position.z);

	
	MS& modelStack = GraphicsManager::GetInstance()->GetModelStack();
	modelStack.PushMatrix();

	modelStack.Translate(position.x, position.y, position.z);
	modelStack.Scale(scale.x, scale.y, scale.z);
	if (enemyType != Target)
	{
		modelStack.Rotate(Math::RadianToDegree(rotate), 0, 1, 0);
		modelStack.PushMatrix();
		modelStack.Translate(0, 12, 0);
		modelStack.Scale(this->Gethp() * 0.15, 1, 0);
		modelStack.Rotate(Math::RadianToDegree(rotate2 - rotate), 0, 1, 0);
		RenderHelper::RenderMesh(MeshBuilder::GetInstance()->GetMesh("quadlow"));
		modelStack.PopMatrix();
	}
	RenderHelper::RenderMesh(modelMesh);
	modelStack.PopMatrix();
}

CEnemy3D* Create::Enemy3D(const std::string& _meshName,
						const Vector3& _position,
						const Vector3& _scale)
{
	Mesh* modelMesh = MeshBuilder::GetInstance()->GetMesh(_meshName);
	if (modelMesh == nullptr)
		return nullptr;

	CEnemy3D* result = new CEnemy3D(modelMesh);
	result->SetPosition(_position);
	result->SetScale(_scale);
	result->SetCollider(true);
	result->SetIsEnemy(true);
	result->SetisBullet(false);
	result->SetIsNade(false);
	result->Sethp(100);
	EntityManager::GetInstance()->AddEntity(result);
	return result;
}