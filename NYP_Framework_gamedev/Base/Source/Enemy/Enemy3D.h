#pragma once
#include "../GenericEntity.h"
#include "../GroundEntity.h"
#include "../PlayerInfo/PlayerInfo.h"
#include "../ItemInfo/Ammo.h"
#include "../WeatherInfo/WeatherInfo.h"

#include <vector>
using namespace std;

class Mesh;

class CEnemy3D :
	public GenericEntity
{
protected:
	Mesh* modelMesh;

	Vector3 defaultPosition, defaultTarget, defaultUp;
	Vector3 target, up;
	Vector3 maxBoundary, minBoundary;
	GroundEntity* m_pTerrain;
	
	double m_dSpeed;
	double m_dAcceleration;
	float m_fElapsedTimeBeforeUpdate;

	float prepChargeTimer;
	Vector3 chargeTarget;
public:
	CEnemy3D(Mesh* _modelMesh);
	virtual ~CEnemy3D();

	void Init(void);
	// Reset this player instance to default
	void Reset(void);

	// Set position
	void SetPos(const Vector3& pos);
	// Set target
	void SetTarget(const Vector3& target);
	// Set Up
	void SetUp(const Vector3& up);
	// Set the boundary for the player info
	void SetBoundary(Vector3 max, Vector3 min);
	// Set the terrain for the player info
	void SetTerrain(GroundEntity* m_pTerrain);
	//Update for Basic enemies
	void updateBasic(double dt);
	//Update for Tank enemies
	void updateTank(double dt);
	//Update for Snatcher enemies
	void updateSnatchers(double dt);
	//Update update for Speed Enemies
	void updateSpeed(double dt);
	//Update for flying enemies
	void updateFlyboi(double dt);
	//Update for target
	void updateTarget(double dt);



	//Call yoink in playerinfo
	void callYoink(void);

	// Get position
	Vector3 GetPos(void) const;
	// Get target
	Vector3 GetTarget(void) const;
	// Get Up
	Vector3 GetUp(void) const;
	// Get the terrain for the player info
	GroundEntity* GetTerrain(void);

	bool runningaway;
	float targetTimer;

	enum BASIC_STATES
	{
		basicIdle,
		basicAttack,
		aggressive
	}basicState;

	enum TANK_STATES
	{
		tankIdle,
		tankAttack,
		prepCharge,
		charge,
		sluggish
	}tankState;
	float chargeTimer;

	enum SPEED_STATES
	{
		speedIdle,
		speedAttack,
		speedRunning
	}speedState;

	enum SNATCHER_STATES
	{
		hunting,
		burrow,
		burrowup,
		yoink,
		snatcherRunning
	}snatcherState;

	enum FLYBOI_STATES
	{
		chase,
		idle,
		retreat,
	}flyboiState;


	enum TARGET_STATES
	{
		targetIdleL,
		targetMoveR,
		targetIdleR,
		targetMoveL
	}targetState;

	enum ENEMYTYPE
	{
		Basic,
		Tank,
		Speed,
		Snatch,
		FlyBoi,
		Target
	} enemyType;

	// Update
	void Update(double dt = 0.0333f);
	//Update for gas
	void Update(CWeatherInfo* weather, double dt);
	// Constrain the position within the borders
	void Constrain(void);
	// Render
	void Render(void);
};

namespace Create
{
	CEnemy3D* Enemy3D(const std::string& _meshName,
						const Vector3& _position,
						const Vector3& _scale = Vector3(1.0f, 1.0f, 1.0f));
};
