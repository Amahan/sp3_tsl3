//week 12 - particle system
#include "Vector3.h"

#ifndef PARTCICLE_H
#define PARTICLE_H

enum ParticleObject_TYPE // Define the parameters/ attributes of a particle
{
	P_WATER = 0,
	P_SMOKE,
	P_GAS,

	P_TOTAL,
};

class ParticleObject
{
public:
	ParticleObject(ParticleObject_TYPE = P_WATER);
	~ParticleObject(void);

	ParticleObject_TYPE type;  // Type of Particle

	Vector3 pos;	// Position of Particle
	Vector3 vel;	// Velocity of Particle
	Vector3 scale;	// Scale of Particle
	float rotation;		//Rotation of particle
	float rotationSpeed;	//Rotational speed of particle
	float decreaseSize;

	float disappeartimer;

	bool active;
};
#endif  PARTCICLE_H

