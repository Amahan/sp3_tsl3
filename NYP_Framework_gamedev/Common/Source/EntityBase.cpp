#include "EntityBase.h"

EntityBase::EntityBase()
	: position(0.0f, 0.0f, 0.0f)
	, scale(1.0f, 1.0f, 1.0f)
	, vel(0.f, 0.f, 0.f)
	, normal(0.f, 0.f, 0.f)
	, gravity(0.f, -9.8f, 0.f)
	, isDone(false)
	, IsNade(false)
	, IsGround(false)
	, m_bCollider(false)
{
}

EntityBase::~EntityBase()
{
}

void EntityBase::Update(double _dt)
{
}

void EntityBase::Update(CWeatherInfo* weather, double dt)
{
}

void EntityBase::Render()
{
}

void EntityBase::RenderUI()
{
}

bool EntityBase::IsDone()
{
	return isDone;
}

void EntityBase::SetIsDone(bool _value)
{
	isDone = _value;
}

// Check if this entity has a collider class parent
bool EntityBase::HasCollider(void) const
{
	return m_bCollider;
}

// Set the flag to indicate if this entity has a collider class parent
void EntityBase::SetCollider(const bool _value)
{
	m_bCollider = _value;
}

bool EntityBase::GetIsBullet(void)
{
	return this->Isbullet;
}

void EntityBase::SetisBullet(const bool bullet)
{
	this->Isbullet = bullet;
}

int EntityBase::Gethp(void)
{
	return this->hp;
}

void EntityBase::Sethp(const int hp)
{
	this->hp = hp;
}

bool EntityBase::GetIsNade(void)
{
	return IsNade;
}

void EntityBase::SetIsNade(const bool nade)
{
	this->IsNade = nade;
}

bool EntityBase::GetIsGround(void)
{
	return IsGround;
}

void EntityBase::SetIsGround(const bool ground)
{
	this->IsGround = ground;
}

int EntityBase::GetIsEnemy(void)
{
	return this->Isenemy;
}

void EntityBase::SetIsEnemy(const bool enemy)
{
	this->Isenemy = enemy;
}

Vector3 EntityBase::GetPos(void)
{
	return position;
}

void EntityBase::SetPos(const Vector3 pos)
{
	this->position = pos;
}

Vector3 EntityBase::GetVel(void)
{
	return vel;
}

void EntityBase::SetVel(const Vector3 vel)
{
	this->vel = vel;
}

//Vector3 EntityBase::GetNormal(void)
//{
//	return normal;
//}
//
//void EntityBase::SetNormal(const Vector3 nor)
//{
//	this->normal = nor;
//}
