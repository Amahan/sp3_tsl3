#ifndef ENTITY_BASE_H
#define ENTITY_BASE_H

#include "Vector3.h"
#include "../../Base/Source/WeatherInfo/WeatherInfo.h"

class EntityBase
{
public:
	EntityBase();
	virtual ~EntityBase();

	virtual void Update(double _dt);
	virtual void Update(CWeatherInfo* weather, double dt);
	virtual void Render();
	virtual void RenderUI();

	inline void SetPosition(const Vector3& _value) { position = _value; };
	inline Vector3 GetPosition() { return position; };

	inline void SetScale(const Vector3& _value) { scale = _value; };
	inline Vector3 GetScale() { return scale; };


	bool IsDone();
	void SetIsDone(const bool _value);

	// Check if this entity has a collider class parent
	virtual bool HasCollider(void) const;
	// Set the flag to indicate if this entity has a collider class parent
	virtual void SetCollider(const bool _value);

	virtual bool GetIsBullet(void);
	virtual void SetisBullet(const bool bullet = false);

	virtual int Gethp(void);
	virtual void Sethp(const int hp = 0);

	virtual bool GetIsNade(void);
	virtual void SetIsNade(const bool nade = false);

	virtual bool GetIsGround(void);
	virtual void SetIsGround(const bool ground = false);

	virtual int GetIsEnemy(void);
	virtual void SetIsEnemy(const bool enemy = false);

	virtual Vector3 GetPos(void);
	virtual void SetPos(const Vector3 pos = Vector3(0, 0, 0));

	virtual Vector3 GetVel(void);
	virtual void SetVel(const Vector3 vel = Vector3(0, 0, 0));

//	virtual Vector3 GetNormal(void);
//	virtual void SetNormal(const Vector3 nor = Vector3(0, 0, 0));

protected:
	Vector3 position;
	Vector3 scale;
	Vector3 target;
	Vector3 vel;
	Vector3 normal;
	Vector3 gravity;


	bool isDone;
	bool m_bCollider;

	bool Isbullet;
	bool IsNade;
	bool IsGround;
	int hp;

	bool Isenemy;
};

#endif // ENTITY_BASE_H