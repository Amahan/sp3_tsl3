#ifndef RENDER_HELPER_H
#define RENDER_HELPER_H

#include "Vertex.h"

class Mesh;

class RenderHelper
{
public:
	static void RenderMesh(Mesh* _mesh, float transparency = 1.0f);
	static void RenderMeshWithLight(Mesh* _mesh);
	static void RenderText(Mesh* _mesh, const std::string& _text, Color _color);

	//render stuff on screen
	static void RenderMeshOnScreen(Mesh* _mesh,float transparency = 1.0f);
};

#endif // RENDER_HELPER_H